import { Component, Injector, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { InstructorServiceProxy, InstructorShortInfoDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateInstructorDialogComponent } from './create-instructor/create-instructor-dialog.component';
import { InstructorEditDialogComponent } from './edit-instructor/edit-instructor-dialog.component';
import { SetOfficeDialogComponent } from './set-office/set-office-dialog.component';
import { AddCourseInstructorDialogComponent } from './add-course/add-course-dialog.component';
import { InstructorDetailsDialogComponent } from './details/details-dialog.component';
import { SetUserInstructorDialogComponent } from './set-user/set-user-dialog.component';

@Component({
    templateUrl: './instructor.component.html',
    animations: [appModuleAnimation()],
    styles: [

    ]
})
export class InstructorComponent extends AppComponentBase implements OnInit {
    instructors: InstructorShortInfoDto[] = [];
    public isLoading = false;

    constructor(
        injector: Injector,
        private _instructorService: InstructorServiceProxy,
        private _dialog: MatDialog
    ) {
        super(injector);
    }

    ngOnInit() {
        this.list();
    }

    public refresh() {
        this.list();
    }

    public async setOffice(instructor: InstructorShortInfoDto) {
        const res = await this._dialog.open(SetOfficeDialogComponent, { data: instructor.id }).afterClosed().toPromise();
        if (res) {
            this.list();
        }
    }

    public async editInstructor(instructor: InstructorShortInfoDto) {
        const res = await this._dialog.open(InstructorEditDialogComponent, { data: instructor.id }).afterClosed().toPromise();
        if (res) {
            this.list();
        }
    }

    public showDetailed(instructor: InstructorShortInfoDto) {
        this._dialog.open(InstructorDetailsDialogComponent, { data: instructor.id });
    }

    public async createInstructor() {
        const res = await this._dialog.open(CreateInstructorDialogComponent).afterClosed().toPromise();
        if (res) {
            this.list();
        }
    }

    public async addCourse(instructor: InstructorShortInfoDto) {
        const res = await this._dialog.open(AddCourseInstructorDialogComponent, { data: instructor.id }).afterClosed().toPromise();
        if (res) {
            this.list();
        }
    }

    public async setUser(instructor: InstructorShortInfoDto) {
        await this._dialog.open(SetUserInstructorDialogComponent, { data: instructor.id }).afterClosed().toPromise();
    }

    protected async list() {
        this.isLoading = true;
        const result = await this._instructorService.getAll(undefined, undefined, undefined, undefined, undefined, undefined).toPromise();
        this.isLoading = false;
        this.instructors = result.items;
        console.log(this.instructors);
    }

    protected async delete(instructor: InstructorShortInfoDto) {
        abp.message.confirm(
            this.l('DeleteInstructorWarningMessage', instructor.firstName, instructor.lastName),
            async (res) => {
                if (res) {
                    await this._instructorService.delete(instructor.id).toPromise();
                    abp.notify.success(this.l('SuccessfullyDeleted'));
                    this.refresh();
                }
            }
        );
    }
}
