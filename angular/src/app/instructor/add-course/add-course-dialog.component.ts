import { Component, Injector, OnInit, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InstructorServiceProxy, CourseServiceProxy, CourseInfoDto, InstructorAddCourseDto } from '@shared/service-proxies/service-proxies';


@Component({
    templateUrl: './add-course-dialog.component.html'
})
export class AddCourseInstructorDialogComponent extends AppComponentBase implements OnInit {
    courses: CourseInfoDto[] = [];
    courseToAdd: InstructorAddCourseDto = new InstructorAddCourseDto();
    saving = false;
    selectedCourse: number;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<AddCourseInstructorDialogComponent>,
        public _instructorService: InstructorServiceProxy,
        public _courseService: CourseServiceProxy,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
        this.courseToAdd.instructorId = this._id;
    }

    async save() {
        this.courseToAdd.courseId = this.selectedCourse;
        this.saving = true;
        const res = await this._instructorService.addCourse(this.courseToAdd).toPromise();
        this.saving = false;
        if (res) { this.notify.success(this.l('SavedSuccessfully')); } else { this.notify.error(this.l('CourseAlreadyAssigned')); }
        this.close(true);
    }

    async ngOnInit() {
        const res = await this._courseService.getAll(undefined, undefined, undefined, undefined, undefined).toPromise();
        this.courses = res.items;
        console.log(this.courses);
    }

    close(result: any) {
        this._dialogRef.close(result);
    }
}
