import { Component, Injector, OnInit, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InstructorServiceProxy, OfficeAssignmentDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './set-office-dialog.component.html'
})
export class SetOfficeDialogComponent extends AppComponentBase implements OnInit {
    officeAssignment: OfficeAssignmentDto = new OfficeAssignmentDto();
    saving = false;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<SetOfficeDialogComponent>,
        public _instructorService: InstructorServiceProxy,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
    }

    ngOnInit() {
        this.officeAssignment.instructorId = this._id;
    }

    async save() {
        this.saving = true;
        const res = await this._instructorService.setOffice(this.officeAssignment).toPromise();
        this.saving = false;
        if (res) {
            this.notify.success(this.l('SavedSuccessfully'));
        } else {
            this.notify.error(this.l('OfficeAlreadyAssigned'));
        }
        this.close(true);
    }

    close(result: any) {
        this._dialogRef.close(result);
    }
}
