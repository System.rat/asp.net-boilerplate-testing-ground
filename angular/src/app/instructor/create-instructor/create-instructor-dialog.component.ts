import { Component, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef } from '@angular/material';
import { CreateInstructorDto, InstructorServiceProxy } from '@shared/service-proxies/service-proxies';


@Component({
    templateUrl: './create-instructor-dialog.component.html'
})
export class CreateInstructorDialogComponent extends AppComponentBase {
    instructor: CreateInstructorDto = new CreateInstructorDto();
    saving = false;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<CreateInstructorDialogComponent>,
        public _instructorService: InstructorServiceProxy
    ) {
        super(injector);
    }

    async save() {
        this.saving = true;
        await this._instructorService.create(this.instructor).toPromise();
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
        this.close(true);
    }

    close(result: any) {
        this._dialogRef.close(result);
    }
}
