import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Component, Injector, Optional, Inject } from '@angular/core';
import { InstructorInfoDto, InstructorServiceProxy } from '@shared/service-proxies/service-proxies';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';


@Component({
    templateUrl: './details-dialog.component.html'
})
export class InstructorDetailsDialogComponent extends AppComponentBase implements OnInit {
    instructor: InstructorInfoDto = null;
    isLoading = false;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<InstructorDetailsDialogComponent>,
        private _instructorService: InstructorServiceProxy,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
    }

    async ngOnInit() {
        this.isLoading = true;
        const res = await this._instructorService.getDetailed(this._id).toPromise();
        this.isLoading = false;
        this.instructor = res;
    }

    close() {
        this._dialogRef.close();
    }
}
