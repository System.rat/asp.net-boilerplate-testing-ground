import { Component, Injector, OnInit, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InstructorServiceProxy, UpdateInstructorDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './edit-instructor-dialog.component.html'
})
export class InstructorEditDialogComponent extends AppComponentBase implements OnInit {
    instructor: UpdateInstructorDto = new UpdateInstructorDto();
    saving = false;

    constructor(
        public _instructorService: InstructorServiceProxy,
        private _dialogRef: MatDialogRef<InstructorEditDialogComponent>,
        injector: Injector,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
    }

    async ngOnInit() {
        const res = await this._instructorService.get(this._id).toPromise();
        this.instructor.firstName = res.firstName;
        this.instructor.lastName = res.lastName;
        this.instructor.id = res.id;
    }

    async save() {
        this.saving = true;
        await this._instructorService.update(this.instructor).toPromise();
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
        this.close(true);
    }

    close(res: any) {
        this._dialogRef.close(res);
    }
}
