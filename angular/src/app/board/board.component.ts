import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Injector, Component } from '@angular/core';

@Component({
    templateUrl: './board.component.html'
})
export class BoardComponent extends AppComponentBase implements OnInit {
    constructor(
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {

    }
}
