import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Component, Injector } from '@angular/core';
import {
    DepartmentServiceProxy,
    InstructorServiceProxy,
    InstructorShortInfoDto,
    CreateDepartnmentDto
} from '@shared/service-proxies/service-proxies';
import { MatDialogRef } from '@angular/material';


@Component({
    templateUrl: './create-department-dialog.component.html'
})
export class CreateDepartmentDialogComponent extends AppComponentBase implements OnInit {
    instructors: InstructorShortInfoDto[] = [];
    department: CreateDepartnmentDto = new CreateDepartnmentDto();
    selectedAdministrator: number;
    saving = false;
    isLoading = false;


    constructor(
        injector: Injector,
        private _departmentService: DepartmentServiceProxy,
        private _instructorService: InstructorServiceProxy,
        private _dialogRef: MatDialogRef<CreateDepartmentDialogComponent>
    ) {
        super(injector);
    }

    async ngOnInit() {
        this.isLoading = true;
        const instructors = await this._instructorService
            .getAll(undefined, undefined, undefined, undefined, undefined, undefined).toPromise();
        this.isLoading = false;
        this.instructors = instructors.items;
    }

    async save() {
        this.saving = true;
        this.department.instructorId = this.selectedAdministrator;
        await this._departmentService.create(this.department).toPromise();
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
        this.close(true);
    }

    close(res: any) {
        this._dialogRef.close(res);
    }
}
