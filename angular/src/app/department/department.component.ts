import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Component, Injector } from '@angular/core';
import {
    DepartmentServiceProxy,
    DepartmentShortInfoDto,
    InstructorServiceProxy,
    InstructorShortInfoDto
} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { MatDialog } from '@angular/material';
import { CreateDepartmentDialogComponent } from './create-department/create-department-dialog.component';
import { UpdateDepartmentDialogComponent } from './update-department/update-department-dialog.component';


class Department extends DepartmentShortInfoDto {
    administratorName: string;
}

@Component({
    templateUrl: './department.component.html',
    animations: [appModuleAnimation()]
})
export class DepartmentComponent extends AppComponentBase implements OnInit {
    isLoading = false;
    departments: Department[] = [];
    instructors: InstructorShortInfoDto[] = [];

    constructor(
        injector: Injector,
        private _departmentService: DepartmentServiceProxy,
        private _instructorService: InstructorServiceProxy,
        private _dialog: MatDialog
    ) {
        super(injector);
    }

    ngOnInit() {
        this.list();
    }

    refresh() {
        this.list();
    }

    async delete(department: Department) {
        abp.message.confirm(
            this.l('DeleteDepartmentWarrningMessage', department.name),
            async (res) => {
                if (res) {
                    await this._departmentService.delete(department.id).toPromise();
                    abp.notify.success(this.l('SuccessfullyDeleted'));
                    this.refresh();
                }
            }
        );
    }

    async editDepartment(department: Department) {
        const res = await this._dialog.open(UpdateDepartmentDialogComponent, { data: department.id }).afterClosed().toPromise();
        if (res) {
            this.list();
        }
    }

    async createDepartment() {
        const res = await this._dialog.open(CreateDepartmentDialogComponent).afterClosed().toPromise();
        if (res) {
            this.list();
        }
    }

    async list() {
        this.isLoading = true;
        const departments = this._departmentService
            .getAll(undefined, undefined, undefined, undefined, undefined, undefined, undefined).toPromise();
        const instructors = this._instructorService.getAll(undefined, undefined, undefined, undefined, undefined, undefined).toPromise();
        const pair = await Promise.all([departments, instructors]);
        this.isLoading = false;
        this.departments = pair[0].items as Department[];
        this.instructors = pair[1].items;
        this.departments.forEach(dep => {
            const i = this.instructors.find(ins => ins.id === dep.instructorId);
            if (i != null) { dep.administratorName = i.firstName; }
        });
    }
}
