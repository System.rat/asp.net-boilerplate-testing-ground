import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Component, Injector, Optional, Inject } from '@angular/core';
import {
    DepartmentServiceProxy,
    InstructorServiceProxy,
    InstructorShortInfoDto,
    UpdateDepartmentDto } from '@shared/service-proxies/service-proxies';
import { MatDialogRef, MAT_DIALOG_DATA, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import * as m from 'moment';
import { FormControl } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';

@Component({
    templateUrl: './update-department-dialog.component.html',
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})
export class UpdateDepartmentDialogComponent extends AppComponentBase implements OnInit {
    instructors: InstructorShortInfoDto[] = [];
    department: UpdateDepartmentDto = new UpdateDepartmentDto();
    selectedAdministrator: number;
    saving = false;
    isLoading = false;
    startDate: FormControl = new FormControl(m(), { updateOn: 'blur'});

    constructor(
        injector: Injector,
        private _departmentService: DepartmentServiceProxy,
        private _instructorService: InstructorServiceProxy,
        private _dialogRef: MatDialogRef<UpdateDepartmentDialogComponent>,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
    }

    async ngOnInit() {
        this.isLoading = true;
        const ins = this._instructorService.getAll(undefined, undefined, undefined, undefined, undefined, undefined).toPromise();
        const dep = this._departmentService.get(this._id).toPromise();

        const pair = await Promise.all([ins, dep]);

        this.isLoading = false;
        this.instructors = pair[0].items;
        this.department.id = this._id;
        this.department.name = pair[1].name;
        this.department.budget = pair[1].budget;
        this.department.startDate = pair[1].startDate;
        this.startDate.setValue(pair[1].startDate);
        this.department.instructorId = pair[1].instructorId;
        if (pair[1].instructorId != null) { this.selectedAdministrator = pair[1].instructorId; }
    }

    async save() {
        this.saving = true;
        this.department.instructorId = this.selectedAdministrator;
        // Timezones will be the end of me
        this.department.startDate = (this.startDate.value as m.Moment).add((this.startDate.value as m.Moment).utcOffset(), 'minutes');
        await this._departmentService.update(this.department).toPromise();
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
        this.close(true);
    }

    close(res: any) {
        this._dialogRef.close(res);
    }
}
