import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';
import {
    CourseServiceProxy,
    DepartmentServiceProxy,
    DepartmentShortInfoDto,
    CreateCourseDto } from '@shared/service-proxies/service-proxies';


@Component({
    templateUrl: './create-course-dialog.component.html'
})
export class CreateCourseDialogComponent extends AppComponentBase implements OnInit {
    departments: DepartmentShortInfoDto[] = [];
    course: CreateCourseDto = new CreateCourseDto();
    selectedDepartment: number;
    saving = false;

    constructor(
        injector: Injector,
        private _courseService: CourseServiceProxy,
        private _departmentService: DepartmentServiceProxy,
        private _dialogRef: MatDialogRef<CreateCourseDialogComponent>
    ) {
        super(injector);
    }

    async ngOnInit() {
        const res = await this._departmentService
            .getAll(undefined, undefined, undefined, undefined, undefined, undefined, undefined).toPromise();
        this.departments = res.items;
        console.log(this.departments);
    }

    async save() {
        this.saving = true;
        this.course.departnmentId = this.selectedDepartment;
        await this._courseService.create(this.course).toPromise();
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
        this.close(true);
    }

    close(res: any) {
        this._dialogRef.close(res);
    }
}
