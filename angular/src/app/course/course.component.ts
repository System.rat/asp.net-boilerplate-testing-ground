import { Component, Injector, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/app-component-base';
import { CourseServiceProxy, CourseInfoDto } from '@shared/service-proxies/service-proxies';
import { CreateCourseDialogComponent } from './create-course/create-course-dialog.component';
import { ListEnrollmentsDialogComponent } from './list-enrollments/list-enrollments-dialog.component';

@Component({
    templateUrl: './course.component.html',
    animations: [appModuleAnimation()],
    styles: [

    ]
})
export class CourseComponent extends AppComponentBase implements OnInit {
    courses: CourseInfoDto[] = [];
    isLoading = false;

    constructor(
        private _courseService: CourseServiceProxy,
        private _dialog: MatDialog,
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {
        this.list();
    }

    refresh() {
        this.list();
    }

    async delete(course: CourseInfoDto) {
        abp.message.confirm(
            this.l('DeleteCourseWarningMessage', course.title),
            async (res) => {
                if (res) {
                    await this._courseService.delete(course.id).toPromise();
                    abp.notify.success(this.l('SuccessfullyDeleted'));
                    this.list();
                }
            }
        );
    }

    async createCourse() {
        const res = await this._dialog.open(CreateCourseDialogComponent).afterClosed().toPromise();
        if (res) {
            this.list();
        }
    }

    listEnrollments(course: CourseInfoDto) {
        this._dialog.open(ListEnrollmentsDialogComponent, { data: course.id });
    }

    async list() {
        this.isLoading = true;
        const res = await this._courseService.getAll(undefined, undefined, undefined, undefined, undefined).toPromise();
        this.isLoading = false;
        this.courses = res.items;
    }
}
