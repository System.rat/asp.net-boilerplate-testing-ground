import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Component, Injector, Optional, Inject } from '@angular/core';
import { CourseServiceProxy,
    StudentServiceProxy,
    EnrollmentInfoCourseDto,
    StudentShortInfoDto } from '@shared/service-proxies/service-proxies';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

class Enrollment extends EnrollmentInfoCourseDto {
    student: StudentShortInfoDto = null;
    gradeLettered: string = null;
}

export enum Grade {
    A = 0,
    B = 1,
    C = 2,
    D = 3,
    F = 4
}

@Component({
    templateUrl: 'list-enrollments-dialog.component.html'
})
export class ListEnrollmentsDialogComponent extends AppComponentBase implements OnInit {
    enrollments: Enrollment[] = [];
    students: StudentShortInfoDto[] = [];
    isLoading = false;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<ListEnrollmentsDialogComponent>,
        private _courseService: CourseServiceProxy,
        private _studentService: StudentServiceProxy,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
    }

    async ngOnInit() {
        this.isLoading = true;
        const enrollments = this._courseService.getEnrollments(this._id).toPromise();
        const students = this._studentService.getAll(undefined, undefined, undefined, undefined, undefined, undefined).toPromise();
        const [e, s] = await Promise.all([enrollments, students]);
        this.isLoading = false;
        this.enrollments = e.items as Enrollment[];
        this.students = s.items;
        console.log(`Student array length: ${s.items.length}`);
        console.log(this.students);
        this.enrollments.forEach((enrollment) => {
            enrollment.student = this.students.find((stu) => { return stu.id === enrollment.studentId; });
            enrollment.gradeLettered = Grade[enrollment.grade];
        });
    }

    close() {
        this._dialogRef.close();
    }
}
