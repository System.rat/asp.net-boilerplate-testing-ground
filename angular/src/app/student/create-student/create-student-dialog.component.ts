import { Component, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef } from '@angular/material';
import { CreateStudentDto, StudentServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';


@Component({
    templateUrl: './create-student-dialog.component.html'
})
export class CreateStudentDialogComponent extends AppComponentBase {
    student: CreateStudentDto = new CreateStudentDto();
    saving = false;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<CreateStudentDialogComponent>,
        public _studentService: StudentServiceProxy
    ) {
        super(injector);
    }

    async save() {
        this.saving = true;
        await this._studentService.create(this.student).toPromise();
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
        this.close(true);
    }

    close(result: any) {
        this._dialogRef.close(result);
    }
}
