import { Component, Injector, OnInit, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserDto, UserServiceProxy, StudentServiceProxy, StudentSetUserDto } from '@shared/service-proxies/service-proxies';


@Component({
    templateUrl: './set-user-dialog.component.html'
})
export class SetUserStudentDialogComponent extends AppComponentBase implements OnInit {
    users: UserDto[] = [];
    setUserDto: StudentSetUserDto = new StudentSetUserDto();
    saving = false;
    selectedUser: number;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<SetUserStudentDialogComponent>,
        public _studentService: StudentServiceProxy,
        public _userService: UserServiceProxy,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
    }

    async save() {
        this.setUserDto.studentId = this._id;
        this.setUserDto.userId = this.selectedUser;
        this.saving = true;
        let res = await this._studentService.setUser(this.setUserDto).toPromise();
        this.saving = false;
        if (res) { this.notify.success(this.l('SavedSuccessfully')); } else { this.notify.error(this.l('Error')); }
        this.close(true);
    }

    async ngOnInit() {
        const res = await this._userService.getAll(undefined, undefined, undefined, undefined).toPromise();
        this.users = res.items;
    }

    close(result: any) {
        this._dialogRef.close(result);
    }
}
