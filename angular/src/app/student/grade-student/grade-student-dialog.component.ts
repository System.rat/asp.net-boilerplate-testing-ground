import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Component, Injector, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS, _MatChipListMixinBase } from '@angular/material';
import {
    InstructorServiceProxy,
    StudentServiceProxy,
    CourseServiceProxy,
    InstructorShortInfoDto,
    CourseInfoDto,
    GivePointsStudentDto,
    PointsListDto,
    EnrollmentShortInfoDto} from '@shared/service-proxies/service-proxies';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import * as m from 'moment';


@Component({
    templateUrl: 'grade-student-dialog.component.html',
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})
export class GradeStudentDialogComponent extends AppComponentBase implements OnInit {
    instructors: InstructorShortInfoDto[] = [];
    courses: CourseInfoDto[] = [];
    selectedInstructor: number;
    selectedCourse: number;
    saving = false;
    isLoading = true;
    pointsToGive: GivePointsStudentDto = new GivePointsStudentDto();
    pointsDate: FormControl = new FormControl(m(), { updateOn: 'blur' });
    points: PointsListDto[] = [];
    studentEnrollments: EnrollmentShortInfoDto[] = [];
    courseFilter: number;


    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<GradeStudentDialogComponent>,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number,
        private _instructorService: InstructorServiceProxy,
        private _studentService: StudentServiceProxy,
        private _courseService: CourseServiceProxy
    ) {
        super(injector);
    }


    async ngOnInit() {
        const ins = this._instructorService
        .getAll(undefined, undefined, undefined, undefined, undefined, undefined).toPromise();
        const poi = this._studentService.getPoints(this._id, undefined).toPromise();
        const stc = this._studentService.getEnrolledCourses(this._id).toPromise();
        const [instructors, points, studentC] = await Promise.all([ins, poi, stc]);

        this.isLoading = false;
        this.instructors = instructors.items;
        this.points = points.items;
        this.studentEnrollments = studentC.items;
        console.log(this.studentEnrollments);
    }

    async updatePoints() {
        this.isLoading = true;
        const res = await this._studentService.getPoints(this._id, this.courseFilter).toPromise();
        this.isLoading = false;
        this.points = res.items;
    }

    async loadCourses() {
        if (this.selectedInstructor == null || this.selectedInstructor === undefined) {
            this.isLoading = true;
        }
        const res = await this._courseService.getAllAllowed(this._id, this.selectedInstructor).toPromise();
        this.isLoading = false;
        this.courses = res.items;
    }

    async save() {
        this.saving = true;
        this.pointsToGive.instructorId = this.selectedInstructor;
        this.pointsToGive.studentId = this._id;
        this.pointsToGive.courseId = this.selectedCourse;
        const gd = this.pointsDate.value as m.Moment;
        this.pointsToGive.dateGraded = gd.add(gd.utcOffset(), 'minutes');
        try {
            const res = await this._studentService.givePoints(this.pointsToGive).toPromise();
            this.saving = false;
            this.notify.success('SavedSuccessfully');
            this.close(true);
        } catch {
            this.notify.error('StudentInstructorNotAssigned');
            this.close(false);
        }
    }

    finalGrade(): number {
        let total = 0;
        this.points.forEach(p => {
            total += p.pointCount;
        });
        if (total > 91) {
           return 10;
        } else if (total > 81) {
            return 9;
        } else if (total > 71) {
            return 8;
        } else if (total > 61) {
            return 7;
        } else if (total > 51) {
            return 6;
        } else {
            return 0;
        }
    }

    total(): number {
        let total = 0;
        this.points.forEach((p) => {
            total += p.pointCount;
        });
        return total;
    }

    close(res: any) {
        this._dialogRef.close(res);
    }
}
