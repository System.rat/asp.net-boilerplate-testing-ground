import { Component, Injector, OnInit, Optional, Inject } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CourseServiceProxy, CourseInfoDto, StudentServiceProxy, EnrollStudentDto } from '@shared/service-proxies/service-proxies';
import { Grade } from '@app/course/list-enrollments/list-enrollments-dialog.component';


@Component({
    templateUrl: './enroll-dialog.component.html'
})
export class EnrollDialogComponent extends AppComponentBase implements OnInit {
    courses: CourseInfoDto[] = [];
    enrollment: EnrollStudentDto = new EnrollStudentDto();
    saving = false;
    selectedCourse: number;
    selectedGrade: number;
    grades: {value: number, name: string}[] = [
        {value: 0, name: 'A'},
        {value: 1, name: 'B'},
        {value: 2, name: 'C'},
        {value: 3, name: 'D'},
        {value: 4, name: 'F'},
    ];

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<EnrollDialogComponent>,
        public _studentService: StudentServiceProxy,
        public _courseService: CourseServiceProxy,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
        // this.enumToString()
        this.enrollment.studentId = this._id;
    }

    enumToString() {
        for (const grade in Grade) {
            if (typeof(grade) === 'number') {
                this.grades.push({
                    name: '',
                    value: grade
                });
            }
        }
        let i = 0;
        for (const grade in Grade) {
            if (typeof(grade) === 'string') {
                this.grades[i].name = grade;
                i++;
            }
        }
        console.log(this.grades);
    }

    async save() {
        this.enrollment.courseId = this.selectedCourse;
        this.enrollment.grade = this.selectedGrade;
        this.saving = true;
        await this._studentService.enroll(this.enrollment).toPromise();
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
        this.close(true);
    }

    async ngOnInit() {
        const res = await this._courseService.getAll(undefined, undefined, undefined, undefined, undefined).toPromise();
        this.courses = res.items;
        console.log(this.courses);
    }

    close(result: any) {
        this._dialogRef.close(result);
    }
}
