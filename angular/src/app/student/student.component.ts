import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Component, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { StudentServiceProxy, StudentShortInfoDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { CreateStudentDialogComponent } from './create-student/create-student-dialog.component';
import { UpdateStudentDialogComponent } from './edit-student/edit-student-dialog.component';
import { EnrollDialogComponent } from './enroll/enroll-dialog.component';
import { GradeStudentDialogComponent } from './grade-student/grade-student-dialog.component';
import { SetUserStudentDialogComponent } from './set-user/set-user-dialog.component';


@Component({
    templateUrl: './student.component.html',
    animations: [appModuleAnimation()]
})
export class StudentComponent extends AppComponentBase implements OnInit {
    students: StudentShortInfoDto[] = [];
    isLoading = false;


    constructor(
        injector: Injector,
        private _studentService: StudentServiceProxy,
        private _dialog: MatDialog
    ) {
        super(injector);
    }

    refresh() {
        this.list();
    }

    ngOnInit() {
        this.list();
    }

    createStudent() {
        this._dialog.open(CreateStudentDialogComponent)
        .afterClosed()
        .subscribe((res) => {
            if (res) { this.list(); }
        });
    }

    editStudent(student: StudentShortInfoDto) {
        this._dialog.open(UpdateStudentDialogComponent, {data: student.id})
        .afterClosed()
        .subscribe((res) => {
            if (res) { this.list(); }
        });
    }

    grade(student: StudentShortInfoDto) {
        this._dialog.open(GradeStudentDialogComponent, { data: student.id })
        .afterClosed()
        .subscribe((res) => {
            if (res) { this.list(); }
        });
    }

    enroll(student: StudentShortInfoDto) {
        this._dialog.open(EnrollDialogComponent, { data: student.id })
        .afterClosed()
        .subscribe((res) => {
            if (res) { this.list(); }
        });
    }

    async setUser(student: StudentShortInfoDto) {
        await this._dialog.open(SetUserStudentDialogComponent, { data: student.id }).afterClosed().toPromise();
    }

    list() {
        this.isLoading = true;
        this._studentService
        .getAll(undefined, undefined, undefined, undefined, undefined, undefined)
        .pipe(finalize(() => {
            this.isLoading = false;
        }))
        .subscribe((res) => {
            this.students = res.items;
        });
    }
}
