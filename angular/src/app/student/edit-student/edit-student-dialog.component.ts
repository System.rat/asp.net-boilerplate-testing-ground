import { Component, Injector, Optional, Inject, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef, MAT_DIALOG_DATA, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { UpdateStudentDto, StudentServiceProxy } from '@shared/service-proxies/service-proxies';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import * as m from 'moment';


@Component({
    templateUrl: './edit-student-dialog.component.html',
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})
export class UpdateStudentDialogComponent extends AppComponentBase implements OnInit {
    student: UpdateStudentDto = new UpdateStudentDto();
    enrollment: FormControl = new FormControl();
    saving = false;
    isLoading = false;


    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<UpdateStudentDialogComponent>,
        public _studentService: StudentServiceProxy,
        @Optional()
        @Inject(MAT_DIALOG_DATA)
        private _id: number
    ) {
        super(injector);
    }

    async ngOnInit() {
        this.isLoading = true;
        const res = await this._studentService.get(this._id).toPromise();
        this.isLoading = false;
        this.student.id = this._id;
        this.student.firstName = res.firstName;
        this.student.lastName = res.lastName;
        this.student.enrollmentDate = res.enrollmentDate;
        this.enrollment.setValue(res.enrollmentDate);
    }

    async save() {
        this.saving = true;
        this.student.enrollmentDate = (this.enrollment.value as m.Moment).add((this.enrollment.value as m.Moment).utcOffset(), 'minutes');
        await this._studentService.update(this.student).toPromise();
        this.saving = false;
        this.notify.success(this.l('SavedSuccessfully'));
        this.close(true);
    }

    close(result: any) {
        this._dialogRef.close(result);
    }
}
