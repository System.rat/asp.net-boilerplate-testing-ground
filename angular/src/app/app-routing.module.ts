import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { InstructorComponent } from './instructor/instructor.component';
import { CourseComponent } from './course/course.component';
import { DepartmentComponent } from './department/department.component';
import { StudentComponent } from './student/student.component';
import { BoardComponent } from './board/board.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AppComponent,
                data: { permission: 'Administration' },
                canActivate: [AppRouteGuard],
                children: [
                    { path: 'home', component: HomeComponent },
                    { path: 'users', component: UsersComponent, data: { permission: 'Pages.Users' } },
                    { path: 'roles', component: RolesComponent, data: { permission: 'Pages.Roles' } },
                    { path: 'tenants', component: TenantsComponent, data: { permission: 'Pages.Tenants' } },
                    { path: 'about', component: AboutComponent },
                    { path: 'update-password', component: ChangePasswordComponent },
                    { path: 'instructor', component: InstructorComponent,
                    data: { permission: 'Pages.Instructor'} },
                    { path: 'course', component: CourseComponent, data: { permission: 'Pages.Course' } },
                    { path: 'department', component: DepartmentComponent,
                    data: { permission: 'Pages.Department' } },
                    { path: 'student', component: StudentComponent, data: { permission: 'Pages.Student' } },
                    { path: 'board', component: BoardComponent }
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
