import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AbpModule } from '@abp/abp.module';

import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';

import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
import { TopBarComponent } from '@app/layout/topbar.component';
import { TopBarLanguageSwitchComponent } from '@app/layout/topbar-languageswitch.component';
import { SideBarUserAreaComponent } from '@app/layout/sidebar-user-area.component';
import { SideBarNavComponent } from '@app/layout/sidebar-nav.component';
import { SideBarFooterComponent } from '@app/layout/sidebar-footer.component';
import { RightSideBarComponent } from '@app/layout/right-sidebar.component';
// tenants
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantDialogComponent } from './tenants/create-tenant/create-tenant-dialog.component';
import { EditTenantDialogComponent } from './tenants/edit-tenant/edit-tenant-dialog.component';
// roles
import { RolesComponent } from '@app/roles/roles.component';
import { CreateRoleDialogComponent } from './roles/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './roles/edit-role/edit-role-dialog.component';
// users
import { UsersComponent } from '@app/users/users.component';
import { CreateUserDialogComponent } from '@app/users/create-user/create-user-dialog.component';
import { EditUserDialogComponent } from '@app/users/edit-user/edit-user-dialog.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { ResetPasswordDialogComponent } from './users/reset-password/reset-password.component';

// instructors
import { InstructorComponent } from './instructor/instructor.component';
import { CreateInstructorDialogComponent } from './instructor/create-instructor/create-instructor-dialog.component';
import { InstructorEditDialogComponent } from './instructor/edit-instructor/edit-instructor-dialog.component';
import { SetOfficeDialogComponent } from './instructor/set-office/set-office-dialog.component';
import { AddCourseInstructorDialogComponent } from './instructor/add-course/add-course-dialog.component';
import { CourseComponent } from './course/course.component';
import { CreateCourseDialogComponent } from './course/create-course/create-course-dialog.component';
import { ListEnrollmentsDialogComponent } from './course/list-enrollments/list-enrollments-dialog.component';
import { DepartmentComponent } from './department/department.component';
import { CreateDepartmentDialogComponent } from './department/create-department/create-department-dialog.component';
import { UpdateDepartmentDialogComponent } from './department/update-department/update-department-dialog.component';
import { StudentComponent } from './student/student.component';
import { CreateStudentDialogComponent } from './student/create-student/create-student-dialog.component';
import { UpdateStudentDialogComponent } from './student/edit-student/edit-student-dialog.component';
import { EnrollDialogComponent } from './student/enroll/enroll-dialog.component';
import { InstructorDetailsDialogComponent } from './instructor/details/details-dialog.component';
import { GradeStudentDialogComponent } from './student/grade-student/grade-student-dialog.component';
import { BoardComponent } from './board/board.component';
import { SetUserInstructorDialogComponent } from './instructor/set-user/set-user-dialog.component';
import { SetUserStudentDialogComponent } from './student/set-user/set-user-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    TopBarComponent,
    TopBarLanguageSwitchComponent,
    SideBarUserAreaComponent,
    SideBarNavComponent,
    SideBarFooterComponent,
    RightSideBarComponent,
    // tenants
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
    // instructors
    InstructorComponent,
    CreateInstructorDialogComponent,
    InstructorEditDialogComponent,
    SetOfficeDialogComponent,
    AddCourseInstructorDialogComponent,
    InstructorDetailsDialogComponent,
    SetUserInstructorDialogComponent,
    // courses
    CourseComponent,
    CreateCourseDialogComponent,
    ListEnrollmentsDialogComponent,
    // departments
    DepartmentComponent,
    CreateDepartmentDialogComponent,
    UpdateDepartmentDialogComponent,
    // students
    StudentComponent,
    CreateStudentDialogComponent,
    UpdateStudentDialogComponent,
    EnrollDialogComponent,
    GradeStudentDialogComponent,
    SetUserStudentDialogComponent,
    // boards
    BoardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forRoot(),
    AbpModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule
  ],
  providers: [],
  entryComponents: [
    // tenants
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,
    // instructors
    CreateInstructorDialogComponent,
    InstructorEditDialogComponent,
    SetOfficeDialogComponent,
    AddCourseInstructorDialogComponent,
    InstructorDetailsDialogComponent,
    SetUserInstructorDialogComponent,
    // courses
    CreateCourseDialogComponent,
    ListEnrollmentsDialogComponent,
    // departments
    CreateDepartmentDialogComponent,
    UpdateDepartmentDialogComponent,
    // students
    CreateStudentDialogComponent,
    UpdateStudentDialogComponent,
    EnrollDialogComponent,
    GradeStudentDialogComponent,
    SetUserStudentDialogComponent,
  ]
})
export class AppModule {}
