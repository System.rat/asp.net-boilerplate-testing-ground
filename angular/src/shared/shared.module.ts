import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AbpModule } from '@abp/abp.module';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppSessionService } from './session/app-session.service';
import { AppUrlService } from './nav/app-url.service';
import { AppAuthService } from './auth/app-auth.service';
import { AppRouteGuard } from './auth/auth-route-guard';
import { AbpPaginationControlsComponent } from './pagination/abp-pagination-controls.component';
import { LocalizePipe } from '@shared/pipes/localize.pipe';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import {
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    MatProgressBar,
} from '@angular/material';
import { BlockDirective } from './directives/block.directive';
import { BusyDirective } from './directives/busy.directive';
import { EqualValidator } from './directives/equal-validator.directive';
import { NotificationsComponent } from './notifications/notifications.component';
import { ThemeSwitcherComponent } from './layout/theme-switcher.component';
import { PrivateMessagesComponent } from './private-messages/private-messages.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RepliesComponent } from './private-messages/replies/replies.component';
import { EntityInfoComponent } from './account/entity-info.component';
import { EntityAccountComponent } from './account/entity-account.component';
import { NavigationComponent } from './layout/nagivation.component';
import { RefreshablePanelComponent } from './refreshable-panel/refreshable-panel.component';
import { RefreshablePanelContentComponent } from './refreshable-panel/refreshable-panel-content.component';
import { RefreshablePanelHeaderComponent } from './refreshable-panel/refreshable-panel-header.component';
import { RefreshablePanelActionsComponent } from './refreshable-panel/refreshable-panel-actions.component';
import { LoadingDirective } from './directives/loading.directive';
@NgModule({
    imports: [
        CommonModule,
        AbpModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatStepperModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTreeModule,
    ],
    declarations: [
        AbpPaginationControlsComponent,
        LocalizePipe,
        BlockDirective,
        BusyDirective,
        LoadingDirective,
        EqualValidator,
        NotificationsComponent,
        ThemeSwitcherComponent,
        PrivateMessagesComponent,
        RepliesComponent,
        EntityInfoComponent,
        EntityAccountComponent,
        NavigationComponent,
        RefreshablePanelComponent,
        RefreshablePanelContentComponent,
        RefreshablePanelHeaderComponent,
        RefreshablePanelActionsComponent
    ],
    exports: [
        AbpPaginationControlsComponent,
        EntityInfoComponent,
        LocalizePipe,
        BlockDirective,
        BusyDirective,
        LoadingDirective,
        EqualValidator,
        CdkTableModule,
        CdkTreeModule,
        DragDropModule,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatStepperModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTreeModule,
        ScrollingModule,
        NotificationsComponent,
        ThemeSwitcherComponent,
        NavigationComponent,
        RefreshablePanelComponent,
        RefreshablePanelContentComponent,
        RefreshablePanelHeaderComponent,
        RefreshablePanelActionsComponent
    ],
    entryComponents: [
        MatProgressBar
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                AppSessionService,
                AppUrlService,
                AppAuthService,
                AppRouteGuard
            ]
        };
    }
}
