import {
    CanActivate, ActivatedRouteSnapshot
} from '@angular/router';
import { AppSessionService } from '@shared/session/app-session.service';
import { UserLoginInfoDtoUserEntityType } from '@shared/service-proxies/service-proxies';

export class UserEntityGuard implements CanActivate {

    constructor(
        private _sessionService: AppSessionService
    ) {

    }
    canActivate = (route: ActivatedRouteSnapshot) =>
        route['_routerState'].url.indexOf('/student') === 0
        && this._sessionService.user.userEntityType === UserLoginInfoDtoUserEntityType._0
        ||
        route['_routerState'].url.indexOf('/instructor') === 0
        && this._sessionService.user.userEntityType === UserLoginInfoDtoUserEntityType._1
}
