import { InjectionToken } from '@angular/core';
import { MenuItem } from '@shared/layout/menu-item';

export enum EntityType {
    Student = 0,
    Instructor = 1
}

export interface Entity {
    id: number;
    type: EntityType;
    name: string;
}

export const ENTITY_SERVICE_TOKEN = new InjectionToken('App.EntityService');

export interface EntityService {
    routes: MenuItem[];
    getEntities(): Promise<Entity[]>;
    getInfoFromUserId(id: number): Promise<{ type: string, data: Map<string, string> }>;
}
