import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Injector, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
    BoardUploadedFileDto,
    FileServiceProxy,
    SharedPrivateMessageDetailDto,
    SharedPrivateMessageReplyDto,
    SharedAttachFileDto,
    SharedAttachFileDtoMessageType,
    SharedUploadedFileDto,
    PrivateMessageServiceProxy
} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { saveAs } from 'file-saver';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './replies.component.html',
    animations: [appModuleAnimation()]
})
export class RepliesComponent extends AppComponentBase implements
    OnInit,
    DataProvider<SharedPrivateMessageDetailDto> {
    messageId: number;
    routerLink: string;
    messageDetail: SharedPrivateMessageDetailDto = new SharedPrivateMessageDetailDto();
    sending = false;
    needsRefresh = new Subject<void>();
    newReply: SharedPrivateMessageReplyDto = new SharedPrivateMessageReplyDto();
    files: FileList | undefined = undefined;

    constructor(
        injector: Injector,
        private _route: ActivatedRoute,
        private _messageService: PrivateMessageServiceProxy,
        private _fileService: FileServiceProxy
    ) {
        super(injector);
        this.messageDetail.uploadedFiles = [];
        this.messageDetail.replies = [];
    }

    getData = () => this._messageService.getPrivateMessage(this.messageId).toPromise();

    onRefresh = (data: SharedPrivateMessageDetailDto) => this.messageDetail = data;

    async ngOnInit() {
        this._route.paramMap.subscribe(async (params) => {
            // tslint:disable-next-line: radix
            this.messageId = Number.parseInt(params.get('id'));
        });
        this._route.data.subscribe((data: { routerLink: string }) => {
            this.routerLink = data.routerLink;
        });
    }

    async downloadFile(file: BoardUploadedFileDto) {
        const res = await this._fileService.downloadAttachment(file.attachmentId).toPromise();
        saveAs(res.data, file.fileName);
    }

    onFileChange(event) {
        this.files = event.srcElement.files;
    }

    async reply() {
        this.sending = true;
        this.newReply.privateBoardMessageId = this.messageId;
        const res = await this._messageService.replyPrivateMessage(this.newReply).toPromise();
        this.newReply = new SharedPrivateMessageReplyDto();
        if (this.files !== undefined) {
            const promises: Promise<SharedUploadedFileDto>[] = [];
            for (let i = 0; i < this.files.length; i++) {
                promises.push(this._messageService.uploadFile({
                    data: this.files[i],
                    fileName: this.files[i].name
                }).toPromise());
            }
            const fresArr = await Promise.all(promises);
            await Promise.all(fresArr.map((fres) => {
                console.log(fres);
                return this._messageService.attachFile({
                    fileId: fres.id,
                    messageId: res.id,
                    messageType: SharedAttachFileDtoMessageType._2
                } as SharedAttachFileDto).toPromise();
            }));
        }
        abp.notify.success(this.l('ReplySent'));
        this.sending = false;
        this.needsRefresh.next();
    }
}
