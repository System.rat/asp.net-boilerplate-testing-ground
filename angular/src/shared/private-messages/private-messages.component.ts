import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Injector, Component, Inject } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
    SharedPrivateMessageListDto,
    SharedPrivateMessageDto,
    SharedAttachFileDto,
    SharedAttachFileDtoMessageType,
    SharedUploadedFileDto,
    PrivateMessageServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { EntityService, Entity, ENTITY_SERVICE_TOKEN } from './entity-provider';
import { ActivatedRoute } from '@angular/router';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';
import { ActionDataProvider } from '@shared/refreshable-panel/refreshable-panel-actions.component';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './private-messages.component.html',
    animations: [appModuleAnimation()]
})
export class PrivateMessagesComponent extends AppComponentBase implements
    OnInit,
    DataProvider<SharedPrivateMessageListDto[]>,
    ActionDataProvider<Entity[]> {
    routerLink: string;
    shouldInit = true;
    messages: SharedPrivateMessageListDto[] = [];
    newPrivateMessage: SharedPrivateMessageDto = new SharedPrivateMessageDto();
    targets: Entity[] = [];
    selectedTarget: Entity;
    files: FileList | undefined = undefined;
    sending = false;
    needsRefresh = new Subject<void>();

    constructor(
        injector: Injector,
        private _route: ActivatedRoute,
        private _messageService: PrivateMessageServiceProxy,
        @Inject(ENTITY_SERVICE_TOKEN)
        private _entityService: EntityService
    ) {
        super(injector);
    }

    ngOnInit() {
        this._route.data.subscribe((data: { routerLink: string }) => {
            this.routerLink = data.routerLink;
        });
    }

    onFileChange(event) {
        this.files = event.srcElement.files;
    }

    getActionData = () => this._entityService.getEntities();

    onActionRefresh = (data) => this.targets = data;

    getData = async () => (await this._messageService.getPrivateMessages().toPromise()).items;

    onRefresh = (data) => this.messages = data;

    async send() {
        this.sending = true;
        let msgId: number;
        this.newPrivateMessage.targetId = this.selectedTarget.id;
        this.newPrivateMessage.targetType = <number>this.selectedTarget.type;
        const res = await this._messageService.sendPrivateMessage(this.newPrivateMessage).toPromise();
        msgId = res.id;
        if (this.files !== undefined) {
            const promises: Promise<SharedUploadedFileDto>[] = [];
            for (let i = 0; i < this.files.length; i++) {
                promises.push(this._messageService.uploadFile({
                    data: this.files[i],
                    fileName: this.files[i].name
                }).toPromise());
            }
            const fresArr = await Promise.all(promises);
            await Promise.all(fresArr.map(fres => {
                console.log(fres);
                return this._messageService.attachFile({
                    fileId: fres.id,
                    messageId: msgId,
                    messageType: SharedAttachFileDtoMessageType._1
                } as SharedAttachFileDto).toPromise();
            }));
        }
        abp.notify.success(this.l('MessageSent'));
        this.needsRefresh.next();
        this.sending = false;
    }
}
