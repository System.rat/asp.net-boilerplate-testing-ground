import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Component, Injector, ViewEncapsulation, Inject } from '@angular/core';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { EntityService, ENTITY_SERVICE_TOKEN } from '@shared/private-messages/entity-provider';

@Component({
    templateUrl: './entity-info.component.html',
    selector: 'entity-info',
    encapsulation: ViewEncapsulation.None
})
export class EntityInfoComponent extends AppComponentBase implements OnInit {
    entityInfo: Map<string, string> = new Map<string, string>();
    isLoading = true;
    entityType = '';

    constructor(
        injector: Injector,
        @Inject(ENTITY_SERVICE_TOKEN)
        private _entityService: EntityService,
        private _authService: AppAuthService
    ) {
        super(injector);
    }

    logout() {
        this._authService.logout();
    }

    async ngOnInit() {
        const res = await this._entityService.getInfoFromUserId(abp.session.userId);
        this.entityType = res.type;
        this.isLoading = false;
        this.entityInfo = res.data;
    }
}
