import { AppComponentBase } from '@shared/app-component-base';
import { Component, Injector, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { SignalRAspNetCoreHelper } from '@shared/helpers/SignalRAspNetCoreHelper';
import { Notification, NotificationsComponent } from '@shared/notifications/notifications.component';

@Component({
    templateUrl: './entity-account.component.html',
    styles: [
        `
        .overlay {
            background-color: rgba(0, 0, 0, 0.1);
        }
        `
    ]
})
export class EntityAccountComponent  extends AppComponentBase implements OnInit {
    notifications: Notification[] = [];
    @ViewChild(NotificationsComponent, {static: false})
    notificationsComponent: NotificationsComponent;
    areNotificationsOn = false;

    constructor(
        injector: Injector,
        private _changeDetector: ChangeDetectorRef
    ) {
        super(injector);
    }

    ngOnInit() {
        SignalRAspNetCoreHelper.initSignalR();

        abp.event.on('abp.notifications.received', userNotification => {
            abp.notifications.showUiNotifyForUserNotification(userNotification);
            this.notifications = [...this.notifications, ({
                text: this.localization
                .getSource(userNotification.notification.data.message.sourceName)
                (userNotification.notification.data.message.name),
                data: userNotification.notification.data,
                type: 'points'
            })];
            this._changeDetector.detectChanges();
            this.notificationsComponent.changeDetector.detectChanges();
            Push.create('AbpZeroTemplate', {
                body: this.localization
                .getSource(userNotification.notification.data.message.sourceName)
                (userNotification.notification.data.message.name),
                icon: abp.appPath + 'assets/app-logo-small.png',
                timeout: 6000,
                onClick: function () {
                    window.focus();
                    this.close();
                }
            });
        });
    }

    hideNotifications() {
        if (this.notificationsComponent.isOpen) {
            this.notificationsComponent.toggleNotifications();
            this.areNotificationsOn = false;
        }
    }

    showNotifications() {
        this.notificationsComponent.toggleNotifications();
        this.areNotificationsOn = this.notificationsComponent.isOpen;
    }
}
