import { AppComponentBase } from '@shared/app-component-base';
import { Injector, Component, Input, ChangeDetectorRef } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

export interface Notification {
    text: string;
    type: 'points' | 'enrollment';
    data: any;
}

@Component({
    templateUrl: './notifications.component.html',
    selector: 'app-notifications',
    styleUrls: ['./notifications.scss'],
    animations: [
        trigger('notificationsInOut', [
            transition('void => *', [
                style({ transform: 'translateY(-100%)' }),
                animate(100)
            ]),
            transition('* => void', [
                animate(100, style({ transform: 'translateY(-100%)' }))
            ]),
        ]),
        trigger('notificationsEnter', [
            transition('void => *', [
                style({ transform: 'translateX(-100%)' }),
                animate(100)
            ]),
            transition('* => void', [
                animate(100, style({ transform: 'translateX(-100%)' }))
            ])
        ])
    ]
})
export class NotificationsComponent extends AppComponentBase {
    @Input()
    public notifications: Notification[] = [];
    isOpen = false;

    constructor(
        injector: Injector,
        public changeDetector: ChangeDetectorRef
    ) {
        super(injector);
    }

    public toggleNotifications() {
        this.isOpen = !this.isOpen;
    }

    clearNotif(notification: Notification) {
        this.notifications.splice(this.notifications.indexOf(notification), 1);
        this.changeDetector.detectChanges();
    }
}
