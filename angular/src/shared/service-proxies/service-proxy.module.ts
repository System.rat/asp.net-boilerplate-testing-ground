import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';

import * as ApiServiceProxies from './service-proxies';

@NgModule({
    providers: [
        ApiServiceProxies.RoleServiceProxy,
        ApiServiceProxies.SessionServiceProxy,
        ApiServiceProxies.TenantServiceProxy,
        ApiServiceProxies.UserServiceProxy,
        ApiServiceProxies.TokenAuthServiceProxy,
        ApiServiceProxies.AccountServiceProxy,
        ApiServiceProxies.ConfigurationServiceProxy,
        ApiServiceProxies.InstructorServiceProxy,
        ApiServiceProxies.CourseServiceProxy,
        ApiServiceProxies.DepartmentServiceProxy,
        ApiServiceProxies.StudentServiceProxy,
        ApiServiceProxies.DepartmentServiceProxy,
        ApiServiceProxies.StudentAccountServiceProxy,
        ApiServiceProxies.InstructorAccountServiceProxy,
        ApiServiceProxies.FileServiceProxy,
        ApiServiceProxies.PrivateMessageServiceProxy,
        { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true }
    ]
})
export class ServiceProxyModule { }
