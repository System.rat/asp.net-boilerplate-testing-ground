import { OnChanges, Directive, ViewContainerRef, ComponentFactoryResolver, Renderer2, SimpleChanges, Injectable, Input, HostBinding } from '@angular/core';
import { MatProgressBar } from '@angular/material';

@Directive({
    selector: '[loading]'
})
@Injectable()
export class LoadingDirective implements OnChanges {
    @HostBinding('class.hasLoader') _class = true;
    @Input()
    loading: boolean;
    bar: any | undefined;

    constructor(
        public view: ViewContainerRef,
        private _resolver: ComponentFactoryResolver,
        private _renderer: Renderer2
    ) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['loading'].currentValue) {
            const factory = this._resolver.resolveComponentFactory(MatProgressBar);
            const progressBar = this.view.createComponent(factory);
            progressBar.instance.mode = 'indeterminate';
            progressBar.instance.color = 'accent';
            const barElement = progressBar.injector.get(MatProgressBar)._elementRef.nativeElement as HTMLElement;
            const firstElement = (this.view.element.nativeElement as HTMLElement).children[0];
            if (firstElement !== undefined) {
                this._renderer.insertBefore(
                    this.view.element.nativeElement,
                    barElement,
                    firstElement);
            } else {
                this._renderer.appendChild(
                    this.view.element.nativeElement,
                    barElement
                );
            }
            this.bar = barElement;
        } else if (this.bar !== undefined) {
            this._renderer.removeChild(this.view.element.nativeElement, this.bar);
        }
    }
}
