import { Component, OnInit, Input, Output, EventEmitter, ContentChild, AfterViewInit, AfterContentInit } from '@angular/core';
import { RefreshablePanelContentComponent } from './refreshable-panel-content.component';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Subject } from 'rxjs';

export interface DataProvider<TData> {
    needsRefresh?: Subject<void>;
    getData(): Promise<TData>;
    onRefresh(data: TData);
}

@Component({
    selector: 'refreshable-panel',
    templateUrl: './refreshable-panel.component.html',
    animations: [appModuleAnimation()]
})
export class RefreshablePanelComponent<TData> extends AppComponentBase implements AfterContentInit {
    @ContentChild(RefreshablePanelContentComponent, { static: false })
    content: RefreshablePanelContentComponent | null;
    @Input()
    shouldInit = false;
    @Input()
    dataProvider: DataProvider<TData> | null;
    @Output()
    refreshed: EventEmitter<TData> = new EventEmitter();

    async ngAfterContentInit() {
        await this.refresh();
        if (this.dataProvider && this.dataProvider.needsRefresh) {
            this.dataProvider.needsRefresh.subscribe(() => {
                this.refresh();
                console.log('refreshing');
            });
        }
    }

    async refresh() {
        if (this.dataProvider) {
            if (this.content != null) {
                this.content.isLoading = true;
            }
            const data = await this.dataProvider.getData();
            if (this.content != null) {
                this.content.isLoading = false;
            }
            this.dataProvider.onRefresh(data);
            this.refreshed.emit(data);
        } else {
            this.refreshed.emit();
        }
    }
}
