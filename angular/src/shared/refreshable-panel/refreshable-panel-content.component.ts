import { Component } from '@angular/core';

@Component({
    selector: 'refreshable-panel-content',
    templateUrl: './refreshable-panel-content.component.html'
})
export class RefreshablePanelContentComponent {
    isLoading: boolean;
}
