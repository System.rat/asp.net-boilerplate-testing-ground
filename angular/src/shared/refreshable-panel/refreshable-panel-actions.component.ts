import { Component, Input, AfterViewInit } from '@angular/core';
import { Subject } from 'rxjs';

export interface ActionDataProvider<TData> {
    needsActionDataRefresh?: Subject<void>;
    getActionData(): Promise<TData>;
    onActionRefresh(data: TData);
}

@Component({
    templateUrl: './refreshable-panel-actions.component.html',
    selector: 'refreshable-panel-actions'
})
export class RefreshablePanelActionsComponent<TData> implements AfterViewInit {
    @Input()
    dataProvider: ActionDataProvider<TData> | null;

    ngAfterViewInit() {
        this.refresh();
        if (this.dataProvider && this.dataProvider.needsActionDataRefresh) {
            this.dataProvider.needsActionDataRefresh.subscribe(() => {
                this.refresh();
            });
        }
    }

    async refresh() {
        if (this.dataProvider) {
            const data = await this.dataProvider.getActionData();
            this.dataProvider.onActionRefresh(data);
        }
    }
}
