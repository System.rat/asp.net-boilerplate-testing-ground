import { Component, Host, Injector, Input } from '@angular/core';
import { RefreshablePanelComponent } from './refreshable-panel.component';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
    selector: 'refreshable-panel-header',
    templateUrl: './refreshable-panel-header.component.html'
})
export class RefreshablePanelHeaderComponent<TData> extends AppComponentBase {
    @Input()
    title: string;

    @Input()
    routerLink: string | null;

    constructor(
        @Host()
        public panel: RefreshablePanelComponent<TData>,
        injector: Injector
    ) {
        super(injector);
    }

    reload() {
        this.panel.refresh();
    }
}
