import { AppComponentBase } from '@shared/app-component-base';
import { Component, Injector, OnInit, Renderer2, HostListener, ViewChild } from '@angular/core';
import { ConfigurationServiceProxy, ChangeUiThemeInput } from '@shared/service-proxies/service-proxies';
import { MatExpansionPanel } from '@angular/material';

class UiThemeInfo {
    constructor(
        public name: string,
        public cssClass: string
    ) { }
}

@Component({
    templateUrl: './theme-switcher.component.html',
    selector: 'theme-switcher',
    styleUrls: ['./theme-switcher.scss']
})
export class ThemeSwitcherComponent extends AppComponentBase implements OnInit {
    themes: UiThemeInfo[] = [
        new UiThemeInfo('Red', 'red'),
        new UiThemeInfo('Pink', 'pink'),
        new UiThemeInfo('Purple', 'purple'),
        new UiThemeInfo('Deep Purple', 'deep-purple'),
        new UiThemeInfo('Indigo', 'indigo'),
        new UiThemeInfo('Blue', 'blue'),
        new UiThemeInfo('Light Blue', 'light-blue'),
        new UiThemeInfo('Cyan', 'cyan'),
        new UiThemeInfo('Teal', 'teal'),
        new UiThemeInfo('Green', 'green'),
        new UiThemeInfo('Light Green', 'light-green'),
        new UiThemeInfo('Lime', 'lime'),
        new UiThemeInfo('Yellow', 'yellow'),
        new UiThemeInfo('Amber', 'amber'),
        new UiThemeInfo('Orange', 'orange'),
        new UiThemeInfo('Deep Orange', 'deep-orange'),
        new UiThemeInfo('Brown', 'brown'),
        new UiThemeInfo('Grey', 'grey'),
        new UiThemeInfo('Blue Grey', 'blue-grey'),
        new UiThemeInfo('Black', 'black')
    ];
    selectedCssClass = 'red';
    selectedTheme: UiThemeInfo = this.themes[0];
    @ViewChild(MatExpansionPanel, { static: false })
    panel: MatExpansionPanel;

    constructor(
        injector: Injector,
        private _configurationService: ConfigurationServiceProxy,
        private _renderer: Renderer2
    ) {
        super(injector);
    }

    ngOnInit() {
        this._renderer.removeClass(document.body, 'theme-' + this.selectedCssClass);
        this.selectedCssClass = this.setting.get('App.UiTheme');
        this.selectedTheme = this.themes.find(t => t.cssClass === this.selectedCssClass);
        this._renderer.addClass(document.body, 'theme-' + this.selectedCssClass);
    }

    async setTheme(theme: UiThemeInfo) {
        const changeThemeInput = new ChangeUiThemeInput();
        changeThemeInput.theme = theme.cssClass;
        await this._configurationService.changeUiTheme(changeThemeInput).toPromise();
        this._renderer.removeClass(document.body, 'theme-' + this.selectedCssClass);
        this._renderer.addClass(document.body, 'theme-' + theme.cssClass);
        this.selectedCssClass = theme.cssClass;
        this.selectedTheme = theme;
    }

    @HostListener('focusout', ['$event'])
    focusout($event) {
        console.log('Unfocused');
        this.panel.close();
    }
}
