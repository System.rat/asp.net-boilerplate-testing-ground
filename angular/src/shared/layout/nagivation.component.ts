import { AppComponentBase } from '@shared/app-component-base';
import { Injector, Component, Output, EventEmitter, Inject } from '@angular/core';
import { MenuItem } from '@shared/layout/menu-item';
import { EntityService, ENTITY_SERVICE_TOKEN } from '@shared/private-messages/entity-provider';

@Component({
    templateUrl: './navigation.component.html',
    selector: 'account-nav'
})
export class NavigationComponent extends AppComponentBase {
    menuItems: MenuItem[] = [];
    entityType: string;
    routerLink: string;
    @Output()
    notificationClick: EventEmitter<null> = new EventEmitter();

    constructor(
        injector: Injector,
        @Inject(ENTITY_SERVICE_TOKEN)
        entityService: EntityService
    ) {
        super(injector);
        this.menuItems = entityService.routes;
        entityService.getInfoFromUserId(this.appSession.userId).then((entity) => {
            this.entityType = entity.type;
            this.routerLink = entity.type === 'Instructor' ? '/instructor' : '/student';
        });
    }

    nClicked() {
        this.notificationClick.emit();
    }
}
