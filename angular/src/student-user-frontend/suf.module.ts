import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { AbpModule } from 'abp-ng2-module/dist/src/abp.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { SufRoutingModule } from './suf.routing.module';
import { PointsComponent } from './points/points.component';
import { InstructorsComponent } from './instructors/instructors.component';
import { CoursesComponent } from './courses/courses.component';
import { BoardsComponent } from './boards/boards.component';
import { ENTITY_SERVICE_TOKEN } from '@shared/private-messages/entity-provider';
import { StudentEntityProviderService } from './student-entity-provider.service';
import { StudentMessageResolverService } from './student-messages-resolver.service';
import { UserEntityGuard } from '@shared/auth/user-entity-account-guard';

@NgModule({
    declarations: [
        PointsComponent,
        InstructorsComponent,
        CoursesComponent,
        BoardsComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpClientJsonpModule,
        ModalModule.forRoot(),
        AbpModule,
        SufRoutingModule,
        ServiceProxyModule,
        SharedModule,
        NgxPaginationModule,
    ],
    providers: [
        UserEntityGuard,
        { provide: ENTITY_SERVICE_TOKEN, useClass: StudentEntityProviderService },
        StudentMessageResolverService
    ],
})
export class SufModule { }
