import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';

export class StudentMessageResolverService implements Resolve<string> {
    resolve(): Observable<string> {
        return of('/student/messages');
    }
}
