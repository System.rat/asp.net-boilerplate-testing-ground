import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PointsComponent } from './points/points.component';
import { InstructorsComponent } from './instructors/instructors.component';
import { CoursesComponent } from './courses/courses.component';
import { BoardsComponent } from './boards/boards.component';
import { PrivateMessagesComponent } from '@shared/private-messages/private-messages.component';
import { RepliesComponent } from '@shared/private-messages/replies/replies.component';
import { StudentMessageResolverService } from './student-messages-resolver.service';
import { UserEntityGuard } from '@shared/auth/user-entity-account-guard';
import { EntityAccountComponent } from '@shared/account/entity-account.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: EntityAccountComponent,
                canActivate: [UserEntityGuard],
                children: [
                    { path: 'points', component: PointsComponent },
                    { path: 'courses', component: CoursesComponent },
                    { path: 'instructors', component: InstructorsComponent },
                    { path: 'boards', component: BoardsComponent },
                    {
                        path: 'messages', component: PrivateMessagesComponent, resolve: {
                            routerLink: StudentMessageResolverService
                        }
                    },
                    {
                        path: 'messages/:id', component: RepliesComponent, resolve: {
                            routerLink: StudentMessageResolverService
                        }
                    }
                ]
            }
        ]),
    ],
    exports: [RouterModule]
})
export class SufRoutingModule { }
