import { AppComponentBase } from '@shared/app-component-base';
import { Injector, Component, OnInit } from '@angular/core';
import { StudentAccountServiceProxy, StudentEnrollmentInfoDto, ListResultDtoOfStudentEnrollmentInfoDto } from '@shared/service-proxies/service-proxies';
import { Grade } from '@app/course/list-enrollments/list-enrollments-dialog.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';

@Component({
    templateUrl: './courses.component.html',
    animations: [appModuleAnimation()]
})
export class CoursesComponent extends AppComponentBase implements DataProvider<ListResultDtoOfStudentEnrollmentInfoDto> {
    shouldInit = true;
    courses: StudentEnrollmentInfoDto[] = [];
    isLoading = true;

    constructor(
        injector: Injector,
        private _studentService: StudentAccountServiceProxy
    ) {
        super(injector);
    }

    getData = () => this._studentService.getEnrollments().toPromise();

    onRefresh = (data: ListResultDtoOfStudentEnrollmentInfoDto) => this.courses = data.items;

    toGrade = (num: number) => Grade[num];
}
