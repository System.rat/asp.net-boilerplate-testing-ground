import { AppComponentBase } from '@shared/app-component-base';
import { Component, Injector, OnInit } from '@angular/core';
import {
    PointsListDto,
    StudentAccountServiceProxy,
    StudentEnrollmentInfoDto,
    ListResultDtoOfStudentEnrollmentInfoDto,
    ListResultDtoOfStudentPointsListDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';
import { Subject } from 'rxjs';
import { ActionDataProvider } from '@shared/refreshable-panel/refreshable-panel-actions.component';

@Component({
    templateUrl: 'points.component.html',
    animations: [appModuleAnimation()]
})
export class PointsComponent extends AppComponentBase implements
    OnInit,
    DataProvider<ListResultDtoOfStudentPointsListDto>,
    ActionDataProvider<ListResultDtoOfStudentEnrollmentInfoDto> {
    shouldInit = true;
    points: PointsListDto[] = [];
    courses: StudentEnrollmentInfoDto[] = [];
    selectedCourse: number;
    needsRefresh = new Subject<void>();

    constructor(
        injector: Injector,
        private _studentService: StudentAccountServiceProxy
    ) {
        super(injector);
    }

    getData(): Promise<ListResultDtoOfStudentPointsListDto> {
        return this._studentService.getPoints(this.selectedCourse).toPromise();
    }

    onRefresh(p: ListResultDtoOfStudentPointsListDto) {
        this.points = p.items;
    }

    getActionData(): Promise<ListResultDtoOfStudentEnrollmentInfoDto> {
        return this._studentService.getEnrollments().toPromise();
    }

    onActionRefresh(data: ListResultDtoOfStudentEnrollmentInfoDto) {
        this.courses = data.items;
    }

    async ngOnInit() {
        abp.event.on('abp.notifications.received', (notification) => {
            this.needsRefresh.next();
        });
    }
}
