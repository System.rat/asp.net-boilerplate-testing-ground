import { EntityService, Entity, EntityType } from '@shared/private-messages/entity-provider';
import { StudentAccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppSessionService } from '@shared/session/app-session.service';
import { MenuItem } from '@shared/layout/menu-item';

class StudentMenuItem extends MenuItem {
    constructor(name: string, icon: string, route: string, childItems?: MenuItem[]) {
        let inter = `/student/${route}`;
        if (route === '') { inter = ''; }
        super(abp.localization.localize(name, 'Test'), 'Entity.Student', icon, inter, childItems);
    }
}

export class StudentEntityProviderService implements EntityService {
    routes: MenuItem[] = [
        new StudentMenuItem('Points', 'people', 'points'),
        new StudentMenuItem('Courses', 'people', 'courses'),
        new StudentMenuItem('Instructors', 'people', 'instructors'),
        new StudentMenuItem('Boards', 'people', 'boards'),
        new StudentMenuItem('Messages', 'people', 'messages'),
    ];

    constructor(
        private _studentService: StudentAccountServiceProxy,
        private _session: AppSessionService
    ) { }

    async getEntities(): Promise<Entity[]> {
        return (await this._studentService.getInstructors().toPromise())
            .items.map(student => <Entity>{
                id: student.id,
                type: EntityType.Student,
                name: student.firstName + ' ' + student.lastName
            });
    }

    async getInfoFromUserId() {
        const student = await this._studentService.getFromUserId(this._session.userId).toPromise();
        return {
            type: 'Student',
            data: new Map([
                ['FirstName', student.firstName],
                ['LastName', student.lastName],
                ['EnrollmentDate', student.enrollmentDate.format('DD MMM, YYYY')]
            ])
        };
    }
}
