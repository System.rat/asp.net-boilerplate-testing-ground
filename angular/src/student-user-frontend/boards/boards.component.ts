import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Injector, Component } from '@angular/core';
import {
    BoardMessageDto,
    StudentAccountServiceProxy,
    BoardUploadedFileDto,
    FileServiceProxy,
    ListResultDtoOfBoardMessageDto} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { saveAs } from 'file-saver';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';

@Component({
    templateUrl: './boards.component.html',
    animations: [appModuleAnimation()]
})
export class BoardsComponent extends AppComponentBase implements DataProvider<ListResultDtoOfBoardMessageDto> {
    messages: BoardMessageDto[] = [];
    shouldInit = true;

    constructor(
        injector: Injector,
        private _studentService: StudentAccountServiceProxy,
        private _fileService: FileServiceProxy
    ) {
        super(injector);
    }

    getData = () => this._studentService.getAllBoardMessages(undefined, undefined).toPromise();

    onRefresh = (data: ListResultDtoOfBoardMessageDto) => this.messages = data.items;

    async downloadFile(file: BoardUploadedFileDto) {
        const res = await this._fileService.downloadAttachment(file.attachmentId).toPromise();
        saveAs(res.data, file.fileName);
    }
}
