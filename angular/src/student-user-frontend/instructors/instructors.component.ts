import { Component, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { StudentAccountServiceProxy, StudentInstructorInfoDto, ListResultDtoOfStudentInstructorInfoDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';

@Component({
    templateUrl: './instructors.component.html',
    animations: [appModuleAnimation()]
})
export class InstructorsComponent extends AppComponentBase implements DataProvider<ListResultDtoOfStudentInstructorInfoDto> {
    instructors: StudentInstructorInfoDto[] = [];
    shouldInit = true;

    constructor(
        injector: Injector,
        private _studentService: StudentAccountServiceProxy
    ) {
        super(injector);
    }

    onRefresh(data: ListResultDtoOfStudentInstructorInfoDto) {
        this.instructors = data.items;
    }

    getData() {
        return this._studentService.getInstructors().toPromise();
    }
}
