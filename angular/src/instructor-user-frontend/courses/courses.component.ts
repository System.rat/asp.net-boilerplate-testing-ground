import { Component, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { InstructorInfoDto, InstructorAccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';


@Component({
    templateUrl: './courses.component.html',
    animations: [appModuleAnimation()]
})
export class CoursesComponent extends AppComponentBase implements DataProvider<InstructorInfoDto> {
    shouldInit = true;
    instructor: InstructorInfoDto = new InstructorInfoDto();

    constructor(
        injector: Injector,
        private _instructorService: InstructorAccountServiceProxy
    ) {
        super(injector);
    }

    getData = (): Promise<InstructorInfoDto> => this._instructorService.getWithCourseAssignments().toPromise();

    onRefresh = (data: InstructorInfoDto) => this.instructor = data;
}
