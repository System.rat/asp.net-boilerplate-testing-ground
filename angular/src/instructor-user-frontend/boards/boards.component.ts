import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Injector, Component } from '@angular/core';
import {
    BoardUploadedFileDto,
    FileServiceProxy,
    InstructorAccountServiceProxy,
    InstructorBoardMessageDto,
    InstructorPostBoardMessageDto,
    InstructorUploadedFileDto,
    SharedAttachFileDtoMessageType,
    SharedAttachFileDto,
    CourseAssignmentDto,
    PrivateMessageServiceProxy,
    ListResultDtoOfInstructorBoardMessageDto
} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { saveAs } from 'file-saver';
import { Grade } from '@app/course/list-enrollments/list-enrollments-dialog.component';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { FormControl } from '@angular/forms';
import * as m from 'moment';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';
import { ActionDataProvider } from '@shared/refreshable-panel/refreshable-panel-actions.component';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './boards.component.html',
    animations: [appModuleAnimation()],
    providers: [
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
    ]
})
export class InstructorBoardsComponent extends AppComponentBase implements
    DataProvider<[ListResultDtoOfInstructorBoardMessageDto, number]>,
    ActionDataProvider<CourseAssignmentDto[]> {
    messages: InstructorBoardMessageDto[] = [];
    dueDate: FormControl = new FormControl(m(), { updateOn: 'blur' });
    hasDueDate = false;
    courses: CourseAssignmentDto[] = [];
    grades = [];
    newBoardMessage: InstructorPostBoardMessageDto = new InstructorPostBoardMessageDto();
    selectedFiles: FileList | undefined;
    sending = false;
    instructorId: number;
    needsRefresh = new Subject<void>();
    shouldInit = true;

    constructor(
        injector: Injector,
        private _instructorService: InstructorAccountServiceProxy,
        private _messageService: PrivateMessageServiceProxy,
        private _fileService: FileServiceProxy
    ) {
        super(injector);
        this.grades = this.getGradeStrings();
    }

    getActionData = async () => (await this._instructorService.getWithCourseAssignments().toPromise()).courseAssignments;

    onActionRefresh = (data) => this.courses = data;

    getData = () => {
        const messages = this._instructorService.getBoardMessages(undefined, undefined, undefined, undefined, undefined).toPromise();
        const id = this._instructorService.getIdFromUser().toPromise();
        return Promise.all([messages, id]);
    }

    onRefresh = ([data, id]: [ListResultDtoOfInstructorBoardMessageDto, number]) =>  {
        this.messages = data.items;
        this.instructorId = id;
    }

    getGradeStrings() {
        const keys: { s: string, v: string }[] = [];
        for (const k in Grade) {
            if ('number' === typeof(Grade[k])) {
                keys.push({ s: k.toString(), v: Grade[k] });
            }
        }
        return keys;
    }

    async create() {
        this.sending = true;
        const gd = this.dueDate.value as m.Moment;
        this.newBoardMessage.dueDate = this.hasDueDate ? gd.add(gd.utcOffset(), 'minutes') : undefined;
        const res = await this._instructorService.postBoardMessage(this.newBoardMessage).toPromise();
        if (this.selectedFiles !== undefined) {
            const promises: Promise<InstructorUploadedFileDto>[] = [];
            for (let i = 0; i < this.selectedFiles.length; i++) {
                promises.push(this._messageService.uploadFile({
                    data: this.selectedFiles[i],
                    fileName: this.selectedFiles[i].name
                }).toPromise());
            }
            const fresArr = await Promise.all(promises);
            fresArr.map((fres) => {
                console.log(fres);
                return this._messageService.attachFile({
                    fileId: fres.id,
                    messageId: res.id,
                    messageType: SharedAttachFileDtoMessageType._0
                } as SharedAttachFileDto).toPromise();
            });
            await Promise.all(fresArr);
        }
        abp.notify.success(this.l('BoardMessagePosted'));
        this.newBoardMessage = new InstructorPostBoardMessageDto();
        this.sending = false;
        this.needsRefresh.next();
    }

    async deleteMessage(id: number) {
        await this._instructorService.deleteBoardmessage(id).toPromise();
        this.needsRefresh.next();
    }

    onFileChange(event) {
        this.selectedFiles = event.srcElement.files;
    }

    async downloadFile(file: BoardUploadedFileDto) {
        const res = await this._fileService.downloadAttachment(file.attachmentId).toPromise();
        saveAs(res.data, file.fileName);
    }
}
