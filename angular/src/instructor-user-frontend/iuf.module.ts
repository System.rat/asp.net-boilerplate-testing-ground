import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { AbpModule } from 'abp-ng2-module/dist/src/abp.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { IufRoutingModule } from './iuf.routing.module';
import { CoursesComponent } from './courses/courses.component';
import { StudentComponent } from './students/student.component';
import { GivePointsDialogComponent } from './students/give-points-dialog.component';
import { ChangePointsDialogComponent } from './students/change-points-dialog.component';
import { InstructorBoardsComponent } from './boards/boards.component';
import { FilesComponent } from './files/files.component';
import { ENTITY_SERVICE_TOKEN } from '@shared/private-messages/entity-provider';
import { InstructorEntityProviderService } from './instructor-entity-provider.service';
import { InstructorMessageResolverService } from './instructor-messages-resolver.service';
import { UserEntityGuard } from '@shared/auth/user-entity-account-guard';

@NgModule({
    declarations: [
        CoursesComponent,
        StudentComponent,
        GivePointsDialogComponent,
        ChangePointsDialogComponent,
        InstructorBoardsComponent,
        FilesComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpClientJsonpModule,
        ModalModule.forRoot(),
        AbpModule,
        IufRoutingModule,
        ServiceProxyModule,
        SharedModule,
        NgxPaginationModule,
    ],
    providers: [
        UserEntityGuard,
        { provide: ENTITY_SERVICE_TOKEN, useClass: InstructorEntityProviderService },
        InstructorMessageResolverService
    ],
    entryComponents: [
        GivePointsDialogComponent,
        ChangePointsDialogComponent
    ]
})
export class IufModule { }
