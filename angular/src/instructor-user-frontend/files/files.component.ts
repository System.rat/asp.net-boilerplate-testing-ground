import { AppComponentBase } from '@shared/app-component-base';
import { Injector, Component } from '@angular/core';
import {
    InstructorUploadedFileDto,
    InstructorAccountServiceProxy,
    FileServiceProxy,
    PrivateMessageServiceProxy,
    ListResultDtoOfInstructorUploadedFileDto} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { saveAs } from 'file-saver';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './files.component.html',
    animations: [appModuleAnimation()],
    styles: [
        `
        input[type="file"] {
            display: inline-block;
        }

        .container-fluid {
            margin-top: 20px;
        }

        .file-item {
            margin-top: 10px;
            padding-top: 1px;
            box-shadow: 0px 0px 3px 1px lightgrey;
            padding-left: 10px;
            padding-bottom: 10px;
            padding-right: 5px;
        }
        `
    ]
})
export class FilesComponent extends AppComponentBase implements DataProvider<ListResultDtoOfInstructorUploadedFileDto> {
    shouldInit = true;
    uploadedFiles: InstructorUploadedFileDto[] = [];
    files: FileList;
    needsRefresh = new Subject<void>();

    constructor(
        injector: Injector,
        private _instructorService: InstructorAccountServiceProxy,
        private _messageService: PrivateMessageServiceProxy,
        private _fileService: FileServiceProxy
    ) {
        super(injector);
    }

    getData = () => this._instructorService.getUploadedFiles().toPromise();

    onRefresh = (data: ListResultDtoOfInstructorUploadedFileDto) => this.uploadedFiles = data.items;

    async uploadFiles() {
        if (this.files === undefined) { return; }
        const uploadPromises = [];
        for (let i = 0; i < this.files.length; i++) {
            uploadPromises.push(this._messageService.uploadFile({
                data: this.files[i],
                fileName: this.files[i].name
            }).toPromise());
        }
        await Promise.all(uploadPromises);
        this.files = undefined;
        abp.notify.success(this.l('FilesUploaded'));
        this.needsRefresh.next();
    }

    async downloadFile(file: InstructorUploadedFileDto) {
        const data = await this._fileService.downloadFile(file.id).toPromise();
        saveAs(data.data, file.fileName);
    }

    deleteFile(file: InstructorUploadedFileDto) {
        abp.message.confirm(this.l('DeleteFile', file.fileName), async (res) => {
            if (res) {
                await this._instructorService.deleteFIle(file.id).toPromise();
                this.needsRefresh.next();
            }
        });
    }
}
