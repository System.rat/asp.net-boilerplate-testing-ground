import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoursesComponent } from './courses/courses.component';
import { StudentComponent } from './students/student.component';
import { InstructorBoardsComponent } from './boards/boards.component';
import { FilesComponent } from './files/files.component';
import { PrivateMessagesComponent } from '@shared/private-messages/private-messages.component';
import { RepliesComponent } from '@shared/private-messages/replies/replies.component';
import { InstructorMessageResolverService } from './instructor-messages-resolver.service';
import { EntityAccountComponent } from '@shared/account/entity-account.component';
import { UserEntityGuard } from '@shared/auth/user-entity-account-guard';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: EntityAccountComponent,
                canActivate: [UserEntityGuard],
                children: [
                    {
                        path: 'courses', component: CoursesComponent
                    },
                    {
                        path: 'students', component: StudentComponent
                    },
                    {
                        path: 'messages', component: PrivateMessagesComponent, resolve: {
                            routerLink: InstructorMessageResolverService
                        }
                    },
                    {
                        path: 'messages/:id', component: RepliesComponent, resolve: {
                            routerLink: InstructorMessageResolverService
                        }
                    },
                    {
                        path: 'boards', component: InstructorBoardsComponent
                    },
                    {
                        path: 'files', component: FilesComponent
                    }
                ]
            }
        ]),
    ],
    exports: [RouterModule]
})
export class IufRoutingModule { }
