import { AppComponentBase } from '@shared/app-component-base';
import { Inject, Injector, Component } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InstructorAccountServiceProxy, InstructorChangeGradeDto } from '@shared/service-proxies/service-proxies';
import { Points } from './student.component';

@Component({
    templateUrl: './change-points-dialog.component.html'
})
export class ChangePointsDialogComponent extends AppComponentBase {
    changePointsDto: Points;
    saving = false;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<ChangePointsDialogComponent>,
        @Inject(MAT_DIALOG_DATA)
        _point: Points,
        private _instructorService: InstructorAccountServiceProxy
    ) {
        super(injector);
        this.changePointsDto = _point;
    }

    async save() {
        this.saving = true;
        try {
            await this._instructorService.updatePoints(<InstructorChangeGradeDto><unknown>this.changePointsDto).toPromise();
            this.saving = false;
            abp.notify.success(this.l('PointsUpdated'));
            this.close(true);
        } catch {
            abp.notify.error(this.l('StudentNotGivenPoints'));
            this.close(false);
        }
    }

    close(result: boolean) {
        this._dialogRef.close(result);
    }
}
