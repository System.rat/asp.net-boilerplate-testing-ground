import { AppComponentBase } from '@shared/app-component-base';
import { OnInit, Inject, Injector, Component } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { InstructorAccountServiceProxy, InstructorGradeStudentDto, CourseAssignmentDto } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './give-points-dialog.component.html'
})
export class GivePointsDialogComponent extends AppComponentBase implements OnInit {
    pointsToGive: InstructorGradeStudentDto = new InstructorGradeStudentDto();
    courses: CourseAssignmentDto[] = [];
    isLoading = false;
    saving = false;

    constructor(
        injector: Injector,
        private _dialogRef: MatDialogRef<GivePointsDialogComponent>,
        @Inject(MAT_DIALOG_DATA)
        private _studentId: number,
        private _instructorService: InstructorAccountServiceProxy
    ) {
        super(injector);
    }

    async save() {
        try {
            this.saving = true;
            this.pointsToGive.studentId = this._studentId;
            await this._instructorService.givePoints(this.pointsToGive).toPromise();
            abp.notify.success(this.l('StudentGivenPoints'));
            this.close(true);
        } catch {
            abp.notify.error(this.l('StudentNotGivenPoints'));
            this.close(false);
        }
    }

    close(result: boolean) {
        this._dialogRef.close(result);
    }

    async ngOnInit() {
        this.isLoading = true;
        const res = await this._instructorService.getWithCourseAssignments().toPromise();
        this.isLoading = false;
        this.courses = res.courseAssignments;
    }
}
