import { AppComponentBase } from '@shared/app-component-base';
import { Injector, Component } from '@angular/core';
import {
    InstructorAccountServiceProxy,
    StudentShortInfoDto,
    InstructorPointsListDto,
    ListResultDtoOfStudentShortInfoDto,
    ListResultDtoOfInstructorPointsListDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { MatDialog } from '@angular/material';
import { GivePointsDialogComponent } from './give-points-dialog.component';
import { ChangePointsDialogComponent } from './change-points-dialog.component';
import { DataProvider } from '@shared/refreshable-panel/refreshable-panel.component';
import { Subject } from 'rxjs';

export class StudentWithPoints extends StudentShortInfoDto {
    points: Points[] | undefined;
}

export type Points = Pick<InstructorPointsListDto, 'id' | 'pointCount' | 'course' | 'dateGraded'>;

@Component({
    templateUrl: './student.component.html',
    animations: [appModuleAnimation()]
})
export class StudentComponent extends AppComponentBase implements
    DataProvider<[
        ListResultDtoOfStudentShortInfoDto,
        ListResultDtoOfInstructorPointsListDto
    ]> {
    students: StudentWithPoints[] = [];
    shouldInit = true;
    needsRefresh = new Subject<void>();


    constructor(
        injector: Injector,
        private _instructorService: InstructorAccountServiceProxy,
        private _dialog: MatDialog
    ) {
        super(injector);
    }

    getData = () => Promise.all([
        this._instructorService.getStudents().toPromise(),
        this._instructorService.getPoints(undefined).toPromise()
    ])

    onRefresh([s, p]: [ListResultDtoOfStudentShortInfoDto, ListResultDtoOfInstructorPointsListDto]) {
        this.students = <StudentWithPoints[]>s.items;
        this.students.forEach(student => {
            student.points = <Points[]>p.items.filter(point => point.studentId === student.id);
        });
    }

    async openPointsDialog(studentId: number) {
        const res = await this._dialog.open(GivePointsDialogComponent, { data: studentId }).afterClosed().toPromise();
        if (res) {
            this.needsRefresh.next();
        }
    }

    async openUpdatePointsDialog(point: Points) {
        const res = await this._dialog.open(ChangePointsDialogComponent, { data: point }).afterClosed().toPromise();
        if (res) {
            this.needsRefresh.next();
        }
    }

    async deletePoints(id: number) {
        abp.message.confirm(this.l('DeletePointsMessage'), async (res: boolean) => {
            if (res) {
                await this._instructorService.removePoints(id).toPromise();
                abp.notify.success(this.l('PointsRemoved'));
                this.needsRefresh.next();
            }
        });
    }
}
