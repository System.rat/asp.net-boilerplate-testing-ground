import { EntityService, Entity, EntityType } from '@shared/private-messages/entity-provider';
import { InstructorAccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppSessionService } from '@shared/session/app-session.service';
import { MenuItem } from '@shared/layout/menu-item';

class InstructorMenuItem extends MenuItem {
    constructor(name: string, icon: string, route: string, childItems?: MenuItem[]) {
        let inter = `/instructor/${route}`;
        if (route === '') { inter = ''; }
        super(abp.localization.localize(name, 'Test'), 'Entity.Instructor', icon, inter, childItems);
    }
}

export class InstructorEntityProviderService implements EntityService {
    routes: MenuItem[] = [
        new InstructorMenuItem('Courses', 'people', 'courses'),
        new InstructorMenuItem('Students', 'people', 'students'),
        new InstructorMenuItem('Boards', 'people', 'boards'),
        new InstructorMenuItem('Messages', 'people', 'messages'),
        new InstructorMenuItem('Files', 'people', 'files')
    ];

    constructor(
        private _instructorService: InstructorAccountServiceProxy,
        private _session: AppSessionService
    ) { }

    async getEntities(): Promise<Entity[]> {
        const students = this._instructorService.getStudents().toPromise();
        const instructors = this._instructorService.getInstructors().toPromise();

        const [studentsResult, instructorsResult] = await Promise.all([students, instructors]);
        return studentsResult.items.map(student => <Entity>{
            id: student.id,
            type: EntityType.Student,
            name: student.firstName + ' ' + student.lastName
        }).concat(instructorsResult.items.map(instructor => <Entity>{
            id: instructor.id,
            type: EntityType.Instructor,
            name: instructor.firstName + ' ' + instructor.lastName
        }));
    }

    async getInfoFromUserId() {
        const instructor = await this._instructorService.getFromUserId(this._session.userId).toPromise();
        return {
            type: 'Instructor',
            data: new Map([
                ['FirstName', instructor.firstName],
                ['LastName', instructor.lastName],
                ['HireDate', instructor.hireDate.format('DD MMM, YYYY')]
            ])
        };
    }
}
