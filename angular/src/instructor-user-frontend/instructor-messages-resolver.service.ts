import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';

export class InstructorMessageResolverService implements Resolve<string> {
    resolve(): Observable<string> {
        return of('/instructor/messages');
    }
}
