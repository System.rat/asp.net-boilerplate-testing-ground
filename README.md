# About

A simple University managment test project for getting used to the ASP.NET Boilerplate Entreprise
framework along with the Angular framework.

## Features

- Administrative managment for instructors, students, courses, departments, etc.
- Student and Instructor accounts for viewing points, courses, etc.
- Private messaging between users and file sending
- Realtime notifications
- Single-page application frontend
- Simple, clear and responsive frontend design

## License

[MIT](LICENSE).
