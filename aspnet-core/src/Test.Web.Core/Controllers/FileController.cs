﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Test.Authorization;
using Test.Authorization.Users;
using Test.Net.MimeTypes;
using Test.University;

namespace Test.Controllers
{
    /// <summary>
    /// File downloading
    /// </summary>
    [Route("api/[controller]/[action]")]
    public class FileController : TestControllerBase
    {
        readonly IRepository<UploadedFile> _uploadedFileRepository;
        readonly IRepository<FileAttachment> _fileAttachmentRepository;
        readonly IRepository<PrivateBoardMessage> _privateBoardMessageRepository;
        readonly IRepository<PrivateBoardMessageReply> _privateReplyRepository;
        readonly IRepository<Student> _studentRepository;
        readonly IRepository<Instructor> _intructroRepository;
        readonly ITestSession _testSession;

        public FileController(
            IRepository<UploadedFile> uploadedFileRepository,
            IRepository<FileAttachment> fileAttachmentRepository,
            IRepository<PrivateBoardMessage> privateBoardMessageRepository,
            IRepository<PrivateBoardMessageReply> privateReplyRepository,
            ITestSession testSession,
            IRepository<Student> studentRepository,
            IRepository<Instructor> intructroRepository)
        {
            _uploadedFileRepository = uploadedFileRepository;
            _fileAttachmentRepository = fileAttachmentRepository;
            _privateBoardMessageRepository = privateBoardMessageRepository;
            _privateReplyRepository = privateReplyRepository;
            _testSession = testSession;
            _studentRepository = studentRepository;
            _intructroRepository = intructroRepository;
        }

        /// <summary>
        /// Downloads a file if the user owns it
        /// </summary>
        /// <param name="fileId"> ID of the file to download </param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(FileStreamResult), 200)]
        public FileStreamResult DownloadFile(int fileId)
        {
            UploadedFile uploadedFile = _uploadedFileRepository.GetOrThrow(fileId, L("FileDoesNotExist"));
            if (uploadedFile.UserId != AbpSession.UserId.Value) throw new UserFriendlyException(L("FileNotOwned"));
            return File(new FileInfo(uploadedFile.FilePath).OpenRead(), MimeTypeNames.ApplicationOctetStream, uploadedFile.FileName);
        }

        /// <summary>
        /// Downloads a file from a message attachment if the user is permited to do so
        /// </summary>
        /// <param name="attachmentId">The attachment ID of the file to download </param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(FileStreamResult), 200)]
        public FileStreamResult DownloadAttachment(int attachmentId)
        {
            FileAttachment fileAttachment = _fileAttachmentRepository.GetAllIncluding(f => f.File).GetOrThrow(a => a.Id == attachmentId, L("AttachmentNotFound"));
            if (!_testSession.UserEntityType.HasValue) throw new UserFriendlyException(L("UnauthorizedAccessToAttachment"));
            UserEntityType userEntityType = _testSession.UserEntityType.Value;
            int entityId;
            if (userEntityType == UserEntityType.Student)
            {
                entityId = _studentRepository.GetAll().GetOrThrow(s => s.UserId.HasValue && s.UserId.Value == _testSession.TUserId.Value, L("StudentDoesNotExist")).Id;
            }
            else
            {
                entityId = _intructroRepository.GetAll().GetOrThrow(i => i.UserId.HasValue && i.UserId.Value == _testSession.TUserId.Value, L("InstructorDoesNotExist")).Id;
            }
            switch (fileAttachment.MessageType)
            {
                case MessageType.Board:
                    break;
                case MessageType.Private:
                    {
                        PrivateBoardMessage privateBoardMessage = _privateBoardMessageRepository.GetOrThrow(fileAttachment.MessageId, L("PrivateMessageDoesNotExist"));
                        if (
                            privateBoardMessage.SenderType == userEntityType &&
                            privateBoardMessage.SenderId != entityId &&
                            privateBoardMessage.TargetType == userEntityType &&
                            privateBoardMessage.TargetId != entityId)
                        {
                            throw new UserFriendlyException(L("Can'tAccesMessage"));
                        }
                        break;
                    }
                case MessageType.Reply:
                    {
                        PrivateBoardMessageReply privateBoardMessageReply = _privateReplyRepository.GetOrThrow(fileAttachment.MessageId, L("PrivateMessageDoesNotExist"));
                        PrivateBoardMessage privateBoardMessage = _privateBoardMessageRepository.GetOrThrow(privateBoardMessageReply.PrivateBoardMessageId, L("PrivateMessageDoesNotExist"));
                        if (
                            privateBoardMessage.SenderType == userEntityType &&
                            privateBoardMessage.SenderId != entityId &&
                            privateBoardMessage.TargetType == userEntityType &&
                            privateBoardMessage.TargetId != entityId)
                        {
                            throw new UserFriendlyException(L("Can'tAccesMessage"));
                        }
                        break;
                    }
            }
            return File(new FileInfo(fileAttachment.File.FilePath).OpenRead(), MimeTypeNames.ApplicationOctetStream, fileAttachment.File.FileName);
        }
    }
}
