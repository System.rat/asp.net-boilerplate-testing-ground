﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class AddedBoundUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "AppStudents",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId1",
                table: "AppStudents",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "AppInstructors",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId1",
                table: "AppInstructors",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AppStudents_UserId1",
                table: "AppStudents",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_AppInstructors_UserId1",
                table: "AppInstructors",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AppInstructors_AbpUsers_UserId1",
                table: "AppInstructors",
                column: "UserId1",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AppStudents_AbpUsers_UserId1",
                table: "AppStudents",
                column: "UserId1",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppInstructors_AbpUsers_UserId1",
                table: "AppInstructors");

            migrationBuilder.DropForeignKey(
                name: "FK_AppStudents_AbpUsers_UserId1",
                table: "AppStudents");

            migrationBuilder.DropIndex(
                name: "IX_AppStudents_UserId1",
                table: "AppStudents");

            migrationBuilder.DropIndex(
                name: "IX_AppInstructors_UserId1",
                table: "AppInstructors");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AppStudents");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "AppStudents");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AppInstructors");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "AppInstructors");
        }
    }
}
