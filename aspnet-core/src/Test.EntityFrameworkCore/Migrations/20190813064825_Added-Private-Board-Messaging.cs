﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class AddedPrivateBoardMessaging : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PrivateBoardMessages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SenderId = table.Column<int>(nullable: false),
                    SenderType = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Text = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrivateBoardMessages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrivateBoardMessageReplies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PrivateBoardMessageId = table.Column<int>(nullable: false),
                    SenderId = table.Column<int>(nullable: false),
                    SenderType = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Text = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrivateBoardMessageReplies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrivateBoardMessageReplies_PrivateBoardMessages_PrivateBoardMessageId",
                        column: x => x.PrivateBoardMessageId,
                        principalTable: "PrivateBoardMessages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PrivateBoardMessageReplies_PrivateBoardMessageId",
                table: "PrivateBoardMessageReplies",
                column: "PrivateBoardMessageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PrivateBoardMessageReplies");

            migrationBuilder.DropTable(
                name: "PrivateBoardMessages");
        }
    }
}
