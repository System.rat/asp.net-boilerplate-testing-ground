﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test.Migrations
{
    public partial class ValueObjectConversion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppOfficeAssignments_AppInstructors_InstructorId",
                table: "AppOfficeAssignments");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "AppOfficeAssignments");

            migrationBuilder.RenameColumn(
                name: "InstructorId",
                table: "AppOfficeAssignments",
                newName: "InstructorId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AppOfficeAssignments_AppInstructors_InstructorId1",
                table: "AppOfficeAssignments",
                column: "InstructorId1",
                principalTable: "AppInstructors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AppOfficeAssignments_AppInstructors_InstructorId1",
                table: "AppOfficeAssignments");

            migrationBuilder.RenameColumn(
                name: "InstructorId1",
                table: "AppOfficeAssignments",
                newName: "InstructorId");

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "AppOfficeAssignments",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AppOfficeAssignments_AppInstructors_InstructorId",
                table: "AppOfficeAssignments",
                column: "InstructorId",
                principalTable: "AppInstructors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
