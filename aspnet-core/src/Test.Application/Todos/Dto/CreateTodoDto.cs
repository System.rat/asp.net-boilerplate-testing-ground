﻿using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;


namespace Test.Todos.Dto
{
    [AutoMapTo(typeof(Todo))]
    public class CreateTodoDto
    {
        [Required]
        public string TaskName { get; set; }

        public string TaskDescription { get; set; }

        [Required]
        public int UserId { get; set; }
    }
}
