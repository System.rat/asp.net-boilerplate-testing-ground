﻿using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace Test.Todos.Dto
{
    [AutoMapFrom(typeof(Todo))]
    public class TodoDto : EntityDto
    {
        [Required]
        public string TaskName { get; set; }

        public string TaskDescription { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public string CreationTime { get; set; }
    }
}
