﻿using Abp.Domain.Repositories;
using Abp.Application.Services;
using Abp.Domain.Entities;
using Test.Todos.Dto;
using System.Threading.Tasks;

namespace Test.Todos
{
    public class TodoAppService : ApplicationService
    {
        IRepository<Todo> _todoRepositroy;

        public TodoAppService(IRepository<Todo> todoRepoditory)
        {
            _todoRepositroy = todoRepoditory;
        }

        
        public async Task<TodoDto> Create(CreateTodoDto input)
        {
            var task = ObjectMapper.Map<Todo>(input);
            await _todoRepositroy.InsertAsync(task);
            return ObjectMapper.Map<TodoDto>(task);
        }

        public async Task<Todo> GetById(int id)
        {
            var task = await _todoRepositroy.GetAsync(id);
            if (task == null)
            {
                throw new EntityNotFoundException(typeof(Todo), id);
            }
            return task;
        }

    }
}
