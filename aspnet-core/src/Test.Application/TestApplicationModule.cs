﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Test.Authorization;
using Test.University.Dto;
using Test.University;

namespace Test
{
    [DependsOn(
        typeof(TestCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class TestApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<TestAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(TestApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
            Configuration.Modules.AbpAutoMapper().AddOptionalMapping<UpdateInstructorDto, Instructor>();
        }
    }

    public static class MappingUtils
    {
        /// <summary>
        /// Adds an optional mapping that ignores null fields when converting to the destination object
        /// </summary>
        /// <typeparam name="TSource"> The source type to convert </typeparam>
        /// <typeparam name="TDestination"> The target type to convert to </typeparam>
        public static void AddOptionalMapping<TSource, TDestination> (this IAbpAutoMapperConfiguration mapper)
        {
            mapper.Configurators.Add(
                cfg => cfg.CreateMap<TSource, TDestination>()
                .ForAllMembers(opts => opts.Condition((source, destination, sourceMember) => sourceMember != null)));
        }
    }
}
