﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Test.University.Dto;
using Abp.Authorization;
using Test.Authorization;
using System.Linq;
using Abp.Linq.Extensions;

namespace Test.University
{
    [AbpAuthorize(PermissionNames.Pages_Department)]
    public class DepartmentAppService : AsyncCrudAppService<
        Department,
        DepartmentShortInfoDto,
        int,
        PagedDepartmentResultRequestDto,
        CreateDepartnmentDto,
        UpdateDepartmentDto>
    {
        readonly IRepository<Department> _departmentRepository;

        public DepartmentAppService(IRepository<Department> departmentRepository) : base(departmentRepository)
        {
            _departmentRepository = departmentRepository;
            LocalizationSourceName = "Test";
        }

        /// <summary>
        /// Creates a department
        /// </summary>
        /// <param name="input"> The information for the new department </param>
        /// <returns> The newly created department </returns>
        [AbpAuthorize(PermissionNames.Department_Create)]
        public override async Task<DepartmentShortInfoDto> Create(CreateDepartnmentDto input)
        {
            Department department = ObjectMapper.Map<Department>(input);
            department.TenantId = AbpSession.TenantId;
            Department result = await _departmentRepository.InsertAsync(department);
            return ObjectMapper.Map<DepartmentShortInfoDto>(result);
        }

        /// <summary>
        /// Updates a department
        /// </summary>
        /// <param name="input"> The new information for the department </param>
        [AbpAuthorize(PermissionNames.Department_Manage)]
        public override async Task<DepartmentShortInfoDto> Update(UpdateDepartmentDto input)
        {
            Department department = await _departmentRepository.GetOrThrowAsync(input.Id, L("DepartmentDoesNotExist"));
            if (input.InstructorId == 0) input.InstructorId = null;
            ObjectMapper.Map(input, department);
            return ObjectMapper.Map<DepartmentShortInfoDto>(await _departmentRepository.UpdateAsync(department));
        }

        /// <summary>
        /// Gets a department by it's ID
        /// </summary>
        /// <param name="id"> The ID of the department</param>
        /// <returns> The department information </returns>
        public DepartmentInfoDto GetDetailed(int id)
        {
            Department department = _departmentRepository
                .GetAllIncluding(dep => dep.Courses)
                .GetOrThrow(d => d.Id == id, L("DepartmentDoesNotExist"));
            return ObjectMapper.Map<DepartmentInfoDto>(department);
        }

        /// <summary>
        /// Creates a paged and search filter for getting multiple departments
        /// </summary>
        /// <param name="input"> The page size, skip count, and search parameters </param>
        /// <returns> The filetered query for the search </returns>
        protected override IQueryable<Department> CreateFilteredQuery(PagedDepartmentResultRequestDto input)
        {
            return _departmentRepository.GetAll()
                .WhereIf(!string.IsNullOrEmpty(input.Name), d => d.Name.ToLower().Contains(input.Name.ToLower().Trim()))
                .WhereIf(input.MaxBudget.HasValue, d => d.Budget <= input.MaxBudget.Value)
                .WhereIf(input.MinBudget.HasValue, d => d.Budget >= input.MinBudget.Value)
                .WhereIf(input.MaxStartDate.HasValue, d => d.StartDate <= input.MaxStartDate.Value)
                .WhereIf(input.MinStartDate.HasValue, d => d.StartDate >= input.MinStartDate.Value);
        }
    }
}
