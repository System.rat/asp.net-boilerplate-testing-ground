﻿using Abp.Application.Services.Dto;
using System;

namespace Test.University.Dto
{
    public class PagedDepartmentResultRequestDto : PagedResultRequestDto
    {
        public string Name { get; set; }

        public decimal? MinBudget { get; set; }

        public decimal? MaxBudget { get; set; }

        public DateTime? MinStartDate { get; set; }

        public DateTime? MaxStartDate { get; set; }
    }
}
