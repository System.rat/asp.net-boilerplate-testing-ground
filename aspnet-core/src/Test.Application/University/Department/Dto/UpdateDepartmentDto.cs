﻿using Abp.AutoMapper;
using System;
using Abp.Application.Services.Dto;

namespace Test.University.Dto
{
    [AutoMapTo(typeof(Department))]
    public class UpdateDepartmentDto : EntityDto
    {

        public string Name { get; set; }
        
        public decimal Budget { get; set; }

        public DateTime StartDate { get; set; }

        public int? InstructorId { get; set; }
    }
}
