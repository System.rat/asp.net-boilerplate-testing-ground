﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;


namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Department))]
    public class DepartmentShortInfoDto : EntityDto
    {

        public string Name { get; set; }

        public decimal Budget { get; set; }

        public DateTime StartDate { get; set; }

        public int InstructorId { get; set; }
    }
}
