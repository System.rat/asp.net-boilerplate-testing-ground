﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;


namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Department))]
    public class DepartmentInfoDto
    {
        public string Name { get; set; }

        public decimal Budget { get; set; }

        public DateTime StartDate { get; set; }

        public int InstructorId { get; set; }

        public ICollection<CourseInfoDto> Courses { get; set; }
    }
}
