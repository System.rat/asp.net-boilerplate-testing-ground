﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using System;


namespace Test.University.Dto
{
    [AutoMapTo(typeof(Department))]
    public class CreateDepartnmentDto
    {
        [Required]
        public string Name { get; set; }

        public decimal Budget { get; set; }

        public DateTime StartDate { get; set; } = DateTime.Now;

        public int? InstructorId { get; set; }
    }
}
