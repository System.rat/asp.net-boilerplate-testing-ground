﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Notifications;
using Microsoft.AspNetCore.Http;
using Test.Authorization;
using Test.Notifications;
using Test.University.Boards.Dto;

namespace Test.University.Boards
{
    [AbpAuthorize(PermissionNames.Administration)]
    public class BoardMessageAppService : AsyncCrudAppService<
        BoardMessage,
        BoardMessageDto,
        int,
        PagedBoardMessageResultRequesDto,
        CreateBoardMessageDto,
        UpdateBoardMessageDto>
    {
        readonly BoardManager _boardManager;
        readonly NotificationService _notificationService;
        readonly CourseAppService _courseAppService;
        readonly IRepository<UploadedFile> _uploadedFileRepository;

        public BoardMessageAppService(
            IRepository<BoardMessage> repository,
            BoardManager boardManager,
            NotificationService notificationService,
            CourseAppService courseAppService,
            IRepository<UploadedFile> uploadedFileRepository)
            : base(repository)
        {
            _courseAppService = courseAppService;
            _notificationService = notificationService;
            _boardManager = boardManager;
            _uploadedFileRepository = uploadedFileRepository;
            LocalizationSourceName = "Test";
        }

        /// <summary>
        /// Posts a new BoardMessage
        /// </summary>
        /// <param name="input"> The board message details to post </param>
        /// <returns> The newly created BoardMessage </returns>
        public override async Task<BoardMessageDto> Create(CreateBoardMessageDto input)
        {
            BoardMessage boardMessage = await _boardManager.CreateBoardMessage(
                input.InstructorId,
                input.CourseId,
                input.Grade,
                input.DueDate,
                input.Text);

            BoardMessageDto boardMessageDto = ObjectMapper.Map<BoardMessageDto>(boardMessage);

            await SendNotifications("NewBoardMessage", boardMessageDto);
            return boardMessageDto;
        }

        /// <summary>
        /// Removes a board message and notifies users
        /// </summary>
        /// <param name="input"> ID of the message </param>
        /// <returns></returns>
        public override async Task Delete(EntityDto<int> input)
        {
            BoardMessageDto boardMessage =  ObjectMapper.Map<BoardMessageDto>(await _boardManager.Delete(input.Id));
            await SendNotifications("BoardMessageDeleted", boardMessage);
        }

        /// <summary>
        /// Uploads a file
        /// </summary>
        /// <param name="file"> The file data </param>
        public void UploadFile(IFormFile file)
        {
            _boardManager.AddFile(file.OpenReadStream(), file.FileName, AbpSession.UserId.Value);
        }

        /// <summary>
        /// Returns all the files that the user owns, or returns all the files
        /// </summary>
        /// <param name="userId"> The optional user to restrict to </param>
        /// <returns></returns>
        public async Task<ListResultDto<UploadedFile>> GetFiles(int? userId)
        {
            List<UploadedFile> files = (await _uploadedFileRepository.GetAllListAsync())
                .WhereIf(userId.HasValue, uf => uf.UserId == userId.Value)
                .ToList();
            return new ListResultDto<UploadedFile>(files);
        }

        /// <summary>
        /// Updates a board message and notifies about it
        /// </summary>
        /// <param name="input"> The new information to use </param>
        /// <returns> The updated message </returns>
        public override async Task<BoardMessageDto> Update(UpdateBoardMessageDto input)
        {
            BoardMessage boardMessage = await _boardManager.UpdateMessage(
                input.Id,
                input.CourseId,
                input.Grade,
                input.DueDate,
                input.Text);

            BoardMessageDto boardMessageDto = ObjectMapper.Map<BoardMessageDto>(boardMessage);

            await SendNotifications("BoardMessageUpdated", boardMessageDto);
            return boardMessageDto;
        }

        /// <summary>
        /// Sends notifications to relevant students
        /// </summary>
        /// <param name="message"> The notification message </param>
        /// <param name="boardMessage"> The board message data to send </param>
        /// <returns></returns>
        private async Task SendNotifications(string message, BoardMessageDto boardMessage)
        {
            if (boardMessage.CourseId.HasValue)
            {
                foreach (var enrollment in _courseAppService.GetEnrollments(boardMessage.CourseId.Value).Items)
                {
                    if (boardMessage.Grade.HasValue && enrollment.Grade != boardMessage.Grade.Value) continue;
                    var notification = new LocalizableMessageNotificationData(new Abp.Localization.LocalizableString(message, LocalizationSourceName));
                    notification["message"] = boardMessage;
                    await _notificationService.NotifyUser(enrollment.StudentId, L(message), notification);
                }
            }
            else
            {
                var notification = new LocalizableMessageNotificationData(new Abp.Localization.LocalizableString(message, LocalizationSourceName));
                notification["message"] = boardMessage;
                await _notificationService.NotifyAll(Authorization.Users.UserEntityType.Student, L(message), notification);
            }
        }

        /// <summary>
        /// Creates a paged and search filter for getting multiple messages
        /// </summary>
        /// <param name="input"> The page size, skip count, and search parameters </param>
        /// <returns> The filetered query for the search </returns>
        protected override IQueryable<BoardMessage> CreateFilteredQuery(PagedBoardMessageResultRequesDto input)
        {
            return _boardManager.GetMessages(
                input.CourseId,
                input.InstructorId,
                input.From,
                input.To);
        }
    }
}
