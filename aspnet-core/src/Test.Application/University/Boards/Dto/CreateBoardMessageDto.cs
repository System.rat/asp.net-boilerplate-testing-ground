﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Test.University.Boards.Dto
{
    public class CreateBoardMessageDto
    {
        [Required]
        public int InstructorId { get; set; }

        public int? CourseId { get; set; }

        [EnumDataType(typeof(Grade))]
        public Grade? Grade { get; set; }

        public DateTime? DueDate { get; set; }

        [Required]
        public string Text { get; set; }
    }
}
