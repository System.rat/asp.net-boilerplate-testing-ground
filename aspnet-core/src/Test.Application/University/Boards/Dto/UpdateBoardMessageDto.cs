﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;


namespace Test.University.Boards.Dto
{
    public class UpdateBoardMessageDto : EntityDto
    {
        public int? CourseId { get; set; }

        [EnumDataType(typeof(Grade))]
        public Grade? Grade { get; set; }

        public DateTime? DueDate { get; set; }

        public string Text { get; set; }
    }
}
