﻿using Abp.Application.Services.Dto;
using System;


namespace Test.University.Boards.Dto
{
    public class PagedBoardMessageResultRequesDto : PagedResultRequestDto
    {
        public int? CourseId { get; set; }

        public int? InstructorId { get; set; }

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }
    }
}
