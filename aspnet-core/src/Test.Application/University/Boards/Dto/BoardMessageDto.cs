﻿using System;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
using Test.University.Dto;
using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Test.University.Boards.Dto
{
    [AutoMapFrom(typeof(BoardMessage))]
    public class BoardMessageDto : EntityDto
    {
        [Required]
        public int InstructorId { get; set; }

        public int? CourseId { get; set; }

        [EnumDataType(typeof(Grade))]
        public Grade? Grade { get; set; }

        [Required]
        public DateTime DatePosted { get; set; }

        public DateTime? DueDate { get; set; }

        [Required]
        public string Text { get; set; }

        public InstructorShortInfoDto Instructor { get; set; }

        public CourseInfoDto Course { get; set; }

        public ICollection<BoardUploadedFileDto> UploadedFiles { get; set; }
    }
}
