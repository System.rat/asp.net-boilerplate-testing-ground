﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Dto
{
    [AutoMapFrom(typeof(UploadedFile))]
    public class BoardUploadedFileDto : EntityDto
    {
        [Required]
        public string FileName { get; set; }

        [Required]
        public long UserId { get; set; }

        [Required]
        public int AttachmentId { get; set; }
    }
}