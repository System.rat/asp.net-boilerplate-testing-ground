﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using System;
using Abp.Application.Services.Dto;

namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Instructor))]
    public class InstructorShortInfoDto : EntityDto
    {

        [Required, StringLength(50, MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required, StringLength(50, MinimumLength = 3)]
        public string LastName { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime HireDate { get; set; }

        public OfficeAssignmentInfoDto OfficeAssignment { get; set; }
    }
}
