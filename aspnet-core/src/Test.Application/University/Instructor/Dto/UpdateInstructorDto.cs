﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Test.University.Dto
{
    [AutoMapTo(typeof(Instructor))]
    public class UpdateInstructorDto : EntityDto
    {

        public string FirstName { get; set; }
        
        public string LastName { get; set; }
    }
}
