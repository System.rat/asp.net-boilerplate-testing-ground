﻿using System.ComponentModel.DataAnnotations;


namespace Test.University.Dto
{
    public class InstructorAddCourseDto
    {
        [Required]
        public int CourseId { get; set; }

        [Required]
        public int InstructorId { get; set; }
    }
}
