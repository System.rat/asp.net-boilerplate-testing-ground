﻿using Abp.Application.Services.Dto;
using System;

namespace Test.University.Dto
{
    public class PagedInstructorResultRequestDto : PagedResultRequestDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? MinHireDate { get; set; }

        public DateTime? MaxHireDate { get; set; }
    }
}
