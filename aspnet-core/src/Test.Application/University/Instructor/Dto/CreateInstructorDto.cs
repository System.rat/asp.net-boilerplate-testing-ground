﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using System;


namespace Test.University.Dto
{
    [AutoMapTo(typeof(Instructor))]
    public class CreateInstructorDto
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public DateTime HireDate { get; set; } = DateTime.Now;
    }
}
