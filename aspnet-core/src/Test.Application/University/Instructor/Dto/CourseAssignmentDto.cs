﻿using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;


namespace Test.University.Dto
{
    [AutoMapFrom(typeof(CourseAssignment))]
    public class CourseAssignmentDto
    {
        [Required]
        public CourseInfoDto Course { get; set; }
    }
}
