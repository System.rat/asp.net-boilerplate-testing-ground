﻿namespace Test.University.Dto
{
    public class GetWithNameInstructorDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
