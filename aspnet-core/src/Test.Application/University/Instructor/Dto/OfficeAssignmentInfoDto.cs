﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;


namespace Test.University.Dto
{
    [AutoMapFrom(typeof(OfficeAssignment))]
    public class OfficeAssignmentInfoDto
    {
        [Required]
        public string Location { get; set; }
    }
}
