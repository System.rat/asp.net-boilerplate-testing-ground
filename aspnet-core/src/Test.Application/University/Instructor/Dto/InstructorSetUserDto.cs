﻿using System.ComponentModel.DataAnnotations;

namespace Test.University.Dto
{
    public class InstructorSetUserDto
    {
        [Required]
        public long UserId;

        [Required]
        public int InstructorId;
    }
}
