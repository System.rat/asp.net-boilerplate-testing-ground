﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Dto
{
    [AutoMapTo(typeof(OfficeAssignment))]
    public class OfficeAssignmentDto
    {
        [Required]
        public int InstructorId { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Location { get; set; }
    }
}
