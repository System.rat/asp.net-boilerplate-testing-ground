﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Test.University.Dto;
using System.Linq;
using Abp.Authorization;
using Test.Authorization;
using Abp.Linq.Extensions;

namespace Test.University
{
    [AbpAuthorize(PermissionNames.Pages_Instructor)]
    public class InstructorAppService : AsyncCrudAppService<
        Instructor,
        InstructorShortInfoDto,
        int,
        PagedInstructorResultRequestDto,
        CreateInstructorDto,
        UpdateInstructorDto>
    {
        readonly IRepository<Instructor> _instructorRepository;
        readonly IRepository<Course> _courseRepository;
        readonly InstructorManager _instructorManager;

        public InstructorAppService(
            IRepository<Instructor> instructorRepository,
            IRepository<Course> courseRepository,
            InstructorManager instructorManager
        ) : base(instructorRepository) {
            _courseRepository = courseRepository;
            _instructorRepository = instructorRepository;
            _instructorManager = instructorManager;
            LocalizationSourceName = "Test";
        }

        /// <summary>
        /// Creates an instructor
        /// </summary>
        /// <param name="input"> The information for the new instructor </param>
        /// <returns> The new instructor </returns>
        [AbpAuthorize(PermissionNames.Instructor_Create)]
        public override async Task<InstructorShortInfoDto> Create(CreateInstructorDto input)
        {
            Instructor instructor = ObjectMapper.Map<Instructor>(input);
            instructor.TenantId = AbpSession.TenantId;
            await _instructorRepository.InsertAsync(instructor);
            return ObjectMapper.Map<InstructorShortInfoDto>(instructor);
        }

        /// <summary>
        /// Gets an instructor by their ID
        /// </summary>
        /// <param name="id"> The ID of the instructor </param>
        /// <returns> Information about the instructor </returns>
        public InstructorInfoDto GetDetailed(int id)
        {
            Instructor instructor = _instructorRepository
                .GetAllIncluding(i => i.OfficeAssignment)
                .GetOrThrow(i => i.Id == id, L("InstructorDoesNotExist"));
            _courseRepository.GetAllIncluding(c => c.CourseAssignments).ToList();
            InstructorInfoDto instructorInfoDto = ObjectMapper.Map<InstructorInfoDto>(instructor);

            // The EntityFramework doesn't load the courses through the CourseAssignment
            // automatically so the joining is done manually
            // NOTE: I might just be a dumb-dumb
            // CONCLUSION: I _AM_ a dumb-dumb, just load the CourseAssignments in through the courses and it will autoload
            /*foreach (Course course in courses) 
            {
                if (course.CourseAssignments.FirstOrDefault(c => c.InstructorId == id) != null)
                {
                    instructorInfoDto.CourseAssignments.Add(new CourseAssignmentDto() { Course = ObjectMapper.Map<CourseInfoDto>(course) });
                }
            }*/
            return instructorInfoDto;
        }

        /// <summary>
        /// Updates an instructor with the given information
        /// </summary>
        /// <param name="input"> The information to update the instructor with (first name, last name) </param>
        /// <returns> The new information </returns>
        [AbpAuthorize(PermissionNames.Instructor_Manage)]
        public override async Task<InstructorShortInfoDto> Update(UpdateInstructorDto input)
        {
            Instructor instructor = await _instructorRepository.GetOrThrowAsync(input.Id, L("InstructorDoesNotExist"));
            ObjectMapper.Map(input, instructor);
            await _instructorRepository.UpdateAsync(instructor);
            return ObjectMapper.Map<InstructorShortInfoDto>(instructor);
        }

        /// <summary>
        /// Creates a paged and search filter for getting multiple instructors
        /// </summary>
        /// <param name="input"> The page size, skip count, and search parameters </param>
        /// <returns> The filetered query for the search </returns>
        protected override IQueryable<Instructor> CreateFilteredQuery(PagedInstructorResultRequestDto input)
        {
            return _instructorRepository.GetAll()
                .WhereIf(!string.IsNullOrEmpty(input.FirstName), i => i.FirstName.ToLower().Contains(input.FirstName.ToLower().Trim()))
                .WhereIf(!string.IsNullOrEmpty(input.LastName), i => i.LastName.ToLower().Contains(input.LastName.ToLower().Trim()))
                .WhereIf(input.MinHireDate.HasValue, i => i.HireDate >= input.MinHireDate.Value)
                .WhereIf(input.MaxHireDate.HasValue, i => i.HireDate <= input.MaxHireDate.Value);
        }

        /// <summary>
        /// Sets the instructors office if the office isn't already assigned to someone
        /// </summary>
        /// <param name="input"> The ID of the instructor and the office location </param>
        /// <returns> `false` if the office was already assigned to someone </returns>
        [AbpAuthorize(PermissionNames.Instructor_Manage)]
        public async Task<bool> SetOffice(OfficeAssignmentDto input)
        {
            return await _instructorManager.AssignOffice(input.InstructorId, input.Location);
        }

        /// <summary>
        /// Assigns a course to the instructor
        /// </summary>
        /// <param name="input"> The ID of the instructor and the ID of the course </param>
        /// <returns> `false` if the course was already assigned to them </returns>
        [AbpAuthorize(PermissionNames.Instructor_Manage)]
        public bool AddCourse(InstructorAddCourseDto input)
        {
            return _instructorManager.AddCourse(input.InstructorId, input.CourseId);
        }

        [AbpAuthorize(PermissionNames.Instructor_Manage)]
        public bool SetUser(InstructorSetUserDto input)
        {
            return _instructorManager.SetUser(input.UserId, input.InstructorId);
        }
    }
}
