﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Authorization;
using Test.Authorization.Users;
using Test.University.Account.PrivateMessages.Dto;
using Test.University.Dto;

namespace Test.University.Account.PrivateMessages
{
    /// <summary>
    /// A service for Instructor/Student accounts for private messaging
    /// </summary>
    [AbpAuthorize(PermissionNames.Student, PermissionNames.Instructor)]
    public class PrivateMessageAppService : TestAppServiceBase
    {
        readonly BoardManager _boardManager;
        readonly IRepository<Student> _studentRepository;
        readonly IRepository<Instructor> _instructorRepository;
        readonly IRepository<PrivateBoardMessage> _privateBoardMessageRepository;
        readonly IRepository<FileAttachment> _fileAttachmentRepository;
        readonly IRepository<PrivateBoardMessageReply> _privateReplyRepository;

        public PrivateMessageAppService(
            BoardManager boardManager,
            IRepository<Student> studentRepository,
            IRepository<Instructor> instructorRepository,
            IRepository<PrivateBoardMessage> privateBoardMessageRepository,
            IRepository<FileAttachment> fileAttachmentRepository,
            IRepository<PrivateBoardMessageReply> privateReplyRepository)
        {
            LocalizationSourceName = "Test";
            _boardManager = boardManager;
            _studentRepository = studentRepository;
            _instructorRepository = instructorRepository;
            _privateBoardMessageRepository = privateBoardMessageRepository;
            _fileAttachmentRepository = fileAttachmentRepository;
            _privateReplyRepository = privateReplyRepository;
        }

        /// <summary>
        /// Sends a private message to a specific entity
        /// </summary>
        /// <param name="input"> The private message to send </param>
        /// <returns> The newly created message </returns>
        public async Task<SharedPrivateMessageListDto> SendPrivateMessage(SharedPrivateMessageDto input)
        {
            Entity entity = await GetFromUserAsync();
            PrivateBoardMessage privateBoardMessage = ObjectMapper.Map<PrivateBoardMessage>(input);
            privateBoardMessage.SenderId = entity.Id;
            privateBoardMessage.SenderType = EntityToType(entity);
            privateBoardMessage.DateCreated = DateTime.Now;
            privateBoardMessage = await _boardManager.CreatePrivateBoardMessage(privateBoardMessage);
            return ObjectMapper.Map<SharedPrivateMessageListDto>(privateBoardMessage);
        }

        /// <summary>
        /// Returns all private messages of the user
        /// </summary>
        /// <returns> The list of messages with short information </returns>
        public async Task<ListResultDto<SharedPrivateMessageListDto>> GetPrivateMessages()
        {
            Entity entity = await GetFromUserAsync();

            List<PrivateBoardMessage> privateBoardMessages = (await _privateBoardMessageRepository
                .GetAllListAsync())
                .Where(pm => (pm.SenderType == EntityToType(entity) && pm.SenderId == entity.Id)
                || (pm.TargetType == EntityToType(entity) && pm.TargetId == entity.Id))
                .OrderByDescending(pm => pm.DateCreated)
                .ToList();
            List<SharedPrivateMessageListDto> privateMessageDtos = ObjectMapper.Map<List<SharedPrivateMessageListDto>>(privateBoardMessages);
            foreach (var msg in privateMessageDtos)
            {
                msg.SenderName = await GetNameFromEntityAsync(msg.SenderType, msg.SenderId);
                msg.TargetName = await GetNameFromEntityAsync(msg.TargetType, msg.TargetId);
            }
            return new ListResultDto<SharedPrivateMessageListDto>(privateMessageDtos);
        }

        /// <summary>
        /// Flattens a file attachment and it's uploaded file
        /// </summary>
        /// <param name="a"> The attachment to flatten </param>
        /// <returns> The flattened uploaded file </returns>
        BoardUploadedFileDto ConvertToDto(FileAttachment a)
        {
            var f = ObjectMapper.Map<BoardUploadedFileDto>(a.File);
            f.AttachmentId = a.Id;
            return f;
        }

        /// <summary>
        /// Gets a private message by it's ID with all of it's replies
        /// </summary>
        /// <param name="messageId"> The message ID </param>
        /// <returns> A private message with a list of replies </returns>
        public SharedPrivateMessageDetailDto GetPrivateMessage(int messageId)
        {
            Entity entity = GetFromUser();
            PrivateBoardMessage privateBoardMessage = _boardManager.GetPrivateMessage(messageId, entity.Id, EntityToType(entity));
            SharedPrivateMessageDetailDto privateMessageDetail = ObjectMapper.Map<SharedPrivateMessageDetailDto>(privateBoardMessage);
            List<BoardUploadedFileDto> uploadedFiles = _fileAttachmentRepository.GetAllIncluding(a => a.File)
                    .Where(a => a.MessageId == privateBoardMessage.Id && a.MessageType == MessageType.Private)
                    .Select(a => ConvertToDto(a))
                    .ToList();
            privateMessageDetail.UploadedFiles = uploadedFiles;
            foreach (var reply in privateMessageDetail.Replies)
            {
                List<BoardUploadedFileDto> uploadedReplyFiles = _fileAttachmentRepository.GetAllIncluding(a => a.File)
                    .Where(a => a.MessageId == reply.Id && a.MessageType == MessageType.Reply)
                    .Select(a => ConvertToDto(a))
                    .ToList();
                reply.UploadedFiles = uploadedReplyFiles;
                reply.SenderName = GetNameFromEntity(reply.SenderType, reply.SenderId);
            }
            privateMessageDetail.Replies = privateMessageDetail.Replies.OrderByDescending(r => r.DateCreated).ToList();
            privateMessageDetail.SenderName = GetNameFromEntity(privateMessageDetail.SenderType, privateMessageDetail.SenderId);
            privateMessageDetail.TargetName = GetNameFromEntity(privateMessageDetail.TargetType, privateMessageDetail.TargetId);
            return privateMessageDetail;
        }

        /// <summary>
        /// Replies to a private message
        /// </summary>
        /// <param name="input"> The reply data </param>
        /// <returns> The newly created reply </returns>
        public async Task<SharedReplyPrivateMessageDetailDto> ReplyPrivateMessage(SharedPrivateMessageReplyDto input)
        {
            Entity entity = await GetFromUserAsync();
            PrivateBoardMessageReply privateBoardMessageReply = ObjectMapper.Map<PrivateBoardMessageReply>(input);
            privateBoardMessageReply.DateCreated = DateTime.Now;
            privateBoardMessageReply.SenderId = entity.Id;
            privateBoardMessageReply.SenderType = EntityToType(entity);
            privateBoardMessageReply = await _boardManager.ReplyPrivateMessage(privateBoardMessageReply);
            return ObjectMapper.Map<SharedReplyPrivateMessageDetailDto>(privateBoardMessageReply);
        }

        /// <summary>
        /// Converts a physical type to an Enum variant
        /// </summary>
        /// <param name="entity"> The entity to check </param>
        /// <returns> The UserEntityType of the entity </returns>
        UserEntityType EntityToType(Entity entity) => entity is Instructor ? UserEntityType.Instructor : UserEntityType.Student;

        /// <summary>
        /// Uploads a file
        /// </summary>
        /// <param name="file"> The file data </param>
        /// <returns> The newly created file information </returns>
        public SharedUploadedFileDto UploadFile(IFormFile file) => ObjectMapper
            .Map<SharedUploadedFileDto>(_boardManager.AddFile(file.OpenReadStream(), file.FileName, AbpSession.UserId.Value));

        /// <summary>
        /// Attaches a file to a message
        /// </summary>
        /// <param name="input"> The file to attach and the message to attach to </param>
        public void AttachFile(SharedAttachFileDto input)
        {
            Entity entity = GetFromUser();
            _boardManager.AttachToMessage(
                input.FileId,
                input.MessageId,
                input.MessageType,
                (int)AbpSession.UserId.Value,
                EntityToType(entity),
                entity.Id);
        }

        /// <summary>
        /// Gets all the file attachments of a specific message
        /// </summary>
        /// <param name="messageId"> The message to get the attachments for </param>
        /// <param name="messageType"> The type of message </param>
        /// <returns> A list of attached files </returns>
        public ListResultDto<SharedFileAttachmentDto> GetAttachments(int messageId, MessageType messageType)
        {
            Entity entity = GetFromUser();
            switch (messageType)
            {
                case MessageType.Board:
                    break;
                case MessageType.Private:
                    {
                        PrivateBoardMessage privateBoardMessage = _privateBoardMessageRepository.GetOrThrow(messageId, L("PrivateMessageDoesNotExist"));
                        if ((
                            privateBoardMessage.SenderType == EntityToType(entity) &&
                            privateBoardMessage.SenderId != entity.Id)
                            && (
                            privateBoardMessage.TargetType == EntityToType(entity) &&
                            privateBoardMessage.TargetId != entity.Id))
                        {
                            throw new UserFriendlyException(L("Can'tAccesMessage"));
                        }
                        break;
                    }
                case MessageType.Reply:
                    {
                        PrivateBoardMessageReply privateBoardMessageReply = _privateReplyRepository.GetOrThrow(messageId, L("PrivateMessageDoesNotExist"));
                        PrivateBoardMessage privateBoardMessage = _privateBoardMessageRepository.GetOrThrow(privateBoardMessageReply.PrivateBoardMessageId, L("PrivateMessageDoesNotExist"));
                        if ((
                            privateBoardMessage.SenderType == EntityToType(entity) &&
                            privateBoardMessage.SenderId != entity.Id)
                            || (
                            privateBoardMessage.TargetType == EntityToType(entity) &&
                            privateBoardMessage.TargetId != entity.Id))
                        {
                            throw new UserFriendlyException(L("Can'tAccesMessage"));
                        }
                        break;
                    }
            }
            List<FileAttachment> fileAttachments = _fileAttachmentRepository.GetAllIncluding(fa => fa.File)
                .Where(fa => fa.MessageType == messageType)
                .Where(fa => fa.MessageId == messageId)
                .ToList();
            return new ListResultDto<SharedFileAttachmentDto>(ObjectMapper.Map<List<SharedFileAttachmentDto>>(fileAttachments));
        }

        /// <summary>
        /// Get's the full name from an entity
        /// </summary>
        /// <param name="entityType"> The type of user entity </param>
        /// <param name="entityId"> The ID of the Instructor/Student entity </param>
        /// <returns> The full name of the entity </returns>
        string GetNameFromEntity(UserEntityType entityType, int entityId)
        {
            if (entityType == UserEntityType.Instructor)
            {
                return _instructorRepository.GetOrThrow(entityId, L("InstructorDoesNotExist")).FullName;
            }
            else
            {
                return _studentRepository.GetOrThrow(entityId, L("StudentDoesNotExist")).FullName;
            }
        }

        /// <summary>
        /// Get's the full name from an entity
        /// </summary>
        /// <param name="entityType"> The type of user entity </param>
        /// <param name="entityId"> The ID of the Instructor/Student entity </param>
        /// <returns> The full name of the entity </returns>
        private async Task<string> GetNameFromEntityAsync(UserEntityType entityType, int entityId)
        {
            if (entityType == UserEntityType.Instructor)
            {
                return (await _instructorRepository.GetOrThrowAsync(entityId, L("InstructorDoesNotExist"))).FullName;
            }
            else
            {
                return (await _studentRepository.GetOrThrowAsync(entityId, L("StudentDoesNotExist"))).FullName;
            }
        }

        async Task<Entity> GetFromUserAsync()
        {
            if (!TestSession.UserEntityType.HasValue) throw new UserFriendlyException(L("UserNotAValidEntity"));
            else if (TestSession.UserEntityType == UserEntityType.Student)
            {
                return await _studentRepository.FirstOrDefaultAsync(s => s.UserId.HasValue && s.UserId.Value == AbpSession.UserId);
            }
            else
            {
                return await _instructorRepository.FirstOrDefaultAsync(i => i.UserId.HasValue && i.UserId.Value == AbpSession.UserId);
            }
        }

        /// <summary>
        /// Gets the Instructor/Student entity from the user
        /// </summary>
        /// <returns> The entity </returns>
        Entity GetFromUser()
        {
            if (!TestSession.UserEntityType.HasValue) throw new UserFriendlyException(L("UserNotAValidEntity"));
            else if (TestSession.UserEntityType == UserEntityType.Student)
            {
                return _studentRepository.FirstOrDefault(s => s.UserId.HasValue && s.UserId.Value == AbpSession.UserId);
            }
            else
            {
                return _instructorRepository.FirstOrDefault(i => i.UserId.HasValue && i.UserId.Value == AbpSession.UserId);
            }
        }
    }
}
