﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using Test.Authorization.Users;

namespace Test.University.Account.PrivateMessages.Dto
{
    [AutoMapTo(typeof(PrivateBoardMessage))]
    public class SharedPrivateMessageDto
    {
        [Required]
        public int TargetId { get; set; }

        [Required, EnumDataType(typeof(UserEntityType))]
        public UserEntityType TargetType { get; set; }

        [Required]
        public string Text { get; set; }
    }
}