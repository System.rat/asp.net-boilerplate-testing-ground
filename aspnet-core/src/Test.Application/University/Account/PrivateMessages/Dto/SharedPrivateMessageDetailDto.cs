﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using Test.Authorization.Users;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Test.University.Dto;

namespace Test.University.Account.PrivateMessages.Dto
{
    [AutoMapFrom(typeof(PrivateBoardMessage))]
    public class SharedPrivateMessageDetailDto : EntityDto
    {
        [Required]
        public int SenderId { get; set; }

        [Required, EnumDataType(typeof(UserEntityType))]
        public UserEntityType SenderType { get; set; }

        [Required]
        public string SenderName { get; set; }

        [Required]
        public int TargetId { get; set; }

        [Required]
        public UserEntityType TargetType { get; set; }

        [Required]
        public string TargetName { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public string Text { get; set; }

        public ICollection<SharedReplyPrivateMessageDetailDto> Replies { get; set; }

        public ICollection<BoardUploadedFileDto> UploadedFiles { get; set; }
    }
}
