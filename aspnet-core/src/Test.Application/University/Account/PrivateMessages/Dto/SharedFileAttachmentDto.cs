﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Test.University.Account.PrivateMessages.Dto
{
    [AutoMapFrom(typeof(FileAttachment))]
    public class SharedFileAttachmentDto : EntityDto
    {
        [Required]
        public int MessageId { get; set; }

        [Required, EnumDataType(typeof(MessageType))]
        public MessageType MessageType { get; set; }

        [Required]
        public SharedUploadedFileDto File { get; set; }
    }
}
