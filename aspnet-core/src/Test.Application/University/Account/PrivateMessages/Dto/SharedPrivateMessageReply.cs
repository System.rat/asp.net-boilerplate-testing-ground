﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Account.PrivateMessages.Dto
{
    [AutoMapTo(typeof(PrivateBoardMessageReply))]
    public class SharedPrivateMessageReplyDto
    {
        [Required]
        public int PrivateBoardMessageId { get; set; }

        [Required]
        public string Text { get; set; }
    }
}
