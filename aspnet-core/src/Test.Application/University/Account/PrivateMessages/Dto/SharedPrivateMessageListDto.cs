﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using System;
using Test.Authorization.Users;
using Abp.Application.Services.Dto;

namespace Test.University.Account.PrivateMessages.Dto
{
    [AutoMapFrom(typeof(PrivateBoardMessage))]
    public class SharedPrivateMessageListDto : EntityDto
    {
        [Required]
        public int SenderId { get; set; }

        [Required, EnumDataType(typeof(UserEntityType))]
        public UserEntityType SenderType { get; set; }

        [Required]
        public string SenderName { get; set; }

        [Required]
        public int TargetId { get; set; }

        [Required]
        public UserEntityType TargetType { get; set; }

        [Required]
        public string TargetName { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public string Text { get; set; }
    }
}
