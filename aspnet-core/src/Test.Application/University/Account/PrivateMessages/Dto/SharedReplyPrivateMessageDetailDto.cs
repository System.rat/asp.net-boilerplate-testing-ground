﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Test.Authorization.Users;
using Test.University.Dto;

namespace Test.University.Account.PrivateMessages.Dto
{
    [AutoMapFrom(typeof(PrivateBoardMessageReply))]
    public class SharedReplyPrivateMessageDetailDto : EntityDto
    {
        [Required]
        public int PrivateBoardMessageId { get; set; }

        [Required]
        public int SenderId { get; set; }

        [Required, EnumDataType(typeof(UserEntityType))]
        public UserEntityType SenderType { get; set; }

        [Required]
        public string SenderName { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public string Text { get; set; }

        public ICollection<BoardUploadedFileDto> UploadedFiles { get; set; }
    }
}
