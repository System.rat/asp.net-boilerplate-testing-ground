﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Account.PrivateMessages.Dto
{
    [AutoMapFrom(typeof(UploadedFile))]
    public class SharedUploadedFileDto : EntityDto
    {
        [Required]
        public string FileName { get; set; }

        [Required]
        public long UserId { get; set; }
    }
}
