﻿using System.ComponentModel.DataAnnotations;

namespace Test.University.Account.PrivateMessages.Dto
{
    public class SharedAttachFileDto
    {
        [Required]
        public int FileId { get; set; }

        [Required]
        public int MessageId { get; set; }

        [Required, EnumDataType(typeof(MessageType))]
        public MessageType MessageType { get; set; }
    }
}
