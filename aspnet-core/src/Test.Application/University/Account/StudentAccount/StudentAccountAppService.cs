﻿using Abp.Domain.Repositories;
using System.Threading.Tasks;
using System.Linq;
using Abp.Application.Services.Dto;
using Test.University.Account.StudentAccount.Dto;
using System.Collections.Generic;
using Test.University.Dto;
using Test.Authorization;
using Abp.Authorization;
using Test.University.Account.Dto;
using Test.University.Boards.Dto;
using Test.Authorization.Users;

namespace Test.University.Account.StudentAccount
{
    /// <summary>
    /// The app service for the student users
    /// </summary>
    [AbpAuthorize(PermissionNames.Student)]
    public class StudentAccountAppService : TestAppServiceBase
    {
        readonly StudentManager _studentManager;
        readonly IRepository<Student> _studentRepository;
        readonly IRepository<Enrollment> _enrollmentRepository;
        readonly IRepository<Instructor> _instructorRepository;
        readonly IRepository<Course> _courseRepository;
        readonly BoardManager _boardManager;
        readonly IRepository<BoardMessage> _boardRepository;
        readonly IRepository<PrivateBoardMessage> _privateBoardMessageRepository;
        readonly IRepository<PrivateBoardMessageReply> _privateReplyRepository;
        readonly IRepository<FileAttachment> _fileAttachmentRepository;

        public StudentAccountAppService(
            StudentManager studentManager,
            IRepository<Student> studentRepository,
            IRepository<Enrollment> enrollmentRepository,
            IRepository<Course> courseRepository,
            IRepository<Instructor> instructorRepository,
            BoardManager boardManager,
            IRepository<BoardMessage> boardRepository,
            IRepository<PrivateBoardMessage> privateBoardMessageRepository,
            IRepository<PrivateBoardMessageReply> privateReplyRepository,
            IRepository<FileAttachment> fileAttachmentRepository)
        {
            _boardRepository = boardRepository;
            _boardManager = boardManager;
            _courseRepository = courseRepository;
            _instructorRepository = instructorRepository;
            _studentRepository = studentRepository;
            _studentManager = studentManager;
            _enrollmentRepository = enrollmentRepository;
            _privateBoardMessageRepository = privateBoardMessageRepository;
            _privateReplyRepository = privateReplyRepository;
            _fileAttachmentRepository = fileAttachmentRepository;
        }

        /// <summary>
        /// Gets all the points given with an optional course to filter by
        /// </summary>
        /// <param name="courseId"> The course ID to filter the results to </param>
        /// <returns> The list of points given </returns>
        public async Task<ListResultDto<StudentPointsListDto>> GetPoints(int? courseId)
        {
            CheckIfStudent();
            List<Points> points = await _studentManager.GetPoints((await GetFromUserAsync()).Id, courseId);
            return new ListResultDto<StudentPointsListDto>(ObjectMapper.Map<List<StudentPointsListDto>>(points));
        }

        /// <summary>
        /// Gets student information from a user ID
        /// </summary>
        /// <param name="userId"> The user ID </param>
        /// <returns> The short student information </returns>
        public async Task<StudentShortInfoDto> GetFromUserId(int userId) => ObjectMapper
            .Map<StudentShortInfoDto>((await _studentRepository
                .GetAllListAsync())
                .GetOrThrow(s => s.UserId == userId, L("StudentDoesNotExist")));

        /// <summary>
        /// Gets all instructors that can grade the student
        /// </summary>
        /// <returns> The list of instructors </returns>
        public async Task<ListResultDto<InstructorShortInfoDto>> GetInstructorsOld()
        {
            CheckIfStudent();
            List<Enrollment> enrollments = await _enrollmentRepository
                .GetAllListAsync(e => e.StudentId == GetFromUser().Id);
            List<Instructor> instructors = _instructorRepository
                .GetAllIncluding(i => i.OfficeAssignment, i => i.CourseAssignments)
                .Where(i => enrollments.Has(e => i.CourseAssignments.Has(c => c.CourseId == e.CourseId)))
                .ToList();
            return new ListResultDto<InstructorShortInfoDto>(ObjectMapper.Map<List<InstructorShortInfoDto>>(instructors));
        }

        /// <summary>
        /// Gets all the instructors with assigned courses relevant to the student
        /// </summary>
        /// <returns> The list of instructors with their assigned courses </returns>
        /// <remarks> This method has a chance to be really slow </remarks>
        public async Task<ListResultDto<StudentInstructorInfoDto>> GetInstructors()
        {
            // NOTE: This method is _horribly_ inefficient
            CheckIfStudent();
            ICollection<Enrollment> enrollments = (await _enrollmentRepository.GetAllListAsync())
                .Where(e => e.StudentId == GetFromUser().Id).ToList();
            List<Instructor> instructors = _instructorRepository
                .GetAllIncluding(i => i.CourseAssignments, i => i.OfficeAssignment)
                .Where(i => i.CourseAssignments.Has(c => enrollments.Has(e => e.CourseId == c.CourseId)))
                .ToList();
            await _courseRepository.GetAllListAsync();
            foreach (var instructor in instructors)
            {
                instructor.CourseAssignments = instructor.CourseAssignments.Where(c => enrollments.Has(e => e.CourseId == c.CourseId)).ToList();
            }
            List<StudentInstructorInfoDto> instructorInfos = ObjectMapper.Map<List<StudentInstructorInfoDto>>(instructors);
            for(int i = 0; i < instructors.Count; i++)
            {
                instructorInfos[i].Courses = ObjectMapper.Map<List<CourseInfoDto>>((await _courseRepository.GetAllListAsync())
                    .Where(c => instructors[i].CourseAssignments.Has(ca => ca.CourseId == c.Id))
                    .ToList());
            }
            return new ListResultDto<StudentInstructorInfoDto>(instructorInfos);
        }

        /// <summary>
        /// Gets all course enrollments
        /// </summary>
        /// <returns> The list of enrollements </returns>
        public async Task<ListResultDto<StudentEnrollmentInfoDto>> GetEnrollments()
        {
            CheckIfStudent();
            Student student = await GetFromUserAsync();
            List<Instructor> instructors = await _instructorRepository.GetAllListAsync();
            List<Enrollment> enrollments = _enrollmentRepository
                .GetAllIncluding(e => e.Course)
                .Where(e => e.StudentId == student.Id)
                .ToList();
            List<StudentEnrollmentInfoDto> enrollmentPoints = ObjectMapper.Map<List<StudentEnrollmentInfoDto>>(enrollments);
            foreach (StudentEnrollmentInfoDto item in enrollmentPoints)
            {
                IEnumerable<Points> points = await _studentManager.GetPoints((await GetFromUserAsync()).Id, item.CourseId);
                item.TotalPoints = points.Sum(p => p.PointCount);
            }
            return new ListResultDto<StudentEnrollmentInfoDto>(enrollmentPoints);
        }

        /// <summary>
        /// Gets all board messages relevant to the student
        /// </summary>
        /// <param name="count"> Optional limit for the list size </param>
        /// <param name="skip"> Optional skip count </param>
        /// <returns> The list of messages </returns>
        public ListResultDto<BoardMessageDto> GetAllBoardMessages(int? count, int? skip)
        {
            CheckIfStudent();
            List<Enrollment> enrollments = _enrollmentRepository
                .GetAll()
                .Where(e => e.StudentId == GetFromUser().Id)
                .ToList();
            IQueryable<BoardMessage> boardQuery = _boardRepository
                .GetAllIncluding(bm => bm.Course, bm => bm.Instructor)
                .Where(bm => !bm.CourseId.HasValue || enrollments.Has(e => e.CourseId == bm.CourseId.Value))
                .OrderByDescending(bm => bm.DatePosted);
            if (skip.HasValue) boardQuery = boardQuery.Skip(skip.Value);
            if (count.HasValue) boardQuery = boardQuery.Take(count.Value);
            List<BoardMessage> boardMessages = boardQuery.ToList();
            List<BoardMessageDto> messageDtos = ObjectMapper.Map<List<BoardMessageDto>>(boardMessages);
            foreach (var message in messageDtos)
            {

                List<BoardUploadedFileDto> uploadedFiles = _fileAttachmentRepository.GetAllIncluding(a => a.File)
                    .Where(a => a.MessageId == message.Id && a.MessageType == MessageType.Board)
                    .Select(a => ConvertToDto(a))
                    .ToList();
                message.UploadedFiles = ObjectMapper.Map<List<BoardUploadedFileDto>>(uploadedFiles);
            }
            return new ListResultDto<BoardMessageDto>(messageDtos);
        }

        /// <summary>
        /// Flattens a file attachment and it's uploaded file
        /// </summary>
        /// <param name="a"> The attachment to flatten </param>
        /// <returns> The flattened uploaded file </returns>
        BoardUploadedFileDto ConvertToDto(FileAttachment a)
        {
            var f = ObjectMapper.Map<BoardUploadedFileDto>(a.File);
            f.AttachmentId = a.Id;
            return f;
        }

        void CheckIfStudent()
        {
            if (TestSession.UserEntityType != UserEntityType.Student) throw new AbpAuthorizationException("Non student entities are unauthorized");
        }

        private Student GetFromUser() => _studentRepository.FirstOrDefault(s => s.UserId.HasValue && s.UserId.Value == AbpSession.UserId);

        private async Task<Student> GetFromUserAsync() => await _studentRepository.FirstOrDefaultAsync(s => s.UserId.HasValue && s.UserId.Value == AbpSession.UserId);
    }
}
