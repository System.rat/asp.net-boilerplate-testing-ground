﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using Test.University.Dto;

namespace Test.University.Account.StudentAccount.Dto
{
    [AutoMapFrom(typeof(Points))]
    public class StudentPointsListDto
    {
        [Required]
        public int PointCount { get; set; }

        public DateTime DateGraded { get; set; }

        [Required]
        public InstructorInfoDto Instructor { get; set; }

        [Required]
        public CourseInfoDto Course { get; set; }
    }
}
