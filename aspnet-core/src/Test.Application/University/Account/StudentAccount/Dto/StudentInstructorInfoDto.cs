﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using Test.University.Dto;


namespace Test.University.Account.Dto
{
    [AutoMapFrom(typeof(Instructor))]
    public class StudentInstructorInfoDto
    {
        [Required]
        public int Id { get; set; }

        [Required, StringLength(50, MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required, StringLength(50, MinimumLength = 3)]
        public string LastName { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime HireDate { get; set; }

        public OfficeAssignmentInfoDto OfficeAssignment { get; set; }

        public ICollection<CourseInfoDto> Courses { get; set; }
    }
}
