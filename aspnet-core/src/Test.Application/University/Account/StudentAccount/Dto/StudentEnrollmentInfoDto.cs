﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using Test.University.Dto;

namespace Test.University.Account.StudentAccount.Dto
{
    [AutoMapFrom(typeof(Enrollment))]
    public class StudentEnrollmentInfoDto
    {
        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [Required]
        public CourseInfoDto Course { get; set; }

        [EnumDataType(typeof(Grade)), Required]
        public Grade Grade { get; set; }

        public int TotalPoints { get; set; }
    }
}
