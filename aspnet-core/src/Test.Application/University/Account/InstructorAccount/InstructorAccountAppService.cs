﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Test.Authorization;
using System.Threading.Tasks;
using System.Linq;
using Test.University.Dto;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Test.University.Account.InstructorAccount.Dto;
using System;
using Test.University.Boards;
using Test.University.Boards.Dto;
using Test.Authorization.Users;

namespace Test.University.Account.InstructorAccount
{
    /// <summary>
    /// The app service for instructor users
    /// </summary>
    [AbpAuthorize(PermissionNames.Instructor)]
    public class InstructorAccountAppService : TestAppServiceBase
    {
        readonly IRepository<Instructor> _instructorRepository;
        readonly IRepository<Course> _courseRepository;
        readonly IRepository<Student> _studentRepository;
        readonly IRepository<PrivateBoardMessage> _privateBoardMessageRepository;
        readonly IRepository<PrivateBoardMessageReply> _privateReplyRepository;
        readonly IRepository<Enrollment> _enrollmentRepository;
        readonly IRepository<Points> _pointsRepository;
        readonly IRepository<UploadedFile> _uploadedFileRepository;
        readonly IRepository<FileAttachment> _fileAttachmentRepository;
        readonly BoardMessageAppService _boardMessageAppService;
        readonly BoardManager _boardManager;
        readonly StudentManager _studentManager;

        public InstructorAccountAppService(
            IRepository<Instructor> instructorRepository,
            IRepository<Course> courseRepository,
            IRepository<Student> studentRepository,
            IRepository<PrivateBoardMessage> privateBoardMessageRepository,
            BoardManager boardManager,
            StudentManager studentManager,
            IRepository<Enrollment> enrollmentRepository,
            IRepository<Points> pointsRepository,
            IRepository<UploadedFile> uploadedFileRepository,
            IRepository<FileAttachment> fileAttachmentRepository,
            IRepository<PrivateBoardMessageReply> privateReplyRepository,
            BoardMessageAppService boardMessageAppService)
        {
            _instructorRepository = instructorRepository;
            _courseRepository = courseRepository;
            _studentRepository = studentRepository;
            _privateBoardMessageRepository = privateBoardMessageRepository;
            _boardManager = boardManager;
            _studentManager = studentManager;
            _enrollmentRepository = enrollmentRepository;
            _pointsRepository = pointsRepository;
            _uploadedFileRepository = uploadedFileRepository;
            _fileAttachmentRepository = fileAttachmentRepository;
            _privateReplyRepository = privateReplyRepository;
            _boardMessageAppService = boardMessageAppService;
        }

        /// <summary>
        /// Gets instructor information from a user ID
        /// </summary>
        /// <param name="id"> The user ID </param>
        /// <returns> The short instructor information </returns>
        public async Task<InstructorShortInfoDto> GetFromUserId(int id) => ObjectMapper
            .Map<InstructorShortInfoDto>(await _instructorRepository
                .GetOrThrowAsync(i => i.UserId == id, L("InstructorDoesNotExist")));

        /// <summary>
        /// Gets all of the assigned courses of the instructor
        /// </summary>
        /// <returns> Instructor information with assigned courses </returns>
        public InstructorInfoDto GetWithCourseAssignments()
        {
            CheckIfInstructor();
            int instructorId = GetFromUser().Id;
            Instructor instructor = _instructorRepository
                .GetAllIncluding(i => i.OfficeAssignment)
                .GetOrThrow(i => i.Id == instructorId, L("InstructorDoesNotExist"));
            _courseRepository.GetAllIncluding(c => c.CourseAssignments, c => c.Department).ToList();
            InstructorInfoDto instructorInfoDto = ObjectMapper.Map<InstructorInfoDto>(instructor);

            // The EntityFramework doesn't load the courses through the CourseAssignment
            // automatically so the joining is done manually
            // NOTE: I might just be a dumb-dumb
            // CONCLUSION: I _AM_ a dumb-dumb, just load the CourseAssignments in through the courses and it will autoload
            /*foreach (Course course in courses)
            {
                if (course.CourseAssignments.FirstOrDefault(c => c.InstructorId == instructorId) != null)
                {
                    instructorInfoDto.CourseAssignments.Add(new CourseAssignmentDto() { Course = ObjectMapper.Map<CourseInfoDto>(course) });
                }
            }*/
            return instructorInfoDto;
        }

        /// <summary>
        /// Gets all the students that the instructor can give points to
        /// </summary>
        /// <returns> The list of short student information </returns>
        public async Task<ListResultDto<StudentShortInfoDto>> GetStudents()
        {
            CheckIfInstructor();
            Instructor instructor = await GetFromUserAsync();
            List<Enrollment> enrollments = await _enrollmentRepository.GetAllListAsync();
            List<CourseAssignment> courseAssignments = new List<CourseAssignment>(_instructorRepository
                .GetAllIncluding(i => i.CourseAssignments)
                .GetOrThrow(i => i.Id == instructor.Id, L("InstructorDoesNotExist"))
                .CourseAssignments);
            List<Student> students = (await _studentRepository
                .GetAllListAsync())
                .Where(s => enrollments.Has(e => e.StudentId == s.Id && courseAssignments.Has(c => c.CourseId == e.CourseId)))
                .ToList();
            return new ListResultDto<StudentShortInfoDto>(ObjectMapper.Map<List<StudentShortInfoDto>>(students));
        }

        /// <summary>
        /// Gets all the instructors
        /// </summary>
        /// <returns> The list of short instructor information </returns>
        public async Task<ListResultDto<InstructorShortInfoDto>> GetInstructors()
        {
            CheckIfInstructor();
            return new ListResultDto<InstructorShortInfoDto>(ObjectMapper.Map<List<InstructorShortInfoDto>>(await _instructorRepository.GetAllListAsync()));
        }

        /// <summary>
        /// Gets all points that the instructor gave with an optional student ID to filter by
        /// </summary>
        /// <param name="studentId"> What student to restrict to </param>
        /// <returns> The list of point information </returns>
        public async Task<ListResultDto<InstructorPointsListDto>> GetPoints(int? studentId)
        {
            CheckIfInstructor();
            Instructor instructor = await GetFromUserAsync();
            List<Points> points = _pointsRepository
                .GetAllIncluding(p => p.Course, p => p.Student)
                .Where(p => p.InstructorId == instructor.Id && (!studentId.HasValue || studentId.Value == p.StudentId))
                .ToList();
            return new ListResultDto<InstructorPointsListDto>(ObjectMapper.Map<List<InstructorPointsListDto>>(points));
        }

        /// <summary>
        /// Gives points to a specific student
        /// </summary>
        /// <param name="input"> The point information to give </param>
        /// <returns> The newly created point information </returns>
        public async Task<InstructorGradeDto> GivePoints(InstructorGradeStudentDto input)
        {
            CheckIfInstructor();
            Instructor instructor = await GetFromUserAsync();
            Points points = ObjectMapper.Map<Points>(input);
            points.InstructorId = instructor.Id;
            points.DateGraded = DateTime.Now;
            return ObjectMapper.Map<InstructorGradeDto>(await _studentManager.GivePoints(points));
        }

        /// <summary>
        /// Updates the point count for a specific point
        /// </summary>
        /// <param name="input"> The new information to update with </param>
        /// <returns> The updated point information </returns>
        public async Task<InstructorGradeDto> UpdatePoints(InstructorChangeGradeDto input)
        {
            CheckIfInstructor();
            Instructor instructor = await GetFromUserAsync();
            Points points = await _pointsRepository
                .GetOrThrowAsync(p => p.InstructorId == instructor.Id && p.Id == input.Id, L("PointsDoNotExist"));
            await _pointsRepository.UpdateAsync(ObjectMapper.Map(input, points));
            return ObjectMapper.Map<InstructorGradeDto>(points);
        }

        /// <summary>
        /// Removes given points from a student
        /// </summary>
        /// <param name="pointsId"> The points to remove </param>
        public async Task RemovePoints(int pointsId)
        {
            CheckIfInstructor();
            await _pointsRepository.DeleteAsync(pointsId);
        }

        /// <summary>
        /// Deletes a file
        /// </summary>
        /// <param name="id"> The ID of the file to delete </param>
        public async Task DeleteFIle(int id)
        {
            CheckIfInstructor();
            await _boardManager.DeleteFile(id, (int)AbpSession.UserId.Value);
        }

        /// <summary>
        /// Gets all the uploaded files
        /// </summary>
        /// <returns> A list of uploaded file information </returns>
        public async Task<ListResultDto<InstructorUploadedFileDto>> GetUploadedFiles()
        {
            CheckIfInstructor();
            List<UploadedFile> uploadedFiles = (await _uploadedFileRepository.GetAllListAsync())
                .Where(uf => uf.UserId == TestSession.TUserId)
                .ToList();
            return new ListResultDto<InstructorUploadedFileDto>(ObjectMapper.Map<List<InstructorUploadedFileDto>>(uploadedFiles));
        }

        /// <summary>
        /// Posts a new board message
        /// </summary>
        /// <param name="input"> The message to post </param>
        /// <returns> The newly created message information </returns>
        public async Task<BoardMessageDto> PostBoardMessage(InstructorPostBoardMessageDto input)
        {
            CheckIfInstructor();
            Instructor instructor = await GetFromUserAsync();
            BoardMessageDto boardMessage = await _boardMessageAppService.Create(new CreateBoardMessageDto()
            {
                InstructorId = instructor.Id,
                CourseId = input.CourseId,
                DueDate = input.DueDate,
                Grade = input.Grade,
                Text = input.Text
            });
            return ObjectMapper.Map<BoardMessageDto>(boardMessage);
        }

        /// <summary>
        /// Deleted a board message
        /// </summary>
        /// <param name="id"> The ID of the message to delete </param>
        public async Task DeleteBoardmessage(int id)
        {
            CheckIfInstructor();
            BoardMessage boardMessage = _boardManager.GetMessages(null, null, null, null)
                .GetOrThrow(bm => bm.Id == id && bm.InstructorId == GetFromUser().Id, L("MessageDoesNotExist"));
            await _boardMessageAppService.Delete(new EntityDto<int>(id));
        }

        /// <summary>
        /// Updates a board message
        /// </summary>
        /// <param name="input"> The new information to update </param>
        /// <returns> The updated board message </returns>
        public async Task<BoardMessageDto> UpdateBoardMessage(UpdateBoardMessageDto input)
        {
            CheckIfInstructor();
            BoardMessage boardMessage = _boardManager.GetMessages(null, null, null, null)
                .GetOrThrow(bm => bm.Id == input.Id && bm.InstructorId == GetFromUser().Id, L("MessageDoesNotExist"));
            return await _boardMessageAppService.Update(input);
        }

        /// <summary>
        /// Gets all board messages
        /// </summary>
        /// <param name="input"> The skip count, amount and search query </param>
        /// <returns> The list of board messages </returns>
        public ListResultDto<InstructorBoardMessageDto> GetBoardMessages(InstructorGetBoardMessagesDto input)
        {
            CheckIfInstructor();
            List<BoardMessage> boardMessages = _boardManager
                .GetMessages(null, input.InstructorId, input.From, input.To)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .OrderByDescending(bm => bm.DatePosted)
                .ToList();
            List<InstructorBoardMessageDto> messageDtos = ObjectMapper.Map<List<InstructorBoardMessageDto>>(boardMessages);
            foreach (var message in messageDtos)
            {
                List<BoardUploadedFileDto> uploadedFiles = _fileAttachmentRepository.GetAllIncluding(a => a.File)
                    .Where(a => a.MessageId == message.Id && a.MessageType == MessageType.Board)
                    .Select(a => ConvertToDto(a))
                    .ToList();
                message.UploadedFiles = ObjectMapper.Map<List<BoardUploadedFileDto>>(uploadedFiles);
            }
            return new ListResultDto<InstructorBoardMessageDto>(messageDtos);
        }

        /// <summary>
        /// Flattens a file attachment and it's uploaded file
        /// </summary>
        /// <param name="a"> The attachment to flatten </param>
        /// <returns> The flattened uploaded file </returns>
        BoardUploadedFileDto ConvertToDto(FileAttachment a)
        {
            var f = ObjectMapper.Map<BoardUploadedFileDto>(a.File);
            f.AttachmentId = a.Id;
            return f;
        }

        void CheckIfInstructor()
        {
            if (TestSession.UserEntityType != UserEntityType.Instructor) throw new AbpAuthorizationException("Non instructor entities are unauthorized");
        }

        /// <summary>
        /// Gets the instructor entity ID from the user
        /// </summary>
        /// <returns> The ID </returns>
        public int GetIdFromUser()
        {
            CheckIfInstructor();
            return GetFromUser().Id;
        }

        private Instructor GetFromUser() => _instructorRepository.FirstOrDefault(i => i.UserId.HasValue && i.UserId.Value == AbpSession.UserId);

        private async Task<Instructor> GetFromUserAsync() => await _instructorRepository.FirstOrDefaultAsync(i => i.UserId.HasValue && i.UserId.Value == AbpSession.UserId);
    }
}
