﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Test.University.Dto;

namespace Test.University.Account.InstructorAccount.Dto
{
    [AutoMapFrom(typeof(Points))]
    public class InstructorPointsListDto : EntityDto
    {
        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [Required, Range(0, 100)]
        public int PointCount { get; set; }

        public DateTime DateGraded { get; set; }

        public StudentShortInfoDto Student { get; set; }
        
        public CourseInfoDto Course { get; set; }
    }
}
