﻿using Abp.Application.Services.Dto;
using System;

namespace Test.University.Account.InstructorAccount.Dto
{
    public class InstructorGetBoardMessagesDto : PagedResultRequestDto
    {
        public int? InstructorId { get; set; }

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }
    }
}
