﻿using Abp.Application.Services.Dto;
using System;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace Test.University.Account.InstructorAccount.Dto
{
    [AutoMapFrom(typeof(Points))]
    public class InstructorGradeDto : EntityDto
    {
        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [Required, Range(0, 100)]
        public int PointCount { get; set; }

        [Required]
        public DateTime DateGraded { get; set; }
    }
}
