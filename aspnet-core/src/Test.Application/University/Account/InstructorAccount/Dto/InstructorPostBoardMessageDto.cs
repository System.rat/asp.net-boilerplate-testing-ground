﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Account.InstructorAccount.Dto
{
    [AutoMapTo(typeof(BoardMessage))]
    public class InstructorPostBoardMessageDto
    {
        public int? CourseId { get; set; }

        [EnumDataType(typeof(Grade))]
        public Grade? Grade { get; set; }

        public DateTime? DueDate { get; set; }

        [Required]
        public string Text { get; set; }
    }
}
