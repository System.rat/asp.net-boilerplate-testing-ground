﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Account.InstructorAccount.Dto
{
    [AutoMapFrom(typeof(UploadedFile))]
    public class InstructorUploadedFileDto : EntityDto
    {
        [Required]
        public string FileName { get; set; }

        [Required]
        public long UserId { get; set; }
    }
}
