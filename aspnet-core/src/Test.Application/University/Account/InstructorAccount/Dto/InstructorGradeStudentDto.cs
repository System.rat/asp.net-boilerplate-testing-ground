﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Account.InstructorAccount.Dto
{
    [AutoMapTo(typeof(Points))]
    public class InstructorGradeStudentDto
    {
        [Required, Range(0, 100)]
        public int PointCount { get; set; }

        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }
    }
}
