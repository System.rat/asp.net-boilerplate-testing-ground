﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Test.University.Dto;

namespace Test.University.Account.InstructorAccount.Dto
{
    [AutoMapFrom(typeof(BoardMessage))]
    public class InstructorBoardMessageDto : EntityDto
    {
        [Required]
        public int InstructorId { get; set; }

        [Required]
        public InstructorShortInfoDto Instructor { get; set; }

        public int? CourseId { get; set; }

        [EnumDataType(typeof(Grade))]
        public Grade? Grade { get; set; }

        [Required]
        public DateTime DatePosted { get; set; }

        public DateTime? DueDate { get; set; }

        [Required]
        public string Text { get; set; }

        public ICollection<BoardUploadedFileDto> UploadedFiles { get; set; }
    }
}
