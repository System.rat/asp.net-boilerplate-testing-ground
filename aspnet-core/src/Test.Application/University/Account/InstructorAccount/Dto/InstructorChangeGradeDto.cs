﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Account.InstructorAccount.Dto
{
    [AutoMapTo(typeof(Points))]
    public class InstructorChangeGradeDto
    {
        [Required]
        public int Id { get; set; }

        [Required, Range(0, 100)]
        public int PointCount { get; set; }
    }
}
