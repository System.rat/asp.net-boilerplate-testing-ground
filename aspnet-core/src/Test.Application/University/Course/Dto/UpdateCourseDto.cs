﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Test.University.Dto
{
    [AutoMapTo(typeof(Course))]
    public class UpdateCourseDto : EntityDto
    {
        public string Title { get; set; }

        public int Credits { get; set; }

        public int DepartmentId { get; set; }
    }
}
