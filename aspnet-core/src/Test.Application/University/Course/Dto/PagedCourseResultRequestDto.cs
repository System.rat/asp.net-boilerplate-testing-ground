﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test.University.Dto
{
    public class PagedCourseResultRequesDto : PagedResultRequestDto
    {
        public string Title { get; set; }

        public int? MinCredits { get; set; }

        public int? MaxCredits { get; set; }
    }
}
