﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Enrollment))]
    public class EnrollmentInfoCourseDto
    {
        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [Required]
        public CourseInfoDto Course { get; set; }

        [EnumDataType(typeof(Grade)), Required]
        public Grade Grade { get; set; }
    }
}
