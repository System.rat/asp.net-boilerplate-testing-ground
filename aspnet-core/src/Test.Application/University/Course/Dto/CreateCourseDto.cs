﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University
{
    [AutoMapTo(typeof(Course))]
    public class CreateCourseDto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public int Credits { get; set; }

        [Required]
        public int DepartnmentId { get; set; }
    }
}
