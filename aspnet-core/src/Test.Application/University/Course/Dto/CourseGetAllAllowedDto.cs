﻿using System.ComponentModel.DataAnnotations;

namespace Test.University.Dto
{
    public class CourseGetAllAllowedDto
    {
        [Required]
        public int StudentId { get; set; }

        [Required]
        public int InstructorId { get; set; }
    }
}
