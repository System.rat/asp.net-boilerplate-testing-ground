﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;


namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Course))]
    public class CourseInfoDto : EntityDto
    {

        [Required]
        public string Title { get; set; }

        [Required]
        public int? Credits { get; set; }

        [Required]
        public int? DepartmentId { get; set; } 

        [Required]
        public DepartmentShortInfoDto Department { get; set; }
    }
}
