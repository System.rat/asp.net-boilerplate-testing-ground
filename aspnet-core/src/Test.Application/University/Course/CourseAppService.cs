﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Test.University.Dto;
using System.Collections.Generic;
using Abp.Authorization;
using Test.Authorization;
using System.Linq;
using Abp.Linq.Extensions;
using System;
using Abp.UI;

namespace Test.University
{
    [AbpAuthorize(PermissionNames.Pages_Course)]
    public class CourseAppService : AsyncCrudAppService<
        Course,
        CourseInfoDto,
        int,
        PagedCourseResultRequesDto,
        CreateCourseDto,
        UpdateCourseDto>
    {
        readonly IRepository<Course> _courseRepository;
        readonly IRepository<Department> _departmentRepository;
        readonly CourseManager _courseManager;

        public CourseAppService(IRepository<Course> courseRepository, IRepository<Department> departmentRepository, CourseManager courseManager) : base(courseRepository)
        {
            _courseManager = courseManager;
            _courseRepository = courseRepository;
            _departmentRepository = departmentRepository;
            LocalizationSourceName = "Test";
        }

        /// <summary>
        /// Creates a course
        /// </summary>
        /// <param name="input"> The information for the new course (title, credits, department) </param>
        [AbpAuthorize(PermissionNames.Course_Create)]
        public override async Task<CourseInfoDto> Create(CreateCourseDto input)
        {
            Department department = await _departmentRepository.GetOrThrowAsync(input.DepartnmentId, L("DepartmentDoesNotExist"));
            Course course = ObjectMapper.Map<Course>(input);
            course.Department = department;
            course.TenantId = AbpSession.TenantId;
            return ObjectMapper.Map<CourseInfoDto>(await _courseRepository.InsertAsync(course));
        }

        /// <summary>
        /// Gets all of the courses that an instructor can "grade" for a given student
        /// </summary>
        /// <param name="input"> The ID of the instructor and student </param>
        /// <returns> The list of courses that match the criteria </returns>
        public async Task<ListResultDto<CourseInfoDto>> GetAllAllowed(CourseGetAllAllowedDto input)
        {
            List<Course> courses = await _courseManager.GetAllAllowed(input.StudentId, input.InstructorId);
            return new ListResultDto<CourseInfoDto>(ObjectMapper.Map<List<CourseInfoDto>>(courses));
        }

        /// <summary>
        /// Updates a course with the provided information
        /// </summary>
        /// <param name="input"> The information to update the course with </param>
        /// <returns> The updated course </returns>
        public override async Task<CourseInfoDto> Update(UpdateCourseDto input)
        {
            try
            {
                Course course = _courseRepository.GetAllIncluding(c => c.Department).GetOrThrow(c => c.Id == input.Id, L("CourseDoesNotExist"));
                Department department = await _departmentRepository.GetOrThrowAsync(input.DepartmentId, L("DepartmentDoesNotExist"));
                course.Title = input.Title;
                course.Credits = input.Credits;
                course.DepartmentId = input.DepartmentId;
                course.Department = department;
                await _courseRepository.UpdateAsync(course);
                return ObjectMapper.Map<CourseInfoDto>(course);
            } catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
        }

        /// <summary>
        /// Gets all of the courses enrollments
        /// </summary>
        /// <param name="id"> The ID of the course </param>
        /// <returns> A list of enrollments with the student's ID, grade (year) </returns>
        public ListResultDto<EnrollmentInfoCourseDto> GetEnrollments(int id)
        {
            Course course = _courseRepository.GetAllIncluding(c => c.Enrollments).GetOrThrow(c => c.Id == id, L("CourseDoesNotExist"));
            return new ListResultDto<EnrollmentInfoCourseDto>(ObjectMapper.Map<List<EnrollmentInfoCourseDto>>(course.Enrollments));
        }

        /// <summary>
        /// Creates a paged and search filter for getting multiple courses
        /// </summary>
        /// <param name="input"> The page size, skip count, and search parameters </param>
        /// <returns> The filetered query for the search </returns>
        protected override IQueryable<Course> CreateFilteredQuery(PagedCourseResultRequesDto input)
        {
            return _courseRepository.GetAllIncluding(c => c.Department)
                    .WhereIf(!string.IsNullOrEmpty(input.Title), c => c.Title.ToLower().Contains(input.Title.ToLower().Trim()))
                    .WhereIf(input.MaxCredits.HasValue, c => c.Credits <= input.MaxCredits.Value)
                    .WhereIf(input.MinCredits.HasValue, c => c.Credits >= input.MinCredits.Value);
        }
    } 
}
