﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Test.University.Dto;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Authorization;
using Test.Authorization;
using System.Linq;
using Abp.Linq.Extensions;

namespace Test.University
{
    [AbpAuthorize(PermissionNames.Pages_Student)]
    public class StudentAppService : AsyncCrudAppService<
        Student,
        StudentShortInfoDto,
        int,
        PagedStudentResultRequestDto,
        CreateStudentDto,
        UpdateStudentDto>
    {
        readonly IRepository<Student> _studentRepository;
        readonly IRepository<Enrollment> _enrollmentRepository;
        readonly IRepository<Course> _courseRepository;
        readonly StudentManager _studentManager;

        public StudentAppService(
            IRepository<Student> studentRepository,
            IRepository<Enrollment> enrollmentRepository,
            IRepository<Course> courseRepository,
            StudentManager studentManager
        ) : base(studentRepository)
        {
            _studentManager = studentManager;
            _courseRepository = courseRepository;
            _enrollmentRepository = enrollmentRepository;
            _studentRepository = studentRepository;
            LocalizationSourceName = "Test";
        }

        /// <summary>
        /// Creates a student
        /// </summary>
        /// <param name="input"> The information required to create the student </param>
        /// <returns> Information about the newly created student </returns>
        [AbpAuthorize(PermissionNames.Student_Create)]
        public override async Task<StudentShortInfoDto> Create(CreateStudentDto input)
        {
            Student student = ObjectMapper.Map<Student>(input);
            student.TenantId = AbpSession.TenantId;
            await _studentRepository.InsertAsync(student);
            return ObjectMapper.Map<StudentShortInfoDto>(student);
        }

        /// <summary>
        /// Gets detailed student information by their ID
        /// </summary>
        /// <param name="id"> The ID of the student </param>
        /// <returns> The detailed student information </returns>
        public StudentInfoDto GetDetailed(int id) => ObjectMapper
            .Map<StudentInfoDto>(_studentRepository
                .GetAllIncluding(s => s.Enrollments)
                .GetOrThrow(s => s.Id == id, L("StudentDoesNotExist")));


        /// <summary>
        /// Adds points to (or "Grades") a student that is enrolled to a course that the instructor is assigned to
        /// </summary>
        /// <param name="input"> The information for giving points (instructor, student, course, amount, when) </param>
        /// <returns> The points that were given </returns>
        [AbpAuthorize(PermissionNames.Student_Manage)]
        public async Task<PointsListDto> GivePoints(GivePointsStudentDto input)
        {
            Points points = ObjectMapper.Map<Points>(input);
            Points givenPoints = await _studentManager.GivePoints(points);
            Student student = await _studentRepository.GetOrThrowAsync(points.StudentId, L("StudentDoesNotExist"));
            return ObjectMapper.Map<PointsListDto>(givenPoints);
        }

        /// <summary>
        /// Gets all the points (or "Grades") of a student
        /// </summary>
        /// <param name="input"> The ID of the student with an optional course to limit to </param>
        /// <returns> The list of grades </returns>
        public async Task<ListResultDto<PointsListDto>> GetPoints(GetPointsInputDto input)
        {
            List<Points> points = await _studentManager.GetPoints(input.StudentId, input.CourseId);
            return new ListResultDto<PointsListDto>(ObjectMapper.Map<List<PointsListDto>>(points));
        }

        /// <summary>
        /// Gets the courses that the student is enrolled in
        /// </summary>
        /// <param name="id"> The ID of the student </param>
        /// <returns> A list of all enrollments </returns>
        public ListResultDto<EnrollmentShortInfoDto> GetEnrolledCourses(int id)
        {
            _studentRepository.GetOrThrow(id, L("StudentDoesNotExist"));
            List<Enrollment> enrollments = _enrollmentRepository
                .GetAllIncluding(e => e.Course)
                .Where(e => e.StudentId == id)
                .ToList();

            return new ListResultDto<EnrollmentShortInfoDto>(ObjectMapper.Map<List<EnrollmentShortInfoDto>>(enrollments));
        }

        /// <summary>
        /// Updates a student with the given information
        /// </summary>
        /// <param name="input"> The information to update the student with </param>
        /// <returns> The new student information </returns>
        [AbpAuthorize(PermissionNames.Student_Manage)]
        public override async Task<StudentShortInfoDto> Update(UpdateStudentDto input)
        {
            Student student = await _studentRepository.GetOrThrowAsync(input.Id, L("StudentDoesNotExist"));
            ObjectMapper.Map(input, student);
            await _studentRepository.UpdateAsync(student);
            return ObjectMapper.Map<StudentShortInfoDto>(student);
        }

        protected override IQueryable<Student> CreateFilteredQuery(PagedStudentResultRequestDto input) => _studentRepository
            .GetAll()
            .WhereIf(!string.IsNullOrEmpty(input.FirstName), s => s.FirstName.ToLower().Contains(input.FirstName.ToLower().Trim()))
            .WhereIf(!string.IsNullOrEmpty(input.LastName), s => s.LastName.ToLower().Contains(input.LastName.ToLower().Trim()))
            .WhereIf(input.MinEnrollmentDate.HasValue, s => s.EnrollmentDate >= input.MinEnrollmentDate.Value)
            .WhereIf(input.MaxEnrollmentDate.HasValue, s => s.EnrollmentDate <= input.MaxEnrollmentDate.Value);

        /// <summary>
        /// Enrolls a student to a course
        /// </summary>
        /// <param name="input"> The information for the enrollment (the student ID, course, grade (year)) </param>
        [AbpAuthorize(PermissionNames.Student_Manage)]
        public async Task Enroll(EnrollStudentDto input)
        {
            Student student = await _studentRepository.GetOrThrowAsync(input.StudentId, L("StudentDoesNotExist"));
            Course course = await _courseRepository.GetOrThrowAsync(input.CourseId, L("CourseDoesNotExist"));
            Enrollment enrollment = new Enrollment();
            enrollment.Student = student;
            enrollment.Course = course;
            enrollment.Grade = input.Grade;
            await _enrollmentRepository.InsertAsync(enrollment);
        }

        [AbpAuthorize(PermissionNames.Student_Manage)]
        public bool SetUser(StudentSetUserDto input)
        {
            return _studentManager.SetUser(input.UserId, input.StudentId);
        }
    }
}
