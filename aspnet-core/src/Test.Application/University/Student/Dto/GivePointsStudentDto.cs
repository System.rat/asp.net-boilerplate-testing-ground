﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using System;


namespace Test.University.Dto
{
    [AutoMapTo(typeof(Points))]
    public class GivePointsStudentDto
    {
        [Required]
        public int InstructorId { get; set; }

        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [Required, Range(0, 100)]
        public int PointCount { get; set; }

        public DateTime DateGraded { get; set; } = DateTime.Now;
    }
}
