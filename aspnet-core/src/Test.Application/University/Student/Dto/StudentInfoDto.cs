﻿using System;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;


namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Student))]
    public class StudentInfoDto
    {
        [Required, StringLength(50, MinimumLength = 3), RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        public string FirstName { get; set; }

        [Required, StringLength(50, MinimumLength = 3), RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        public string LastName { get; set; }

        public DateTime EnrollmentDate { get; set; }

        public ICollection<EnrollmentInfoStudentDto> Enrollments { get; set; }
    }
}
