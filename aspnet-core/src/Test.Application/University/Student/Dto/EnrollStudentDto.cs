﻿using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;


namespace Test.University.Dto
{
    [AutoMapTo(typeof(Enrollment))]
    public class EnrollStudentDto
    {
        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }

        public Grade Grade { get; set; }
    }
}
