﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using System;


namespace Test.University.Dto
{
    [AutoMapTo(typeof(Student))]
    public class CreateStudentDto
    {
        [Required, StringLength(50, MinimumLength = 3), RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        public string FirstName { get; set; }

        [Required, StringLength(50, MinimumLength = 3), RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        public string LastName { get; set; }

        public DateTime EnrollmentDate { get; set; } = DateTime.Now;
    }
}
