﻿using Abp.Application.Services.Dto;
using System;

namespace Test.University.Dto
{
    public class PagedStudentResultRequestDto : PagedResultRequestDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? MinEnrollmentDate { get; set; }

        public DateTime? MaxEnrollmentDate { get; set; }
    }
}
