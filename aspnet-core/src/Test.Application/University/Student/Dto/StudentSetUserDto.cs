﻿using System.ComponentModel.DataAnnotations;

namespace Test.University.Dto
{
    public class StudentSetUserDto
    {
        [Required]
        public long UserId;

        [Required]
        public int StudentId;
    }
}
