﻿using System;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace Test.University.Dto
{
    [AutoMapTo(typeof(Student))]
    public class UpdateStudentDto : EntityDto
    {

        [StringLength(50, MinimumLength = 3)]
        public string FirstName { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string LastName { get; set; }

        public DateTime EnrollmentDate { get; set; } = DateTime.Now;
    }
}
