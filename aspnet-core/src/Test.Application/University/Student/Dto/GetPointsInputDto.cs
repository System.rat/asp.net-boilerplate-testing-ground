﻿using System.ComponentModel.DataAnnotations;

namespace Test.University.Dto
{
    public class GetPointsInputDto
    {
        [Required]
        public int StudentId { get; set; }

        public int? CourseId { get; set; }
    }
}
