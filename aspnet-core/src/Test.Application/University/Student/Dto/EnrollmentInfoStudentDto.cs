﻿using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;


namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Enrollment))]
    public class EnrollmentInfoStudentDto
    {
        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [EnumDataType(typeof(Grade)), Required]
        public Grade Grade { get; set; }
    }
}
