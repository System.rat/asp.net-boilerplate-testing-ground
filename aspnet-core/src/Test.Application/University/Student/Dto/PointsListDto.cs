﻿using System;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Points))]
    public class PointsListDto
    {
        [Required]
        public InstructorShortInfoDto Instructor { get; set; }

        [Required]
        public CourseInfoDto Course { get; set; }

        public DateTime DateGraded { get; set; }

        [Required, Range(0, 100)]
        public int PointCount { get; set; }
    }
}
