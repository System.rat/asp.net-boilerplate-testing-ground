﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Test.University.Dto
{
    [AutoMapFrom(typeof(Enrollment))]
    public class EnrollmentShortInfoDto
    { 
        [Required]
        public int CourseId { get; set; }

        [EnumDataType(typeof(Grade))]
        public Grade? Grade { get; set; }

        public CourseInfoDto Course { get; set; }
    }
}
