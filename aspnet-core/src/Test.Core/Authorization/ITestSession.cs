﻿using Abp.Dependency;
using Test.Authorization.Users;

namespace Test.Authorization
{
    public interface ITestSession : ITransientDependency
    {
        /// <summary>
        /// JWT Name identifier
        /// </summary>
        string NameIdentifier { get; set; }
        
        /// <summary>
        /// The ID of the respective Instructor/Student entity
        /// </summary>
        int? EntityId { get; set; }

        /// <summary>
        /// Is the user an Instructor or Student
        /// </summary>
        UserEntityType? UserEntityType { get; set; }

        long? TUserId { get; set; }
    }
}
