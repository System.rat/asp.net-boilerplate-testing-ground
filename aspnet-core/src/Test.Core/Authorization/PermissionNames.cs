﻿namespace Test.Authorization
{
    public static class PermissionNames
    {
        public const string Administration = "Administration";

        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        public const string Pages_Student = "Pages.Student";

        public const string Pages_Instructor = "Pages.Instructor";

        public const string Pages_Department = "Pages.Department";

        public const string Pages_Course = "Pages.Course";

        public const string Student_Create = "Student.Create";

        public const string Student_Delete = "Student.Delete";

        public const string Student_Manage = "Student.Manage";

        public const string Instructor_Create = "Instructor.Create";

        public const string Instructor_Delete = "Instructor.Delete";

        public const string Instructor_Manage = "Instructor.Manage";

        public const string Course_Create = "Course.Create";

        public const string Course_Delete = "Course.Delete";

        public const string Department_Create = "Department.Create";

        public const string Department_Delete = "Department.Delete";

        public const string Department_Manage = "Department.Manage";

        public const string Student = "Entity.Student";

        public const string Instructor = "Entity.Instructor";
    }
}
