﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Test.Authorization
{
    public class TestAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Administration, L("Administration"));
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Course_Create, L("CreateCourse"));
            context.CreatePermission(PermissionNames.Course_Delete, L("DeleteCOurse"));
            context.CreatePermission(PermissionNames.Pages_Course, L("Courses"));
            context.CreatePermission(PermissionNames.Instructor_Create, L("CreateInstructor"));
            context.CreatePermission(PermissionNames.Instructor_Manage, L("ManageInstructor"));
            context.CreatePermission(PermissionNames.Instructor_Delete, L("DeleteInstructor"));
            context.CreatePermission(PermissionNames.Pages_Instructor, L("Instructors"));
            context.CreatePermission(PermissionNames.Department_Create, L("CreateDepartment"));
            context.CreatePermission(PermissionNames.Department_Manage, L("ManageDepartment"));
            context.CreatePermission(PermissionNames.Department_Delete, L("DeleteDepartment"));
            context.CreatePermission(PermissionNames.Pages_Department, L("Departments"));
            context.CreatePermission(PermissionNames.Student_Create, L("CreateStudent"));
            context.CreatePermission(PermissionNames.Student_Manage, L("ManageStudent"));
            context.CreatePermission(PermissionNames.Student_Delete, L("DeleteStudent"));
            context.CreatePermission(PermissionNames.Pages_Student, L("Students"));
            context.CreatePermission(PermissionNames.Student, L("Student"));
            context.CreatePermission(PermissionNames.Instructor, L("Instructor"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, TestConsts.LocalizationSourceName);
        }
    }
}
