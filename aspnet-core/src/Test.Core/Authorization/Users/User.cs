﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;
using Abp.Extensions;

namespace Test.Authorization.Users
{
    public enum UserEntityType
    {
        Student = 0,
        Instructor = 1
    }

    public class User : AbpUser<User>
    {
        public const string DefaultPassword = "123qwe";

        [EnumDataType(typeof(UserEntityType))]
        public UserEntityType? UserEntityType { get; set; }

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress,
                Roles = new List<UserRole>()
            };

            user.SetNormalizedNames();

            return user;
        }
    }
}
