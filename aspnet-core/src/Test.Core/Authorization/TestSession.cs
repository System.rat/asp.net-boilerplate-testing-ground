﻿using System.Linq;
using System.Security.Claims;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.MultiTenancy;
using Abp.Runtime;
using Abp.Runtime.Session;
using Test.Authorization.Users;

namespace Test.Authorization
{
    /// <summary>
    /// A custom session that contains additional information on Instructor/Student users
    /// </summary>
    public class TestSession : ClaimsAbpSession, ITransientDependency, ITestSession
    {
        public TestSession(
            IPrincipalAccessor principalAccessor,
            IMultiTenancyConfig multiTenancy,
            ITenantResolver tenantResolver,
            IAmbientScopeProvider<SessionOverride> sessionOverrideScopeProvider)
            : base(principalAccessor, multiTenancy, tenantResolver, sessionOverrideScopeProvider)
        {
        }

        private UserEntityType? _userEntityType;

        private string _nameIdentifier;

        private int? _entityId;

        /// <summary>
        /// JWT Name identifier
        /// </summary>
        public string NameIdentifier
        {
            get
            {
                if (!string.IsNullOrEmpty(_nameIdentifier)) return _nameIdentifier;
                else
                {
                    try
                    {
                        _nameIdentifier = PrincipalAccessor
                            .Principal?
                            .Claims
                            .FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                        return _nameIdentifier;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
            set
            {

            }
        }

        /// <summary>
        /// The ID of the respective Instructor/Student entity
        /// </summary>
        public int? EntityId
        {
            get
            {
                if (_entityId.HasValue) return _entityId;
                else
                {
                    try
                    {
                        _entityId = int.Parse(
                            PrincipalAccessor
                            .Principal?
                            .Claims
                            .FirstOrDefault(c => c.Type == typeof(User).FullName + ".EntityId").Value);
                        return _entityId;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
            set
            {

            }
        }

        /// <summary>
        /// Is the user an Instructor or Student
        /// </summary>
        public UserEntityType? UserEntityType
        {
            get
            {
                if (_userEntityType.HasValue) return _userEntityType;
                else
                {
                    try
                    {
                        _userEntityType = (UserEntityType)int.Parse(
                            PrincipalAccessor
                            .Principal?
                            .Claims
                            .FirstOrDefault(c => c.Type == typeof(User).FullName + ".UserEntityType").Value);
                        return _userEntityType;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
            set
            {

            }
        }

        public long? TUserId
        {
            get => base.UserId;
            set { }
        }
    }
}
