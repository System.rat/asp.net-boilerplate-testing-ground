﻿using Abp.Domain.Services;
using Abp.Domain.Repositories;
using System.Linq;
using System.Collections.Generic;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Abp.UI;
using Abp.Notifications;
using Test.Notifications;
using Test.Authorization.Users;

namespace Test.University
{
    public class StudentManager : DomainService
    {
        readonly InstructorManager _instructorManager;
        readonly NotificationService _notificationService;
        readonly IRepository<Instructor> _instructorRepository;
        readonly IRepository<Course> _courseRepository;
        readonly IRepository<Student> _studentRepository;
        readonly IRepository<Points> _pointsRepository;
        readonly IRepository<User, long> _userRepository;

        public StudentManager(
            InstructorManager instructorManager,
            IRepository<Student> studentRepository,
            IRepository<Course> courseRepository,
            IRepository<Instructor> instructorRepository,
            IRepository<Points> pointsRepositroy,
            NotificationService notificationService,
            IRepository<User, long> userRepository)
        {
            LocalizationSourceName = "Test";
            _studentRepository = studentRepository;
            _instructorManager = instructorManager;
            _courseRepository = courseRepository;
            _instructorRepository = instructorRepository;
            _pointsRepository = pointsRepositroy;
            _notificationService = notificationService;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Gets all of the points of a student
        /// </summary>
        /// <param name="id"> The ID of the student </param>
        /// <param name="courseId"> Optional: Which course to get the points from </param>
        /// <returns> A list of all the points </returns>
        public async Task<List<Points>> GetPoints(int id, int? courseId)
        {
            Student student = await _studentRepository.GetOrThrowAsync(id, L("StudentDoesNotExist"));
            Course course = null;
            if (courseId.HasValue)
            {
                course = await _courseRepository.GetOrThrowAsync(courseId.Value, L("CourseDoesNotExist"));
            }

            List<Points> points = _pointsRepository
                    .GetAllIncluding(p => p.Instructor, p => p.Course)
                    .WhereIf(course != null, p => p.Course.Id == course.Id)
                    .Where(p => p.StudentId == id)
                    .ToList();
            return points;
        }

        /// <summary>
        /// Gives points to (or "Grades") a student if the student is enrolled to the course and the instructor has
        /// the course assigned to them
        /// </summary>
        /// <param name="points"> The <see cref="Points"/> object to give </param>
        /// <returns> The points that were added </returns>
        public async Task<Points> GivePoints(Points points)
        {
            Instructor instructor = await _instructorRepository.GetOrThrowAsync(points.InstructorId, L("InstructorDoesNotExist"));
            Student student = await _studentRepository.GetOrThrowAsync(points.StudentId, L("StudentDoesNotExist"));
            Course course = await _courseRepository.GetOrThrowAsync(points.CourseId, L("CourseDoesNotExist"));

            if (!_instructorManager.IsCourseAssigned(instructor.Id, course.Id)) throw new UserFriendlyException(L("InstructorNotAssigned"));
            if (!IsEnrolled(student.Id, course.Id)) throw new UserFriendlyException(L("StudentNotEnrolled"));
            points = await _pointsRepository.InsertAsync(points);
            await CurrentUnitOfWork.SaveChangesAsync();

            if (student.UserId.HasValue)
            {
                var data = new LocalizableMessageNotificationData(new Abp.Localization.LocalizableString("GotPointsNotification", LocalizationSourceName));
                data["points"] = new
                {
                    points.Id,
                    points.PointCount,
                    points.DateGraded,
                    points.CourseId,
                    points.InstructorId,
                    CourseTitle = course.Title,
                    InstructorName = instructor.FirstName + " " + instructor.LastName,

                };
                await _notificationService.NotifyUser(student.UserId.Value, "You got points", data);
            }
            return points;
        }

        /// <summary>
        /// Checks if a student is enrolled in a course
        /// </summary>
        /// <param name="studentId"> The ID of the student </param>
        /// <param name="courseId"> The ID of the course to check </param>
        /// <returns> `true` if the student is enrolled </returns>
        public bool IsEnrolled(int studentId, int courseId)
        {
            Student student = _studentRepository.GetAllIncluding(s => s.Enrollments)
                .GetOrThrow(s => s.Id == studentId, L("StudentDoesNotExist"));
            return student.Enrollments.Has(e => e.CourseId == courseId);
        }

        /// <summary>
        /// Sets this students User account
        /// </summary>
        /// <param name="userId">The Id of the User account</param>
        /// <param name="studentId">The Id of the student</param>
        /// <returns>`true` if the setting is successfull</returns>
        public bool SetUser(long userId, int studentId)
        {
            User user = _userRepository.GetOrThrow(userId, L("UserDoesNotExist"));
            Student student = _studentRepository.GetOrThrow(studentId, L("StudentDoesNotExist"));
            user.UserEntityType = UserEntityType.Student;
            student.UserId = (int)user.Id;
            _studentRepository.Update(student);
            _userRepository.Update(user);
            return true;
        }
    }
}
