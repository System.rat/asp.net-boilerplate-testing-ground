﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.University
{
    public class CourseManager : DomainService
    {
        readonly IRepository<Course> _courseRepository;
        readonly IRepository<Instructor> _instructorRepository;
        readonly IRepository<Student> _studentRepository;

        public CourseManager(IRepository<Course> courseRepository, IRepository<Instructor> instructorRepository, IRepository<Student> studentRepository)
        {
            _courseRepository = courseRepository;
            _instructorRepository = instructorRepository;
            _studentRepository = studentRepository;
            LocalizationSourceName = "Test";
        }

        /// <summary>
        /// Returns a list of courses that can be graded for a student by the instructor 
        /// </summary>
        /// <param name="studentId"> The ID of the enrolled student </param>
        /// <param name="instructorId"> The ID of the instructor assigned for this course </param>
        /// <returns> The list of courses available to be graded </returns>
        public async Task<List<Course>> GetAllAllowed(int studentId, int instructorId)
        {
            Task<Student> studentTask = _studentRepository.GetOrThrowAsync(studentId, L("StudentDoesNotExist"));
            Task<Instructor> instructorTask = _instructorRepository.GetOrThrowAsync(instructorId, L("InstructorDoesNotExist"));
            await Task.WhenAll(studentTask, instructorTask);
            Student student = studentTask.Result;
            Instructor instructor = instructorTask.Result;

            List<Course> courses = _courseRepository
                .GetAllIncluding(c => c.Enrollments, c => c.CourseAssignments)
                .Where(c => c.Enrollments.Has(e => e.StudentId == student.Id))
                .Where(c => c.CourseAssignments.Has(a => a.InstructorId == instructor.Id))
                .ToList();
            return courses;
        }
    }
}
