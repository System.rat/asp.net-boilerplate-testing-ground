﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Test.Authorization.Users;

namespace Test.University
{

    [Table("AppStudents")]
    public class Student : Entity, ISoftDelete, IMayHaveTenant
    {
        /// <summary>
        /// The ID of the <see cref="Authorization.Users.User"/> account that's tied to this student
        /// </summary>
        public int? UserId { get; set; }

        public User User { get; set; }

        [Required, StringLength(50, MinimumLength = 3), RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        public string FirstName { get; set; }

        [Required, StringLength(50, MinimumLength = 3), RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$")]
        public string LastName { get; set; }

        public DateTime EnrollmentDate { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public bool IsDeleted { get; set; }

        public int? TenantId { get; set; }
    }
}
