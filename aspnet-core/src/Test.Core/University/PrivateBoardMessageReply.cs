﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using Test.Authorization.Users;

namespace Test.University
{
    public class PrivateBoardMessageReply: Entity
    {
        [Required]
        public int PrivateBoardMessageId { get; set; }

        public PrivateBoardMessage PrivateBoardMessage { get; set; }

        [Required]
        public int SenderId { get; set; }

        [Required, EnumDataType(typeof(UserEntityType))]
        public UserEntityType SenderType { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public string Text { get; set; }
    }
}
