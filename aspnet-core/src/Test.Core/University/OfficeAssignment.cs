﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Values;
using System.Collections.Generic;

namespace Test.University
{
    [Table("AppOfficeAssignments")]
    public class OfficeAssignment : ValueObject
    {
        [Key]
        public int InstructorId { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Location { get; set; }

        public Instructor Instructor { get; set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Location;
        }
    }
}
