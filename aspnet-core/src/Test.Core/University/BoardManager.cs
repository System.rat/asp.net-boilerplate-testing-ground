﻿using System;
using Abp.Domain.Services;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using System.Linq;
using Abp.Linq.Extensions;
using Abp.UI;
using Test.Scheduling;
using System.IO;
using Test.Authorization.Users;
using Test.Notifications;
using Abp.Notifications;
using Abp.Localization;

namespace Test.University
{
    /// <summary>
    /// Manages board messages, files, private messages and replies
    /// </summary>
    public class BoardManager : DomainService
    {
        readonly IRepository<BoardMessage> _boardMessageReposotiry;
        readonly IRepository<Instructor> _instructorRepository;
        readonly IRepository<Course> _courseRepository;
        readonly IRepository<PrivateBoardMessage> _privateBoardMessageRepository;
        readonly IRepository<PrivateBoardMessageReply> _privateReplyRepository;
        readonly IRepository<UploadedFile> _uploadedFileRepository;
        readonly IRepository<FileAttachment> _fileAttachmentRepository;
        readonly IRepository<Student> _studentRepository;
        readonly InstructorManager _instructorManager;
        readonly DueDateNotificationScheduler _notificationScheduler;
        readonly NotificationService _notificationService;

        public BoardManager(
            IRepository<BoardMessage> boardMessageRepository,
            IRepository<Instructor> instructorRepository,
            IRepository<Course> courseRepository,
            InstructorManager instructorManager,
            IRepository<PrivateBoardMessage> privateBoardMessageRepository,
            IRepository<PrivateBoardMessageReply> privateReplyRepository,
            IRepository<Student> studentRepository,
            DueDateNotificationScheduler notificationScheduler,
            IRepository<UploadedFile> uploadedFileRepository,
            IRepository<FileAttachment> fileAttachmentRepository,
            NotificationService notificationService)
        {
            LocalizationSourceName = "Test";
            _courseRepository = courseRepository;
            _boardMessageReposotiry = boardMessageRepository;
            _instructorRepository = instructorRepository;
            _instructorManager = instructorManager;
            _privateBoardMessageRepository = privateBoardMessageRepository;
            _privateReplyRepository = privateReplyRepository;
            _studentRepository = studentRepository;
            _notificationScheduler = notificationScheduler;
            _uploadedFileRepository = uploadedFileRepository;
            _fileAttachmentRepository = fileAttachmentRepository;
            _notificationService = notificationService;
        }

        /// <summary>
        /// Posts a new board message
        /// </summary>
        /// <param name="instructorId"> The ID of the instructor that is posting </param>
        /// <param name="courseId"> Optional ID of the course to limit the message to </param>
        /// <param name="grade"> Optional grade to limit the message to </param>
        /// <param name="dueDate"> The due date of the "Task" </param>
        /// <param name="text"> The message text </param>
        /// <returns> The newly created message </returns>
        public async Task<BoardMessage> CreateBoardMessage(
            int instructorId,
            int? courseId,
            Grade? grade,
            DateTime? dueDate,
            string text)
        {
            Instructor instructor = await _instructorRepository.GetOrThrowAsync(instructorId, L("InstructorDoesNotExist"));
            Course course = null;
            if (courseId.HasValue)
            {
                if (_instructorManager.IsCourseAssigned(instructorId, courseId.Value))
                    course = await _courseRepository.GetOrThrowAsync(courseId.Value, L("CourseDoesNotExist"));
                else throw new UserFriendlyException(L("InstructorNotAssigned"));
            }
            BoardMessage boardMessage = new BoardMessage()
            {
                Instructor = instructor,
                Course = course,
                Grade = grade,
                DatePosted = DateTime.Now,
                DueDate = dueDate,
                Text = text
            };
            boardMessage = await _boardMessageReposotiry.InsertAsync(boardMessage);
            await CurrentUnitOfWork.SaveChangesAsync();
            if (boardMessage.DueDate.HasValue) _notificationScheduler.Schedule(boardMessage);
            return boardMessage;
        }

        /// <summary>
        /// Saves a file to the uploads directory
        /// </summary>
        /// <param name="file"> The file stream to use </param>
        /// <param name="fileName"> Name of the file </param>
        /// <param name="userId"> The uploading user </param>
        /// <returns> The newly uploaded file information </returns>
        public UploadedFile AddFile(Stream file, string fileName, long userId)
        {
            string dirPath = $@"{TestConsts.FileUploadPath}\User{userId}";
            string filePath = $@"{dirPath}\{fileName}";
            if (File.Exists(filePath))
            {
                throw new UserFriendlyException(L("FileAlreadyExists"));
            }
            Directory.CreateDirectory(dirPath);
            using (var stream = new BinaryReader(file))
            using (var newFile = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                byte[] bytes = new byte[1024];
                int read = 0;
                while ((read = stream.Read(bytes)) > 0)
                {
                    newFile.Write(bytes, 0, read);
                }
            }
            UploadedFile uploaded = new UploadedFile
            {
                FileName = fileName,
                FilePath = filePath,
                UserId = userId
            };
            uploaded = _uploadedFileRepository.Insert(uploaded);
            CurrentUnitOfWork.SaveChanges();
            return uploaded;
        }

        /// <summary>
        /// Deletes a file if the user owns it
        /// </summary>
        /// <param name="id"> ID of the file to delete </param>
        /// <param name="userId"> The deleting user ID </param>
        public async Task DeleteFile(int id, int userId)
        {
            UploadedFile file = await _uploadedFileRepository.GetOrThrowAsync(id, L("FileDoesNotExist"));
            if (file.UserId != userId) throw new UserFriendlyException(L("UserDoesNotOwnFile"));
            await _uploadedFileRepository.DeleteAsync(file);
        }

        /// <summary>
        /// Attaches a file to a message
        /// </summary>
        /// <param name="fileId"> ID of the file to attach </param>
        /// <param name="messageId"> The ID of the message to attach to </param>
        /// <param name="messageType"> The type of the target message </param>
        /// <param name="userId"> The user attaching the file </param>
        /// <param name="userEntityType"> The type of the attaching user </param>
        /// <param name="entityId"> The Instructor/Student entity ID of the attaching user </param>
        public void AttachToMessage(int fileId, int messageId, MessageType messageType, int userId, UserEntityType userEntityType, int entityId)
        {
            UploadedFile uploadedFile = _uploadedFileRepository.GetOrThrow(fileId, L("FileNotFound"));
            if (uploadedFile.UserId != userId) throw new UserFriendlyException(L("FileNotOwned"));
            FileAttachment fileAttachment = new FileAttachment()
            {
                File = uploadedFile,
                MessageId = messageId,
                MessageType = messageType
            };
            switch (messageType)
            {
                case MessageType.Board:
                    BoardMessage boardMessage = _boardMessageReposotiry.GetOrThrow(messageId, L("BoardMessageDoesNotExist"));
                    if (userEntityType != UserEntityType.Instructor) throw new UserFriendlyException(L("StudentsCantPostBoardMessages"));
                    if (boardMessage.InstructorId != entityId) throw new UserFriendlyException(L("BoardMessageNotOwned"));
                    break;
                case MessageType.Private:
                    PrivateBoardMessage privateBoardMessage = _privateBoardMessageRepository.GetOrThrow(messageId, L("PrivateBoardMessageDoesNotExist"));
                    if (privateBoardMessage.SenderType != userEntityType) throw new UserFriendlyException(L("PrivateBoardMessageNotSameType"));
                    if (privateBoardMessage.SenderId != entityId) throw new UserFriendlyException(L("PrivateBoardMessageNotOwned"));
                    break;
                case MessageType.Reply:
                    PrivateBoardMessageReply privateBoardMessageReply = _privateReplyRepository.GetOrThrow(messageId, L("PrivateBoardMessageReplyDoesNotExist"));
                    if (privateBoardMessageReply.SenderType != userEntityType) throw new UserFriendlyException(L("PrivateBoardMessageReplyNotSameType"));
                    if (privateBoardMessageReply.SenderId != entityId) throw new UserFriendlyException(L("PrivateBoardMessageReplyNotOwned"));
                    break;
            }
            _fileAttachmentRepository.Insert(fileAttachment);
        }

        /// <summary>
        /// Sends a private message to a user
        /// </summary>
        /// <param name="privateBoardMessage"> The message to send </param>
        /// <returns> The newly created private message </returns>
        public async Task<PrivateBoardMessage> CreatePrivateBoardMessage(PrivateBoardMessage privateBoardMessage)
        {
            int? targetId;
            switch (privateBoardMessage.SenderType)
            {
                case UserEntityType.Student:
                    targetId = (await _studentRepository.GetOrThrowAsync(privateBoardMessage.SenderId, L("StudentDoesNotExist"))).UserId;
                    break;
                case UserEntityType.Instructor:
                    targetId = (await _instructorRepository.GetOrThrowAsync(privateBoardMessage.SenderId, L("InstructorDoesNotExist"))).UserId;
                    break;
                default:
                    throw new UserFriendlyException(L("ImpossibleException"));
            }
            switch (privateBoardMessage.TargetType)
            {
                case UserEntityType.Student:
                    targetId = (await _studentRepository.GetOrThrowAsync(privateBoardMessage.TargetId, L("StudentDoesNotExist"))).UserId;
                    break;
                case UserEntityType.Instructor:
                    targetId = (await _instructorRepository.GetOrThrowAsync(privateBoardMessage.TargetId, L("InstructorDoesNotExist"))).UserId;
                    break;
                default:
                    throw new UserFriendlyException(L("ImpossibleException"));
            }

            if (privateBoardMessage.SenderType == UserEntityType.Student
                && privateBoardMessage.TargetType == UserEntityType.Student) throw new UserFriendlyException(L("StudentsCantMessageStudents"));

            privateBoardMessage = await _privateBoardMessageRepository.InsertAsync(privateBoardMessage);
            CurrentUnitOfWork.SaveChanges();
            if (targetId.HasValue)
            {
                var notification = new LocalizableMessageNotificationData(new LocalizableString("GotNewMessage", LocalizationSourceName));
                notification["message"] = privateBoardMessage;
                await _notificationService.NotifyUser(targetId.Value, L("New private message"), notification);
            }
            return privateBoardMessage;
        }

        /// <summary>
        /// Replies to a private message
        /// </summary>
        /// <param name="reply"> The reply </param>
        /// <returns> The newly created reply </returns>
        public async Task<PrivateBoardMessageReply> ReplyPrivateMessage(PrivateBoardMessageReply reply)
        {
            int? targetId;
            PrivateBoardMessage privateBoardMessage = await _privateBoardMessageRepository.GetOrThrowAsync(reply.PrivateBoardMessageId, L("MessageDoesNotExist"));
            switch (reply.SenderType)
            {
                case UserEntityType.Student:
                    await _studentRepository.GetOrThrowAsync(reply.SenderId, L("StudentDoesNotExist"));
                    break;
                case UserEntityType.Instructor:
                    await _instructorRepository.GetOrThrowAsync(reply.SenderId, L("InstructorDoesNotExist"));
                    break;
                default:
                    throw new UserFriendlyException(L("ImpossibleException"));
            }
            UserEntityType replyPersonType;
            int replyPersonId;
            if (privateBoardMessage.SenderType == reply.SenderType && privateBoardMessage.SenderId == reply.SenderId)
            {
                replyPersonType = privateBoardMessage.TargetType;
                replyPersonId = privateBoardMessage.TargetId;
            } else
            {
                replyPersonType = privateBoardMessage.SenderType;
                replyPersonId = privateBoardMessage.SenderId;
            }
            switch (replyPersonType)
            {
                case UserEntityType.Student:
                    targetId = (await _studentRepository.GetOrThrowAsync(replyPersonId, L("StudentDoesNotExist"))).UserId;
                    break;
                case UserEntityType.Instructor:
                    targetId = (await _instructorRepository.GetOrThrowAsync(replyPersonId, L("StudentDoesNotExist"))).UserId;
                    break;
                default:
                    throw new UserFriendlyException(L("ImpossibleException"));
            }
            if (reply.SenderId == privateBoardMessage.SenderId || reply.SenderId == privateBoardMessage.TargetId)
            {
                reply = await _privateReplyRepository.InsertAsync(reply);
                CurrentUnitOfWork.SaveChanges();
                if (targetId.HasValue)
                {
                    var notification = new LocalizableMessageNotificationData(new LocalizableString("GotNewMessage", LocalizationSourceName));
                    notification["message"] = reply;
                    await _notificationService.NotifyUser(targetId.Value, L("New private message"), notification);
                }
                return reply;
            }
            throw new UserFriendlyException(L("UserNotPartOfPrivateMessage"));
        }

        /// <summary>
        /// Gets a message
        /// </summary>
        /// <param name="id"> ID of the message </param>
        /// <returns> The requested message </returns>
        public async Task<BoardMessage> GetMessageAsync(int id)
        {
            return await _boardMessageReposotiry.GetOrThrowAsync(id, L("BoardMessageDoesNotExist"));
        }

        /// <summary>
        /// Gets a private message
        /// </summary>
        /// <param name="messageId"> The ID of the message </param>
        /// <param name="entityId"> The requestinf entitiy's ID </param>
        /// <param name="entityType"> The requesting entity's type </param>
        /// <returns> The private message with replies </returns>
        public PrivateBoardMessage GetPrivateMessage(int messageId, int entityId, UserEntityType entityType) => _privateBoardMessageRepository
            .GetAllIncluding(pm => pm.Replies)
            .Where(pm =>
                (pm.SenderId == entityId && pm.SenderType == entityType) ||
                (pm.TargetId == entityId && pm.TargetType == entityType))
            .GetOrThrow(pm => pm.Id == messageId, L("PrivateMessageDoesNotExist"));

        /// <summary>
        /// Gets a query for all board messages based on the provided search terms
        /// </summary>
        /// <param name="courseId"> The optional course to limit to </param>
        /// <param name="instructorId"> The optional instructor to limit to </param>
        /// <param name="from"> Optional minimum date </param>
        /// <param name="to"> Optional maximum date </param>
        /// <returns> The query for the messages </returns>
        public IQueryable<BoardMessage> GetMessages(
            int? courseId,
            int? instructorId,
            DateTime? from,
            DateTime? to)
        {
            return _boardMessageReposotiry
                .GetAllIncluding(bm => bm.Course, bm => bm.Instructor)
                .WhereIf(courseId.HasValue, bm =>
                    bm.Course == null || bm.CourseId.Value == courseId.Value)
                .WhereIf(instructorId.HasValue, bm =>
                    bm.InstructorId == instructorId.Value)
                .WhereIf(from.HasValue, bm =>
                    bm.DatePosted >= from)
                .WhereIf(to.HasValue, bm =>
                    bm.DatePosted <= to);
        }

        /// <summary>
        /// Updates a board message and reschedules if it has a due date
        /// </summary>
        /// <param name="messageId"> The ID of the message to update </param>
        /// <param name="newCourseId"> Optional new course ID </param>
        /// <param name="newGrade"> Optional new grade </param>
        /// <param name="newDueDate"> Optional new due date for the "task" </param>
        /// <param name="newText"> Optional new text </param>
        /// <returns> The updated message </returns>
        public async Task<BoardMessage> UpdateMessage(
            int messageId,
            int? newCourseId,
            Grade? newGrade,
            DateTime? newDueDate,
            string newText = null)
        {
            BoardMessage boardMessage = await _boardMessageReposotiry.GetOrThrowAsync(messageId, L("BoardMessageDoesNotExist"));
            if (newCourseId.HasValue && _instructorManager.IsCourseAssigned(boardMessage.InstructorId, newCourseId.Value))
            {
                boardMessage.Course = await _courseRepository.GetOrThrowAsync(newCourseId.Value, L("CourseDoesNotExist"));
            }
            if (newGrade.HasValue) boardMessage.Grade = newGrade;
            if (newDueDate.HasValue) boardMessage.DueDate = newDueDate;
            if (!string.IsNullOrEmpty(newText)) boardMessage.Text = newText;
            await _boardMessageReposotiry.UpdateAsync(boardMessage);
            if(boardMessage.DueDate.HasValue) _notificationScheduler.StopJob("message" + boardMessage.Id);
            if (boardMessage.DueDate.HasValue) _notificationScheduler.Schedule(boardMessage);
            return boardMessage;
        }

        /// <summary>
        /// Deletes a board message
        /// </summary>
        /// <param name="messageId"> The ID of the message to delete </param>
        public async Task<BoardMessage> Delete(int messageId)
        {
            BoardMessage boardMessage = await _boardMessageReposotiry.GetOrThrowAsync(messageId, L("BoardMessageDoesNotExist"));
            await _boardMessageReposotiry.DeleteAsync(boardMessage);
            if (boardMessage.DueDate.HasValue) _notificationScheduler.StopJob("message" + boardMessage.Id);
            return boardMessage;
        }
    }
}
