﻿using Abp.Domain.Services;
using Abp.Domain.Repositories;
using System.Linq;
using System.Collections.Generic;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using Test.Authorization.Users;

namespace Test.University
{
    public class InstructorManager: DomainService
    {
        readonly IRepository<Instructor> _instructorRepository;
        readonly IRepository<Course> _courseRepository;
        readonly IRepository<User, long> _userRepository;

        public InstructorManager(
            IRepository<Instructor> instructorRepository,
            IRepository<Course> courseRepository,
            IRepository<User, long> userRepository)
        {
            _instructorRepository = instructorRepository;
            _courseRepository = courseRepository;
            _userRepository = userRepository;
            LocalizationSourceName = "Test";
        }

        /// <summary>
        /// Checks if a course is assigned to an instructor
        /// </summary>
        /// <param name="instructorId">The ID of the instructor </param>
        /// <param name="courseId">The ID of the course </param>
        /// <returns> `true` if the course is assigned to the instructor </returns>
        public bool IsCourseAssigned(int instructorId, int courseId)
        {
            Instructor instructor = _instructorRepository.GetAllIncluding(i => i.CourseAssignments)
                .GetOrThrow(i => i.Id == instructorId, L("InstructorDoesNotExist"));
            return instructor.CourseAssignments.Has(c => c.CourseId == courseId);
        }

        /// <summary>
        /// Assigns an office to an instructor
        /// </summary>
        /// <param name="id"> The ID of the instructor </param>
        /// <param name="location"> The location of the office </param>
        /// <returns> `true` if the office can be assigned (Is not already assigned to someone else) </returns>
        public async Task<bool> AssignOffice(int id, string location)
        {
            Instructor instructor = await _instructorRepository.GetOrThrowAsync(id, L("InstructorDoesNotExist"));
            bool exists = _instructorRepository.GetAllIncluding(i => i.OfficeAssignment)
                .Has(i => {
                    if (i.OfficeAssignment == null) return false;
                    else if (i.OfficeAssignment.Location == location) return true;
                    else return false;
                });
            if (exists) return false;
            instructor.OfficeAssignment = new OfficeAssignment() { Location = location, Instructor = instructor };
            await _instructorRepository.UpdateAsync(instructor);
            return true;
        }

        /// <summary>
        /// Gets a list of instructors based on wether they contain the following first name or last name
        /// </summary>
        /// <param name="firstName"> The first name to search for </param>
        /// <param name="lastName"> The last name to search for </param>
        /// <returns> The list of instructors that match the criteria </returns>
        /// <remarks> If the first name and last name are emtpy strings, it returns all of the instructors </remarks>
        public List<Instructor> GetSearch(string firstName, string lastName)
        {
            return _instructorRepository
                .GetAllIncluding(i => i.OfficeAssignment)
                .WhereIf(true, i => i.FirstName.Contains(firstName.Trim()))
                .WhereIf(true, i => i.LastName.Contains(lastName.Trim()))
                .OrderBy(i => i.FirstName)
                .ToList();
        }

        /// <summary>
        /// Adds a course assignment to an instructor
        /// </summary>
        /// <param name="instructorId"> The ID of the instructor </param>
        /// <param name="courseId"> The course to add </param>
        /// <returns> `true` if the course wasn't already assigned </returns>
        public bool AddCourse(int instructorId, int courseId)
        {
            Instructor instructor = _instructorRepository
                .GetAllIncluding(i => i.CourseAssignments)
                .GetOrThrow(i => i.Id == instructorId, L("InstructorDoesNotExist"));
            if (instructor.CourseAssignments.Has(c => c.CourseId == courseId)) return false;
            Course course = _courseRepository
                .GetAllIncluding(c => c.CourseAssignments)
                .GetOrThrow(c => c.Id == courseId, L("CourseDoesNotExist"));
            instructor.CourseAssignments.Add(new CourseAssignment
            {
                Course = course,
                Instructor = instructor
            });
            _instructorRepository.Update(instructor);
            return true;
        }

        /// <summary>
        /// Sets this instructors User account
        /// </summary>
        /// <param name="userId">The Id of the User account</param>
        /// <param name="instructorId">The Id of the Instructor</param>
        /// <returns>`true` if the setting is successfull</returns>
        public bool SetUser(long userId, int instructorId)
        {
            User user = _userRepository.GetOrThrow(userId, L("UserDoesNotExist"));
            Instructor instructor = _instructorRepository.GetOrThrow(instructorId, L("InstructorDoesNotExist"));
            user.UserEntityType = UserEntityType.Instructor;
            instructor.UserId = (int)user.Id;
            _instructorRepository.Update(instructor);
            _userRepository.Update(user);
            return true;
        }
    }
}
