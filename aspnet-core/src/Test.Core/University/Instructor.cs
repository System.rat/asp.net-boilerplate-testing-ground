﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Test.Authorization.Users;

namespace Test.University
{
    [Table("AppInstructors")]
    public class Instructor : Entity, ISoftDelete, IMayHaveTenant
    {
        /// <summary>
        /// The ID of the <see cref="Authorization.Users.User"/> account tied to this instructor
        /// </summary>
        public int? UserId { get; set; }

        public User User { get; set; }

        [Required, StringLength(50)]
        public string FirstName { get; set; }

        [Required, StringLength(50)]
        public string LastName { get; set; }
        
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime HireDate { get; set; }

        public bool IsDeleted { get; set; }

        public int? TenantId { get; set; }

        public string FullName
        {
            get { return FirstName + ", " + LastName; }
        }

        public ICollection<CourseAssignment> CourseAssignments { get; set; }

        public OfficeAssignment OfficeAssignment { get; set; }
    }
}
