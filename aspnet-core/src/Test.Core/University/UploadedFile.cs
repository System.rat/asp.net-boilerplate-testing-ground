﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.University
{
    [Table("AppFiles")]
    public class UploadedFile : Entity
    {
        [Required]
        public string FileName { get; set; }

        [Required]
        public string FilePath { get; set; }

        [Required]
        public long UserId { get; set; }
    }
}
