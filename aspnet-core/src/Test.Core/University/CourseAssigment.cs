﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Test.University
{
    [Table("AppCourseAssignments")]
    public class CourseAssignment
    {
        public int InstructorId { get; set; }

        public int CourseId { get; set; }

        public Instructor Instructor { get; set; }

        public Course Course { get; set; }
    }
}
