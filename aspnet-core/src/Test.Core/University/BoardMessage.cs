﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.University
{
    [Table("AppBoardMessages")]
    public class BoardMessage : Entity
    {
        [Required]
        public int InstructorId { get; set; }

        public int? CourseId { get; set; }
        
        [EnumDataType(typeof(Grade))]
        public Grade? Grade { get; set; }

        [Required]
        public DateTime DatePosted { get; set; }

        public DateTime? DueDate { get; set; }

        [Required]
        public string Text { get; set; }

        public Instructor Instructor { get; set; }

        public Course Course { get; set; }
    }
}
