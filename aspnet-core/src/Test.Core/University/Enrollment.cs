﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;

namespace Test.University
{
    public enum Grade
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        F = 4
    }

    [Table("AppEnrollments")]
    public class Enrollment : Entity
    {
        public int CourseId { get; set; }

        public int StudentId { get; set; }

        [EnumDataType(typeof(Grade))]
        public Grade? Grade { get; set; }

        public Course Course { get; set; }

        public Student Student { get; set; }
    }
}
