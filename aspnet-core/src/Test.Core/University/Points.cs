﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Test.University
{
    [Table("AppPoints")]
    public class Points : Entity
    {
        [Required]
        public int InstructorId { get; set; }

        [Required]
        public int StudentId { get; set; }

        [Required]
        public int CourseId { get; set; }

        [Required, Range(0, 100)]
        public int PointCount { get; set; }

        public DateTime DateGraded { get; set; } = DateTime.Now;

        public Instructor Instructor { get; set; }

        public Student Student { get; set; }

        public Course Course { get; set; }
    }
}
