﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Test.Authorization.Users;

namespace Test.University
{
    public class PrivateBoardMessage : Entity
    {
        [Required]
        public int SenderId { get; set; }
        
        [Required, EnumDataType(typeof(UserEntityType))]
        public UserEntityType SenderType { get; set; }

        [Required]
        public int TargetId { get; set; }

        [Required]
        public UserEntityType TargetType { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public string Text { get; set; }

        public ICollection<PrivateBoardMessageReply> Replies { get; set; }
    }
}
