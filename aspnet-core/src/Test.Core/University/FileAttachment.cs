﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.University
{
    public enum MessageType
    {
        Board = 0,
        Private = 1,
        Reply = 2,
    }

    [Table("AppMessageFileAttachments")]
    public class FileAttachment : Entity
    {
        [Required]
        public int MessageId { get; set; }

        [Required, EnumDataType(typeof(MessageType))]
        public MessageType MessageType { get; set; }

        [Required]
        public UploadedFile File { get; set; }
    }
}
