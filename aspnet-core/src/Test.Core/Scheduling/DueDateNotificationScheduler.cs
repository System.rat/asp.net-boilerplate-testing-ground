﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Test.University;
using System.Collections.Generic;
using System.Linq;
using System;
using Abp.Domain.Uow;
using Hangfire;
using Abp.Linq.Extensions;
using Abp.Notifications;
using Abp.Localization;
using Test.Notifications;

namespace Test.Scheduling
{
    /// <summary>
    /// Schedules due dates from the board repository
    /// </summary>
    public class DueDateNotificationScheduler : ISingletonDependency
    {
        readonly IRepository<BoardMessage> _boardMessageRepository;
        readonly IUnitOfWorkManager _unitOfWorkManager;
        //readonly IQuartzScheduleJobManager _scheduler;
        //readonly IAbpQuartzConfiguration _quartzConfiguration;
        readonly IRepository<Enrollment> _enrollmentRepository;
        readonly NotificationService _notificationService;
        private Dictionary<string, string> _messageIdMap = new Dictionary<string, string>();

        public DueDateNotificationScheduler(
            IRepository<BoardMessage> boardRepositroy, IRepository<Enrollment> enrollmentRepository, NotificationService notificationService, IUnitOfWorkManager unitOfWorkManager)//,
                                                                                                                                                                                   //IQuartzScheduleJobManager scheduler,
                                                                                                                                                                                   //IAbpQuartzConfiguration quartzConfiguration)
        {
            _boardMessageRepository = boardRepositroy;
            _enrollmentRepository = enrollmentRepository;
            _notificationService = notificationService;
            _unitOfWorkManager = unitOfWorkManager;
            //_scheduler = scheduler;
            //_quartzConfiguration = quartzConfiguration;
        }

        /// <summary>
        /// Stops a currently running due date
        /// </summary>
        /// <param name="jobName"></param>
        public void StopJob(string jobName)
        {
            //_quartzConfiguration.Scheduler.DeleteJob(new JobKey(jobName));
            lock (_messageIdMap)
            {
                BackgroundJob.Delete(_messageIdMap[jobName]);
            }
        }

        /// <summary>
        /// Directly schedule a notification
        /// </summary>
        /// <param name="message"> The board message with a due date to schedule </param>
        /// <exception> Throws an exception if the message doesn't have a due date </exception>
        public virtual void Schedule(BoardMessage message)
        {
            /*await _scheduler.ScheduleAsync<DueDateNotificationJob>(x => 
            {
                x.WithIdentity("message" + message.Id)
                .SetJobData(new JobDataMap()
                {
                    new KeyValuePair<string, object>("boardMessage", message)
                });
            }, t => 
            {
                t.StartAt(message.DueDate.Value.ToLocalTime().AddHours(-1))
                .WithSimpleSchedule(x =>
                {
                    x.WithIntervalInMinutes(30)
                    .WithRepeatCount(2);
                });
            });*/
            string id = BackgroundJob.Schedule(() => ExecuteNotification(message.Id, message.CourseId, message.Grade, message.Text, message.DatePosted, message.InstructorId), (message.DueDate - DateTime.Now).Value);
            lock(_messageIdMap) {
                _messageIdMap.Add("message" + message.Id, id);
            }
        }
        
        public void ExecuteNotification(int messageId, int? courseId, Grade? grade, string text, DateTime datePosted, int instructorId)
        {
            //throw new Exception(boardMessage.Text);
            using(_unitOfWorkManager.Begin())
            {
                List<Enrollment> enrollments = _enrollmentRepository.GetAllIncluding(e => e.Student)
                    .WhereIf(courseId.HasValue, (e => e.CourseId == courseId.Value))
                    .WhereIf(grade.HasValue, (e => e.Grade == grade.Value))
                    .ToList();
                enrollments.ForEach(async e =>
                {
                    if (e.Student.UserId.HasValue)
                    {
                        var data = new LocalizableMessageNotificationData(new LocalizableString("DueDateWarning", "Test"));
                        data["boardMessage"] = new BoardMessage
                        {
                            Id = messageId,
                            Grade = grade,
                            CourseId = courseId,
                            DatePosted = datePosted,
                            InstructorId = instructorId,
                            Text = text
                        };
                        await _notificationService.NotifyUser(e.Student.UserId.Value, "Due date warning", data);
                    }
                });
            }
        }
    }
}
