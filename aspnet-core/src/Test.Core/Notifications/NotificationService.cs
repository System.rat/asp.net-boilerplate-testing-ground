﻿using Abp.Application.Services;
using Abp.Dependency;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Abp.Notifications;
using System;
using Test.Authorization.Users;

namespace Test.Notifications
{
    /// <summary>
    /// Sends UserNotifications to specific users or all users
    /// </summary>
    public class NotificationService : ApplicationService, ITransientDependency
    {
        IHubContext<NotificationHub> _hubContext;

        public NotificationService(IHubContext<NotificationHub> hubContext)
        {
            _hubContext = hubContext;
        }

        /// <summary>
        /// Send a message to a specific user
        /// </summary>
        /// <param name="userId"> ID of the user </param>
        /// <param name="notificationName"> The display name of the notification </param>
        /// <param name="notificationData"> Additional notification data to be passed </param>
        /// <param name="notificationSeverity"> The severity of the notification </param>
        /// <returns></returns>
        public async Task NotifyUser(int userId, string notificationName, NotificationData notificationData, NotificationSeverity notificationSeverity = NotificationSeverity.Info)
        {
            var data = new UserNotification
            {
                State = UserNotificationState.Unread,
                UserId = userId,
                TenantId = AbpSession.TenantId.GetValueOrDefault(),
                Id = new Guid(),
                Notification = new TenantNotification()
                {
                    CreationTime = DateTime.Now,
                    NotificationName = notificationName,
                    Severity = notificationSeverity,
                    Data = notificationData
                }
            };

            await _hubContext.Clients.User(userId.ToString()).SendAsync("getNotification", data);
        }

        /// <summary>
        /// Notifies all users with a specific entity type
        /// </summary>
        /// <param name="entityType"> The type of users to notify </param>
        /// <param name="notificationName"> The display name of the notification </param>
        /// <param name="notificationData"> Additional notification data to be passed </param>
        /// <param name="notificationSeverity"> The severity of the notification </param>
        /// <returns></returns>
        public async Task NotifyAll(
            UserEntityType entityType,
            string notificationName,
            NotificationData notificationData,
            NotificationSeverity notificationSeverity = NotificationSeverity.Info)
        {
            var data = new
            {
                State = UserNotificationState.Unread,
                TenantId = AbpSession.TenantId.GetValueOrDefault(),
                Id = new Guid(),
                Notification = new TenantNotification()
                {
                    CreationTime = DateTime.Now,
                    NotificationName = notificationName,
                    Severity = notificationSeverity,
                    Data = notificationData
                }
            };

            await _hubContext.Clients.Group(entityType.ToString()).SendAsync("getNotification", data);
        }
    }
}
