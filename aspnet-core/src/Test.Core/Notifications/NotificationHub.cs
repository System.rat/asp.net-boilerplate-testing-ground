﻿using Abp.AspNetCore.SignalR.Hubs;
using Abp.Auditing;
using Abp.RealTime;
using System;
using System.Threading.Tasks;
using Test.Authorization;

namespace Test.Notifications
{
    /// <summary>
    /// A notification hub that groups Instructor/Student users into their respective groups
    /// </summary>
    public class NotificationHub : AbpCommonHub
    {
        readonly ITestSession _testSession;

        public NotificationHub(IOnlineClientManager onlineClientManager, IClientInfoProvider clientInfoProvider, ITestSession testSession) : base(onlineClientManager, clientInfoProvider)
        {
            _testSession = testSession;
        }

        public override async Task OnConnectedAsync()
        {
            if (_testSession.UserEntityType.HasValue)
                await Groups.AddToGroupAsync(Context.ConnectionId, _testSession.UserEntityType.Value.ToString());
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (_testSession.UserEntityType.HasValue)
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, _testSession.UserEntityType.Value.ToString());
        }
    }
}
