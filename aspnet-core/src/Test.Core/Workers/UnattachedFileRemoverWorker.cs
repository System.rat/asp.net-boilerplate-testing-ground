﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using System.Collections.Generic;
using Test.University;
using System.IO;
using Abp.Dependency;

namespace Test.Workers
{
    /// <summary>
    /// A worker that removes all files from the filesystem if it's not in the database.
    /// It also removes all unlinked (file doesn't actually exist on the filesystem but has an entry) files
    /// </summary>
    public class UnattachedFileRemoverWorker : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        readonly IRepository<UploadedFile> _uploadedFileRepository;

        public UnattachedFileRemoverWorker(
            AbpTimer timer,
            IRepository<UploadedFile> uploadedFileRepository) : base(timer)
        {
            LocalizationSourceName = "Test";
            _uploadedFileRepository = uploadedFileRepository;
            Timer.Period = 5 * 60 * 1000;
            Timer.RunOnStart = true;
        }

        [UnitOfWork]
        protected override void DoWork()
        {
            List<UploadedFile> uploadedFiles = _uploadedFileRepository.GetAllList();
            // Physical check
            foreach (string userDir in Directory.GetDirectories("./App_Data/FileUploads"))
            {
                foreach (string userFile in Directory.GetFiles(userDir))
                {
                    if (!uploadedFiles.Has(uf => uf.FilePath == userFile))
                    {
                        File.Delete(userFile);
                    }
                }
            }

            // Database check
            foreach (UploadedFile file in uploadedFiles)
            {
                if (!File.Exists(file.FilePath))
                {
                    _uploadedFileRepository.Delete(file.Id);
                }
            }
        }
    }
}
