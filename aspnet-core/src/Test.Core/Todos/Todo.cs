﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;

namespace Test.Todos
{
    public class Todo : Entity, IHasCreationTime
    {
        public string TaskName { get; set; }

        public int UserId { get; set; }

        public string TaskDescription { get; set; }

        public DateTime CreationTime { get; set; }

        public Todo()
        {
            CreationTime = DateTime.Now;
        }
    }
}
