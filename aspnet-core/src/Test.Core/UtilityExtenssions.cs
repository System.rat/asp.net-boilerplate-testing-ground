﻿using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Test
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Checks if a collection has an item that matches the predicate
        /// </summary>
        /// <typeparam name="TItem"> The item type </typeparam>
        /// <param name="self"> The collection </param>
        /// <param name="predicate"> The predicate to match against the items in the collection </param>
        /// <returns> `true` if the collection has such an item </returns>
        public static bool Has<TItem> (this ICollection<TItem> self, Func<TItem, bool> predicate)
        {
            return self.Where(predicate).Count() > 0;
        }

        /// <summary>
        /// Gets the first item that matches the predicate or throws a UserFriendlyException with a localized string
        /// </summary>
        /// <typeparam name="T"> The type of the item in the enumeration </typeparam>
        /// <param name="self"> The enumerable to get from </param>
        /// <param name="predicate"> The predicate to match against the items </param>
        /// <param name="localizedString"> The localized string in the UserFriendlyException </param>
        /// <returns> The item requested </returns>
        public static T GetOrThrow<T>(this IEnumerable<T> self, Func<T, bool> predicate, string localizedString) where T : Entity
        {
            T item = self.Where(predicate).FirstOrDefault();
            if (item == null) throw new UserFriendlyException(localizedString);
            return item;
        }
    }

    public static class RepositoryQueryExtensions
    {
        /// <summary>
        /// Gets the first item that matches the ID or throws a UserFriendlyException with a localized string
        /// </summary>
        /// <typeparam name="T"> The type of the item in the repository </typeparam>
        /// <param name="self"> The repository to get from </param>
        /// <param name="id"> ID of the Entity </param>
        /// <param name="localizedString"> The localized string in the UserFriendlyException </param>
        /// <returns> The item requested </returns>
        public static T GetOrThrow<T, U>(this IRepository<T, U> self, U id, string localizedString) where T : Entity<U>
        {
            T item = self.FirstOrDefault(id);
            if (item == null) throw new UserFriendlyException(localizedString);
            return item;
        }

        /// <summary>
        /// Asynchronously gets the first item that matches the ID or throws a UserFriendlyException with a localized string
        /// </summary>
        /// <typeparam name="T"> The type of the item in the repository </typeparam>
        /// <param name="self"> The repository to get from </param>
        /// <param name="id"> ID of the Entity </param>
        /// <param name="localizedString"> The localized string in the UserFriendlyException </param>
        /// <returns> The item requested </returns>
        public static async Task<T> GetOrThrowAsync<T, U>(this IRepository<T, U> self, U id, string localizedString) where T : Entity<U>
        {
            T item = await self.FirstOrDefaultAsync(id);
            if (item == null) throw new UserFriendlyException(localizedString);
            return item;
        }

        /// <summary>
        /// Asynchronously gets the first item that matches the predicate or throws a UserFriendlyException with a localized string
        /// </summary>
        /// <typeparam name="T"> The type of the item in the repository </typeparam>
        /// <param name="self"> The repository to get from </param>
        /// <param name="predicate"> Predicate to match against </param>
        /// <param name="localizedString"> The localized string in the UserFriendlyException </param>
        /// <returns> The item requested </returns>
        public static async Task<T> GetOrThrowAsync<T, U>(this IRepository<T, U> self, Expression<Func<T, bool>> predicate, string localizedString) where T : Entity<U>
        {
            T item = await self.FirstOrDefaultAsync(predicate);
            if (item == null) throw new UserFriendlyException(localizedString);
            return item;
        }

        /// <summary>
        /// Gets the first item that matches the predicate or throws a UserFriendlyException with a localized string
        /// </summary>
        /// <typeparam name="T"> The type of the item in the query </typeparam>
        /// <param name="self"> The query to get from </param>
        /// <param name="predicate"> The predicate to match the items against </param>
        /// <param name="localizedString"> The localized string in the UserFriendlyException </param>
        /// <returns> The item requested </returns>
        public static T GetOrThrow<T>(this IQueryable<T> self, Expression<Func<T, bool>> predicate, string localizedString)
        {
            T item = self.FirstOrDefault(predicate);
            if (item == null) throw new UserFriendlyException(localizedString);
            return item;
        }

        /// <summary>
        /// Checks if a query has an item that matches the predicate
        /// </summary>
        /// <typeparam name="TItem"> The item type </typeparam>
        /// <param name="self"> The query to check </param>
        /// <param name="predicate"> The predicate to match against the items in the query </param>
        /// <returns> `true` if the query has such an item </returns>
        public static bool Has<TItem>(this IQueryable<TItem> self, Func<TItem, bool> predicate)
        {
            return self.Where(predicate).Count() > 0;
        }
    }
}
