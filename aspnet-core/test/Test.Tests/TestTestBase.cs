﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Abp;
using Abp.Authorization.Users;
using Abp.Events.Bus;
using Abp.Events.Bus.Entities;
using Abp.MultiTenancy;
using Abp.Runtime.Session;
using Abp.TestBase;
using Test.Authorization.Users;
using Test.EntityFrameworkCore;
using Test.EntityFrameworkCore.Seed.Host;
using Test.EntityFrameworkCore.Seed.Tenants;
using Test.MultiTenancy;
using Test.University;
using Test.University.Dto;
using Test.Users;
using Test.University.Account.StudentAccount;
using Test.University.Account.InstructorAccount;
using Test.Authorization;
using Test.Tests.University.Account;
using Test.Roles;
using Test.Users.Dto;
using Test.University.Account.PrivateMessages;

namespace Test.Tests
{
    public abstract class TestTestBase : AbpIntegratedTestBase<TestTestModule>
    {
        protected readonly StudentAppService _studentService;
        protected readonly CourseAppService _courseService;
        protected readonly DepartmentAppService _departmentService;
        protected readonly InstructorAppService _instructorService;
        protected readonly IUserAppService _userService;
        protected readonly StudentAccountAppService _studentAccountService;
        protected readonly InstructorAccountAppService _instructorAccountService;
        protected readonly RoleAppService _roleService;
        protected readonly PrivateMessageAppService _messageService;
        public ITestSession TestSession { get; set; }

        protected TestTestBase()
        {
            void NormalizeDbContext(TestDbContext context)
            {
                context.EntityChangeEventHelper = NullEntityChangeEventHelper.Instance;
                context.EventBus = NullEventBus.Instance;
                context.SuppressAutoSetTenantId = true;
            }

            // Seed initial data for host
            AbpSession.TenantId = null;
            UsingDbContext(context =>
            {
                NormalizeDbContext(context);
                new InitialHostDbBuilder(context).Create();
                new DefaultTenantBuilder(context).Create();
            });

            // Seed initial data for default tenant
            AbpSession.TenantId = 1;
            UsingDbContext(context =>
            {
                NormalizeDbContext(context);
                new TenantRoleAndUserBuilder(context, 1).Create();
            });

            LoginAsDefaultTenantAdmin();
            _studentService = Resolve<StudentAppService>();
            _courseService = Resolve<CourseAppService>();
            _departmentService = Resolve<DepartmentAppService>();
            _instructorService = Resolve<InstructorAppService>();
            _userService = Resolve<IUserAppService>();
            _studentAccountService = Resolve<StudentAccountAppService>();
            _instructorAccountService = Resolve<InstructorAccountAppService>();
            LocalIocManager.Register<ITestSession, SimpleTestSession>();
            TestSession = new SimpleTestSession();
            _roleService = Resolve<RoleAppService>();
            _messageService = Resolve<PrivateMessageAppService>();
            _studentAccountService.TestSession = TestSession;
            _instructorAccountService.TestSession = TestSession;
            _messageService.TestSession = TestSession;

            // Populate data
            DateTime now = DateTime.Now;
            int instructorId = 0;
            int departmentId = 0;
            UserDto studentUser1 = null;
            UserDto studentUser2 = null;
            UserDto instructorUser = null;

            int studentRoleId = 0;
            int instructorRoleId = 0;
            WithUnitOfWork(() =>
            {
                studentRoleId = (_roleService.Create(new Roles.Dto.CreateRoleDto
                {
                    Description = "Student",
                    DisplayName = "Student",
                    GrantedPermissions = new System.Collections.Generic.List<string> { PermissionNames.Student, "Users" },
                    Name = "Student"
                }).Result).Id;
                instructorRoleId = (_roleService.Create(new Roles.Dto.CreateRoleDto
                {
                    Description = "Instructor",
                    DisplayName = "Instructor",
                    GrantedPermissions = new System.Collections.Generic.List<string> { PermissionNames.Instructor, PermissionNames.Administration, "Users" },
                    Name = "Instructor"
                }).Result).Id;
            });
            WithUnitOfWork(async () =>
            {
                await _studentService.Create(new CreateStudentDto
                {
                    FirstName = "Bob",
                    LastName = "Bobbert",
                    EnrollmentDate = now
                });
                await _studentService.Create(new CreateStudentDto
                {
                    FirstName = "POSTAL",
                    LastName = "Dude",
                    EnrollmentDate = now
                });
                await _studentService.Create(new CreateStudentDto
                {
                    FirstName = "Jack",
                    LastName = "Daniel",
                    EnrollmentDate = now
                });
                await _studentService.Create(new CreateStudentDto
                {
                    FirstName = "Rick",
                    LastName = "Astley",
                    EnrollmentDate = now
                });
                departmentId = (await _departmentService.Create(new CreateDepartnmentDto
                {
                    Name = "Main",
                })).Id;
                instructorId = (await _instructorService.Create(new CreateInstructorDto
                {
                    FirstName = "Daniel",
                    LastName = "Lazarski",
                    HireDate = now
                })).Id;

                // users
                studentUser1 = await _userService.Create(new Test.Users.Dto.CreateUserDto
                {
                    Name = "POSTAL",
                    Surname = "Dude",
                    EmailAddress = "dude@example.com",
                    Password = "dude",
                    UserName = "PDude"
                });
                studentUser1.RoleNames = new string[] { "Student" };
                await _userService.Update(studentUser1);

                studentUser2 = await _userService.Create(new Test.Users.Dto.CreateUserDto
                {
                    Name = "Jack",
                    Surname = "Daniel",
                    EmailAddress = "jack@example.com",
                    Password = "123",
                    UserName = "jack"
                });
                studentUser2.RoleNames = new string[] { "Student" };
                await _userService.Update(studentUser2);

                instructorUser = await _userService.Create(new Test.Users.Dto.CreateUserDto
                {
                    Name = "Daniel",
                    Surname = "Lazarski",
                    EmailAddress = "daniel@example.com",
                    Password = "give_me_synchrozine",
                    UserName = "detective"
                });
                instructorUser.RoleNames = new string[] { "Instructor" };
                await _userService.Update(instructorUser);
            });
            int courseId = 0;
            WithUnitOfWork(async () =>
            {
                _instructorService.SetUser(new InstructorSetUserDto
                {
                    InstructorId = instructorId,
                    UserId = (int)instructorUser.Id
                });
                _studentService.SetUser(new StudentSetUserDto
                {
                    StudentId = 2,
                    UserId = (int)studentUser1.Id
                });
                _studentService.SetUser(new StudentSetUserDto
                {
                    StudentId = 3,
                    UserId = (int)studentUser2.Id
                });
                courseId = (await _courseService.Create(new CreateCourseDto
                {
                    Id = 111,
                    Credits = 6,
                    DepartnmentId = departmentId,
                    Title = "Math"
                })).Id;
            });
            WithUnitOfWork(async () =>
            {
                if (!_instructorService.AddCourse(new InstructorAddCourseDto
                {
                    CourseId = courseId,
                    InstructorId = instructorId
                }))
                {
                    throw new Exception("Failed");
                }
                await _studentService.Enroll(new EnrollStudentDto
                {
                    CourseId = courseId,
                    Grade = Grade.A,
                    StudentId = 1
                });
                await _studentService.Enroll(new EnrollStudentDto
                {
                    CourseId = courseId,
                    Grade = Grade.A,
                    StudentId = 2
                });
                await _studentService.Enroll(new EnrollStudentDto
                {
                    CourseId = courseId,
                    Grade = Grade.A,
                    StudentId = 3
                });
            });
        }

        #region UsingDbContext

        protected IDisposable UsingTenantId(int? tenantId)
        {
            var previousTenantId = AbpSession.TenantId;
            AbpSession.TenantId = tenantId;
            return new DisposeAction(() => AbpSession.TenantId = previousTenantId);
        }

        protected void UsingDbContext(Action<TestDbContext> action)
        {
            UsingDbContext(AbpSession.TenantId, action);
        }

        protected Task UsingDbContextAsync(Func<TestDbContext, Task> action)
        {
            return UsingDbContextAsync(AbpSession.TenantId, action);
        }

        protected T UsingDbContext<T>(Func<TestDbContext, T> func)
        {
            return UsingDbContext(AbpSession.TenantId, func);
        }

        protected Task<T> UsingDbContextAsync<T>(Func<TestDbContext, Task<T>> func)
        {
            return UsingDbContextAsync(AbpSession.TenantId, func);
        }

        protected void UsingDbContext(int? tenantId, Action<TestDbContext> action)
        {
            using (UsingTenantId(tenantId))
            {
                using (var context = LocalIocManager.Resolve<TestDbContext>())
                {
                    action(context);
                    context.SaveChanges();
                }
            }
        }

        protected async Task UsingDbContextAsync(int? tenantId, Func<TestDbContext, Task> action)
        {
            using (UsingTenantId(tenantId))
            {
                using (var context = LocalIocManager.Resolve<TestDbContext>())
                {
                    await action(context);
                    await context.SaveChangesAsync();
                }
            }
        }

        protected T UsingDbContext<T>(int? tenantId, Func<TestDbContext, T> func)
        {
            T result;

            using (UsingTenantId(tenantId))
            {
                using (var context = LocalIocManager.Resolve<TestDbContext>())
                {
                    result = func(context);
                    context.SaveChanges();
                }
            }

            return result;
        }

        protected async Task<T> UsingDbContextAsync<T>(int? tenantId, Func<TestDbContext, Task<T>> func)
        {
            T result;

            using (UsingTenantId(tenantId))
            {
                using (var context = LocalIocManager.Resolve<TestDbContext>())
                {
                    result = await func(context);
                    await context.SaveChangesAsync();
                }
            }

            return result;
        }

        #endregion

        #region Login

        protected void LoginAsHostAdmin()
        {
            LoginAsHost(AbpUserBase.AdminUserName);
        }

        protected void LoginAsDefaultTenantAdmin()
        {
            LoginAsTenant(AbpTenantBase.DefaultTenantName, AbpUserBase.AdminUserName);
        }

        protected void LoginAsHost(string userName)
        {
            AbpSession.TenantId = null;

            var user =
                UsingDbContext(
                    context =>
                        context.Users.FirstOrDefault(u => u.TenantId == AbpSession.TenantId && u.UserName == userName));
            if (user == null)
            {
                throw new Exception("There is no user: " + userName + " for host.");
            }

            AbpSession.UserId = user.Id;
        }

        protected void LoginAsTenant(string tenancyName, string userName)
        {
            var tenant = UsingDbContext(context => context.Tenants.FirstOrDefault(t => t.TenancyName == tenancyName));
            if (tenant == null)
            {
                throw new Exception("There is no tenant: " + tenancyName);
            }

            AbpSession.TenantId = tenant.Id;

            var user =
                UsingDbContext(
                    context =>
                        context.Users.FirstOrDefault(u => u.TenantId == AbpSession.TenantId && u.UserName == userName));
            if (user == null)
            {
                throw new Exception("There is no user: " + userName + " for tenant: " + tenancyName);
            }

            AbpSession.UserId = user.Id;
        }

        protected void LoginAsEntity(string userName, int EntityId, UserEntityType? type)
        {
            LoginAsTenant(AbpTenantBase.DefaultTenantName, userName);
            TestSession.UserEntityType = type;
            TestSession.EntityId = EntityId;
            TestSession.TUserId = AbpSession.UserId;
        }

        #endregion

        /// <summary>
        /// Gets current user if <see cref="IAbpSession.UserId"/> is not null.
        /// Throws exception if it's null.
        /// </summary>
        protected async Task<User> GetCurrentUserAsync()
        {
            var userId = AbpSession.GetUserId();
            return await UsingDbContext(context => context.Users.SingleAsync(u => u.Id == userId));
        }

        /// <summary>
        /// Gets current tenant if <see cref="IAbpSession.TenantId"/> is not null.
        /// Throws exception if there is no current tenant.
        /// </summary>
        protected async Task<Tenant> GetCurrentTenantAsync()
        {
            var tenantId = AbpSession.GetTenantId();
            return await UsingDbContext(context => context.Tenants.SingleAsync(t => t.Id == tenantId));
        }
    }
}
