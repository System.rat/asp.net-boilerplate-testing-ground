﻿using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.MultiTenancy;
using Abp.Runtime;
using Abp.Runtime.Session;
using Test.Authorization;
using Test.Authorization.Users;

namespace Test.Tests.University.Account
{
    public class SimpleTestSession : ITransientDependency, ITestSession
    {
        public string NameIdentifier { get; set; }
        public int? EntityId { get; set; }
        public UserEntityType? UserEntityType { get; set; }

        public long? TUserId { get; set; }
    }
}
