﻿using Xunit;
using Shouldly;
using System.Threading.Tasks;
using Test.University.Account.InstructorAccount.Dto;
using System.Collections.Generic;
using Test.University.Dto;
using System.Linq;
using System;

namespace Test.Tests.University.Account
{
    public class StudentInstructorAccount_Tests : TestTestBase
    {
        [Fact]
        public async Task AddAndGetPoints()
        {
            // Login as an instructor
            LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
            await WithUnitOfWorkAsync(async () =>
            {
                var result = await _instructorAccountService.GivePoints(new InstructorGradeStudentDto
                {
                    CourseId = 111,
                    PointCount = 13,
                    StudentId = 2
                });
                result.StudentId.ShouldBe(2);
                result.PointCount.ShouldBe(13);
            });
            // Login as a student
            LoginAsEntity("PDude", 2, Authorization.Users.UserEntityType.Student);
            await WithUnitOfWorkAsync(async () =>
            {
                var result = await _studentAccountService.GetPoints(null);
                result.Items.Count.ShouldBeGreaterThan(0);
                result.Items[0].PointCount.ShouldBe(13);
                result.Items[0].Instructor.FirstName.ShouldBe("Daniel");
            });
        }

        [Fact]
        public void GetCourseAssignments_Test()
        {
            WithUnitOfWork(() =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var result = _instructorAccountService.GetWithCourseAssignments();
                result.CourseAssignments.Count.ShouldBeGreaterThan(0);
                result.CourseAssignments.Has(c => c.Course.Id == 111).ShouldBeTrue();
            });
        }

        [Fact]
        public async Task GetStudents_Test()
        {
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var result = await _instructorAccountService.GetStudents();
                result.Items.Count.ShouldBeGreaterThan(0);
                var res = from s in result.Items
                          where s.FirstName == "POSTAL"
                          select s;
                res.Count().ShouldBeGreaterThan(0);
            });
        }

        [Fact]
        public async Task GetInstructors_Test()
        {
            await WithUnitOfWorkAsync(async () =>
            {
                await _instructorService.Create(new CreateInstructorDto
                {
                    FirstName = "Leonardo",
                    LastName = "DaVinci",
                    HireDate = DateTime.Now
                });
            });
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var result = await _instructorAccountService.GetInstructors();
                (from i in result.Items
                 where i.FirstName == "Leonardo"
                 select i).Count().ShouldBeGreaterThan(0);
            });
        }

        [Fact]
        public async Task GivePoints_Test()
        {
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var result = await _instructorAccountService.GivePoints(new InstructorGradeStudentDto
                {
                    CourseId = 111,
                    PointCount = 10,
                    StudentId = 2
                });
                result.StudentId.ShouldBe(2);
                result.PointCount.ShouldBe(10);
                result.CourseId.ShouldBe(111);
                result = await _instructorAccountService.GivePoints(new InstructorGradeStudentDto
                {
                    CourseId = 111,
                    PointCount = 20,
                    StudentId = 3
                });
                result.StudentId.ShouldBe(3);
                result.PointCount.ShouldBe(20);
                result.CourseId.ShouldBe(111);
            });
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("PDude", 2, Authorization.Users.UserEntityType.Student);
                var result = await _studentAccountService.GetPoints(null);
                result.Items.Count.ShouldBeGreaterThan(0);
                result.Items[0].Instructor.Id.ShouldBe(1);
                result.Items[0].PointCount.ShouldBe(10);

                LoginAsEntity("jack", 3, Authorization.Users.UserEntityType.Student);
                result = await _studentAccountService.GetPoints(null);
                result.Items.Count.ShouldBeGreaterThan(0);
                result.Items[0].Instructor.Id.ShouldBe(1);
                result.Items[0].PointCount.ShouldBe(20);
            });
        }

        [Fact]
        public async Task GetEnrollments_Test()
        {
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("jack", 3, Authorization.Users.UserEntityType.Student);
                var result = await _studentAccountService.GetEnrollments();
                result.Items.Count.ShouldBeGreaterThan(0);
                result.Items[0].CourseId.ShouldBe(111);
                result.Items[0].Course.Title.ShouldBe("Math");
                result.Items[0].Grade.ShouldBe(Test.University.Grade.A);
            });
        }
    }
}
