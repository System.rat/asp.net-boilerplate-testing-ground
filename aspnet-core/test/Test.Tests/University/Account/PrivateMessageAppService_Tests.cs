﻿using Xunit;
using Shouldly;
using System.Threading.Tasks;
using Test.University.Account.InstructorAccount.Dto;
using Test.University.Account.PrivateMessages.Dto;
using System;
using System.Linq;

namespace Test.Tests.University.Account
{
    public class PrivateMessageAppService_Tests : TestTestBase
    {

        [Fact]
        public async Task CreateAndGetBoardMessage_Test()
        {
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var message = await _instructorAccountService.PostBoardMessage(new InstructorPostBoardMessageDto
                {
                    CourseId = null,
                    DueDate = null,
                    Grade = null,
                    Text = "Give me synchrozine"
                });
                message.Instructor.FirstName.ShouldBe("Daniel");
                message.Text.ShouldBe("Give me synchrozine");
            });
            WithUnitOfWork(() =>
            {
                LoginAsEntity("jack", 3, Authorization.Users.UserEntityType.Student);
                var messages = _studentAccountService.GetAllBoardMessages(null, null);
                messages.Items.Count.ShouldBeGreaterThan(0);
                messages.Items[0].InstructorId.ShouldBe(1);
                messages.Items[0].Text.ShouldBe("Give me synchrozine");
            });
        }

        [Fact]
        public async Task InstructorSendPrivateMessage_Test()
        {
            int instructorId = 0;
            await WithUnitOfWorkAsync(async () =>
            {
                var instructor = await _instructorService.Create(new Test.University.Dto.CreateInstructorDto
                {
                    FirstName = "Leaonardo",
                    LastName = "DaVinci",
                    HireDate = DateTime.Now
                });
                instructorId = instructor.Id;
            });
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var message = await _messageService.SendPrivateMessage(new SharedPrivateMessageDto
                {
                    TargetId = instructorId,
                    TargetType = Authorization.Users.UserEntityType.Instructor,
                    Text = "Nice stuff"
                });
                message.TargetType.ShouldBe(Authorization.Users.UserEntityType.Instructor);
            });
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var messages = await _messageService.GetPrivateMessages();
                messages.Items.Count.ShouldBeGreaterThan(0);
                messages.Items[0].SenderType.ShouldBe(Authorization.Users.UserEntityType.Instructor);
                messages.Items[0].Text.ShouldBe("Nice stuff");
            });
        }

        [Fact]
        public async Task StudentPostPrivateMessage_Test()
        {
            WithUnitOfWork(() =>
            {
                LoginAsEntity("PDude", 2, Authorization.Users.UserEntityType.Student);
                Action action = () => _messageService.SendPrivateMessage(new SharedPrivateMessageDto
                {
                    TargetId = 3,
                    TargetType = Authorization.Users.UserEntityType.Student,
                    Text = "This should not work"
                }).GetAwaiter().GetResult();
                action.ShouldThrow<Abp.UI.UserFriendlyException>();
            });
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("PDude", 2, Authorization.Users.UserEntityType.Student);
                var message = await _messageService.SendPrivateMessage(new SharedPrivateMessageDto
                {
                    TargetId = 1,
                    TargetType = Authorization.Users.UserEntityType.Instructor,
                    Text = "This should work"
                });
                message.TargetType.ShouldBe(Authorization.Users.UserEntityType.Instructor);
            });
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var messages = await _messageService.GetPrivateMessages();
                messages.Items.Count.ShouldBeGreaterThan(0);
                messages.Items[0].Text.ShouldBe("This should work");
                messages.Items[0].SenderName.ShouldBe("POSTAL Dude");
            });
        }

        [Fact]
        public async Task SendReply_Test()
        {
            int messageId = 0;
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var message = await _messageService.SendPrivateMessage(new SharedPrivateMessageDto
                {
                    TargetId = 2,
                    TargetType = Authorization.Users.UserEntityType.Student,
                    Text = "This is a message"
                });
                message.Text.ShouldBe("This is a message");
                messageId = message.Id;
            });
            int replyId = 0;
            await WithUnitOfWorkAsync(async () =>
            {
                LoginAsEntity("PDude", 2, Authorization.Users.UserEntityType.Student);
                var reply = await _messageService.ReplyPrivateMessage(new SharedPrivateMessageReplyDto
                {
                    PrivateBoardMessageId = messageId,
                    Text = "This is a reply"
                });
                reply.Text.ShouldBe("This is a reply");
                reply.SenderType.ShouldBe(Authorization.Users.UserEntityType.Student);
                replyId = reply.Id;
            });
            WithUnitOfWork(() =>
            {
                LoginAsEntity("detective", 1, Authorization.Users.UserEntityType.Instructor);
                var message = _messageService.GetPrivateMessage(messageId);
                message.Replies.Count.ShouldBeGreaterThan(0);
                message.Replies.ToList()[0].Text.ShouldBe("This is a reply");
                message.Replies.ToList()[0].SenderType.ShouldBe(Authorization.Users.UserEntityType.Student);
            });
            WithUnitOfWork(() =>
            {
                LoginAsEntity("jack", 3, Authorization.Users.UserEntityType.Student);
                Action action = () =>
                {
                    _messageService.ReplyPrivateMessage(new SharedPrivateMessageReplyDto
                    {
                        PrivateBoardMessageId = messageId,
                        Text = "If this works I'll yeet the machine through the window"
                    }).GetAwaiter().GetResult();
                };
                action.ShouldThrow<Abp.UI.UserFriendlyException>();
            });
        }
    }
}
