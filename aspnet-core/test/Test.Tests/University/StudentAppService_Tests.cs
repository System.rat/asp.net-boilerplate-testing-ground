﻿using Xunit;
using Shouldly;
using Test.University;
using Test.University.Dto;
using System.Threading.Tasks;
using System;
using Microsoft.EntityFrameworkCore;

namespace Test.Tests.University
{
    public class StudentAppService_Tests : TestTestBase
    {

        [Fact]
        public async Task CreateStudent_Test()
        {
            DateTime now = DateTime.Now;
            var student = await _studentService.Create(new CreateStudentDto
            {
                FirstName = "Jonathan",
                LastName = "Joestar",
                EnrollmentDate = now
            });
            student.FirstName.ShouldBe("Jonathan");
            student.LastName.ShouldBe("Joestar");
            student.EnrollmentDate.ShouldBe(now);
            await UsingDbContextAsync(async ctx =>
            {
                var dbStudent = await ctx.Students.FirstOrDefaultAsync(s => s.Id == student.Id);
                dbStudent.FullName.ShouldBe("Jonathan Joestar");
                dbStudent.EnrollmentDate.ShouldBe(now);
            });
        }

        [Fact]
        public async Task ListStudents_Test()
        {
            var students = await _studentService.GetAll(new PagedStudentResultRequestDto());
            students.Items.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task SearchStudents_Test()
        {
            var studentList = await _studentService.GetAll(new PagedStudentResultRequestDto
            {
                FirstName = "Bob",
                LastName = "Bob",
            });
            studentList.Items.Count.ShouldBe(1);
            studentList.Items[0].LastName.ShouldBe("Bobbert");
            studentList.Items[0].FirstName.ShouldBe("Bob");
        }

        [Fact]
        public void GivePointsInvalid_Test()
        {
            Action action = () => _studentService.GivePoints(new GivePointsStudentDto
            {
                PointCount = 100
            }).GetAwaiter().GetResult();
            action.ShouldThrow<Abp.UI.UserFriendlyException>();
        }

        [Fact]
        public async Task GivePoints_Test()
        {
            await WithUnitOfWorkAsync(async () =>
            {
                var result = await _studentService.GivePoints(new GivePointsStudentDto
                {
                    CourseId = 111,
                    InstructorId = 1,
                    DateGraded = DateTime.Now,
                    PointCount = 100,
                    StudentId = 1
                });
                result.PointCount.ShouldBe(100);
                result.Instructor.FirstName.ShouldBe("Daniel");
                result.Course.Title.ShouldBe("Math");
                var points = await _studentService.GetPoints(new GetPointsInputDto
                {
                    CourseId = 111,
                    StudentId = 1
                });
                points.Items.Count.ShouldBe(1);
            });
        }

        [Fact]
        public void GetEnrolledCourses_Test()
        {
            WithUnitOfWork(() =>
            {
                var enrollments = _studentService.GetEnrolledCourses(1);
                enrollments.Items.Count.ShouldBe(1);
                enrollments.Items[0].Grade.ShouldBe(Grade.A);
                enrollments.Items[0].CourseId.ShouldBe(111);
            });
        }

        [Fact]
        public async Task SetUser_Test()
        {
            int userId = 0;
            await WithUnitOfWorkAsync(async () =>
            {
                userId = (int)(await _userService.Create(new Test.Users.Dto.CreateUserDto
                {
                    Name = "Bob",
                    Surname = "Bobbert",
                    EmailAddress = "bobbert@example.com",
                    Password = "123",
                    UserName = "bob"
                })).Id;
            });
            WithUnitOfWork(() =>
            {
                var result = _studentService.SetUser(new StudentSetUserDto
                {
                    StudentId = 1,
                    UserId = userId
                });
                result.ShouldBeTrue();
            });
        }
    }
}
