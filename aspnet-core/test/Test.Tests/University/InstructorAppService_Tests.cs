﻿using Xunit;
using Shouldly;
using Test.University;
using Test.University.Dto;
using System.Threading.Tasks;
using System;
using Microsoft.EntityFrameworkCore;


namespace Test.Tests.University
{
    public class InstructorAppService_Tests : TestTestBase
    {
        [Fact]
        public async Task CreateInstructor_Test()
        {
            DateTime now = DateTime.Now;
            await WithUnitOfWorkAsync(async () =>
            {
                var instructor = await _instructorService.Create(new CreateInstructorDto
                {
                    FirstName = "Robbert",
                    LastName = "Pike",
                    HireDate = now
                });
                instructor.FirstName.ShouldBe("Robbert");
                instructor.LastName.ShouldBe("Pike");
                instructor.HireDate.ShouldBe(now);
            });
        }

        [Fact]
        public async Task SetOffice_Test()
        {
            await WithUnitOfWorkAsync(async () =>
            {
                var instrucor = await _instructorService.Get(new Abp.Application.Services.Dto.EntityDto<int>
                {
                    Id = 1
                });
                instrucor.OfficeAssignment.ShouldBeNull();
                await _instructorService.SetOffice(new OfficeAssignmentDto
                {
                    InstructorId = 1,
                    Location = "Wall A"
                });
            });
            await WithUnitOfWorkAsync(async () =>
            {
                (await _instructorService.Get(new Abp.Application.Services.Dto.EntityDto<int>
                {
                    Id = 1
                })).OfficeAssignment.Location.ShouldBe("Wall A");
            });
        }

        [Fact]
        public async Task SetUser_Test()
        {
            var userId = 0;
            await WithUnitOfWorkAsync(async () =>
            {
                userId = (int)(await _userService.Create(new Test.Users.Dto.CreateUserDto
                {
                    Name = "Daniel",
                    Surname = "Lazarski",
                    EmailAddress = "lazarski@example.com",
                    Password = "123",
                    UserName = "daniel"
                })).Id;
            });
            WithUnitOfWork(() =>
            {
                var result = _instructorService.SetUser(new InstructorSetUserDto
                {
                    InstructorId = 1,
                    UserId = userId
                });
                result.ShouldBeTrue();
            });
        }
    }
}
