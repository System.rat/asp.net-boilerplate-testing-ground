﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Test.Tests.Notifications
{

    public class NullHub<T> : IHubContext<T> where T : Hub
    {
        public IHubClients Clients => new NullClients();

        public IGroupManager Groups => throw new NotImplementedException();
    }

    public class NullGroupManager : IGroupManager
    {
        public Task AddToGroupAsync(string connectionId, string groupName, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        public Task RemoveFromGroupAsync(string connectionId, string groupName, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }
    }

    public class NullClients : IHubClients
    {
        public IClientProxy All => new NullCientProxy();

        public IClientProxy AllExcept(IReadOnlyList<string> excludedConnectionIds)
        {
            return new NullCientProxy();
        }

        public IClientProxy Client(string connectionId)
        {
            return new NullCientProxy();
        }

        public IClientProxy Clients(IReadOnlyList<string> connectionIds)
        {
            return new NullCientProxy();
        }

        public IClientProxy Group(string groupName)
        {
            return new NullCientProxy();
        }

        public IClientProxy GroupExcept(string groupName, IReadOnlyList<string> excludedConnectionIds)
        {
            return new NullCientProxy();
        }

        public IClientProxy Groups(IReadOnlyList<string> groupNames)
        {
            return new NullCientProxy();
        }

        public IClientProxy User(string userId)
        {
            return new NullCientProxy();
        }

        public IClientProxy Users(IReadOnlyList<string> userIds)
        {
            return new NullCientProxy();
        }
    }

    public class NullCientProxy : IClientProxy
    {
        public Task SendCoreAsync(string method, object[] args, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }
    }
}
